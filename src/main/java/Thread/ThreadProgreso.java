/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Thread;

import Utilidades.Constantes;
import Utilidades.Dialog;
import javax.swing.JFrame;
import javax.swing.WindowConstants;
import java.awt.Taskbar;
import java.awt.Taskbar.State;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author batoi
 */
public class ThreadProgreso extends Thread {

    private long time;
    protected static JFrame dialog;

    public ThreadProgreso(long time) {
        this.time = time;
        if (dialog == null){
            dialog = new JFrame(Constantes.VACIO);
        }
    }

    public ThreadProgreso(int progreso, ThreadProgreso thread_a_esperar) {
        
    }
    public synchronized void cerrar_dialog() {
        dialog.setVisible(Constantes.BOOLEAN_POR_DEFECTO);
    }

    public ThreadProgreso() {
        
    }
    @Override
    public void run() {
        super.run(); //To change body of generated methods, choose Tools | Templates.
        if (Taskbar.isTaskbarSupported()) {
            try {
                dialog.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
                dialog.setVisible(true);
                
                Taskbar taskbar = Taskbar.getTaskbar();
                taskbar.setWindowProgressState(dialog, Taskbar.State.NORMAL);
                taskbar.setWindowProgressValue(dialog, (int) this.time);
                Thread.sleep(50);
            } catch (InterruptedException ex) {
                Dialog.Mensaje_de_excepcion(ex);
            }
        }
    }

}
