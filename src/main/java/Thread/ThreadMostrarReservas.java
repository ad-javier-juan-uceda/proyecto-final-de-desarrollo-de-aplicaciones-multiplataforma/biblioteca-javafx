/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Thread;

import Modelo.Libro;
import Utilidades.Dialog;
import com.mycompany.proyectobiblioteca.App;
import java.util.ResourceBundle;
import javafx.application.Platform;

/**
 *
 * @author batoi
 */
public class ThreadMostrarReservas extends Thread {

    private final Libro libro;

    public ThreadMostrarReservas(Libro libro) {
        this.libro = libro;
    }

    @Override
    public void run() {
        super.run();
        ResourceBundle recursos = App.ObtenerRecursos();
        try {
            Dialog.notificacion(libro);
        } catch (NullPointerException n) {
            Dialog.Dialogo_de_confirmacion(recursos.getString("tituloLibro") + libro.getTitulo() + System.lineSeparator() + (libro.isEsta_prestado() ? recursos.getString("prestado") : recursos.getString("noprestado")));
        }

    }

}
