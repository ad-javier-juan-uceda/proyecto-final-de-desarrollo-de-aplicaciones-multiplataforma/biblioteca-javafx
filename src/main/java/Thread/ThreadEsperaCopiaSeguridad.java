/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Thread;

import Utilidades.Constantes;
import Utilidades.Dialog;
import java.io.File;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;

/**
 *
 * @author batoi
 */
public class ThreadEsperaCopiaSeguridad extends Thread {
    
    private Preferences preferences;

    public ThreadEsperaCopiaSeguridad() {
        preferences = Preferences.userRoot().node(Constantes.ARCHIVO_PREFERENCIAS);
    }

    @Override
    public void run() {
        super.run(); //To change body of generated methods, choose Tools | Templates.
        //System.out.println(Calendar.getInstance().get(Calendar.DAY_OF_WEEK) - 1);
        while (!isInterrupted()) {
            Date fecha = new Date();
            String[] diasDeLaSemana = preferences.get("dias", "-1 -1 -1 -1 -1 -1 -1").split(" ");
            int hora = preferences.getInt("hora", 0);
            int minuto = preferences.getInt("minuto", 0);
            int segundo = preferences.getInt("segundo", 0);
            boolean isHora = hora == fecha.getHours();
            boolean isMinutos = minuto == fecha.getMinutes();
            boolean isSegundos = segundo == fecha.getSeconds();
            int diaSemana = Calendar.getInstance().get(Calendar.DAY_OF_WEEK);
            if (diaSemana == Calendar.SATURDAY){
                diaSemana -= 2;
            }
            else{
                diaSemana--;
            }
            //System.out.println(Integer.parseInt(diasDeLaSemana[diaSemana]) + " == " + diaSemana);
            if (Integer.parseInt(diasDeLaSemana[diaSemana]) == diaSemana && isHora && isMinutos && isSegundos){
                String nombreDelArchivo = preferences.get("carpeta", System.getProperty("user.home")) + File.separator + fecha.getDate() + "-" + (fecha.getMonth() + 1) + "-" + (fecha.getYear() + 1900);
                copiaSeguridad copia = new copiaSeguridad(nombreDelArchivo);
                copia.start();
                try {
                    Thread.sleep(60000);
                } catch (InterruptedException ex) {
                    
                }
            }
        }
    }
    
    
    
}
