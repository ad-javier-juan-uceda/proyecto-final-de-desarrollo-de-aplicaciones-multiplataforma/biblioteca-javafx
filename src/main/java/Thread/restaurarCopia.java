/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Thread;

import ConexionOdoo.ConexionObjectDB;
import ControladorVista.ControladorGlobal;
import static ControladorVista.ControladorGlobal.*;
import DAO.*;
import DAOObjectDB.*;
import Utilidades.Dialog;
import com.jfoenix.controls.JFXButton;
import java.io.File;
import java.text.DecimalFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.scene.control.ProgressBar;

/**
 *
 * @author Usuario
 */
public class restaurarCopia extends Thread {

    private File nombreDelArchivo;
    private ProgressBar progreso;
    private JFXButton cancelar;

    public restaurarCopia(File nombreDelArchivo) {
        this.nombreDelArchivo = nombreDelArchivo;
    }

    public restaurarCopia(File archivo, ProgressBar progreso) {
        this.nombreDelArchivo = archivo;
        this.progreso = progreso;
    }

    public restaurarCopia(File archivo, ProgressBar progreso, JFXButton cancelar) {
        this.nombreDelArchivo = archivo;
        this.progreso = progreso;
        this.cancelar = cancelar;
    }

    @Override
    public void run() {
        super.run(); //To change body of generated methods, choose Tools | Templates.
        int total = 11;
        this.progreso.setVisible(true);
        this.cancelar.setVisible(true);
        try {
            this.cancelar.setOnAction((arg0) -> {
                try {
                    Platform.runLater(() -> {
                        this.progreso.setVisible(false);
                        this.progreso.setProgress(0.0);
                        this.cancelar.setVisible(false);
                        this.cancelar.setOnAction((arg1) -> {

                        });
                    });

                    finalize();
                } catch (Throwable ex) {

                }
            });
            ConexionObjectDB.setLugar(nombreDelArchivo.getAbsolutePath());
            cambiarProgreso(0.0);
            try {
                tipoDBDAO.insertarMasDeUnRegistro(tipoDAO.buscarTodos());
                cambiarProgreso((double) 1 / total);
                autorDAO.insertarMasDeUnRegistro(autorObjectDBDAO.buscarTodos());
                cambiarProgreso((double) 2 / total);
                localizacionDAO.insertarMasDeUnRegistro(localizacionDBDAO.buscarTodos());
                cambiarProgreso((double) 3 / total);
                editorialDAO.insertarMasDeUnRegistro(editorialObjectDBDAO.buscarTodos());
                cambiarProgreso((double) 4 / total);
                libroDAO.insertarMasDeUnRegistro(libroObjectDBDAO.buscarTodos());
                cambiarProgreso((double) 5 / total);
                usuarioDBDAO.insertarMasDeUnRegistro(usuarioDAO.buscarTodos());
                cambiarProgreso((double) 6 / total);
                socioDBDAO.insertarMasDeUnRegistro(socioDAO.buscarTodos());
                cambiarProgreso((double) 7 / total);
                retiradaDBDAO.insertarMasDeUnRegistro(retiradaDAO.buscarTodos());
                cambiarProgreso((double) 8 / total);
                prestamoDBDAO.insertarMasDeUnRegistro(prestamoDAO.buscarTodos());
                cambiarProgreso((double) 9 / total);
                pedidoDBDAO.insertarMasDeUnRegistro(pedidoDAO.buscarTodos());
                cambiarProgreso((double) 10 / total);
                Linea_pedidoDBDAO.insertarMasDeUnRegistro(linea_PedidoObjectDBDAO.buscarTodos());
                cambiarProgreso((double) 11 / total);
                pedidoDBDAO.rellenar(pedidoDAO.buscarTodos(), pedidoDBDAO.buscarTodos());
                cambiarProgreso((double) 12 / total);
            } catch (Exception e) {
                System.err.println("Error");
                e.printStackTrace();
            }
            Platform.runLater(() -> {
                Dialog.Dialogo_de_confirmacion("Copia de seguridad " + nombreDelArchivo + " completada.");
            });

        } catch (Exception ex) {
            Dialog.Mensaje_de_excepcion(ex);
        } finally {
            Platform.runLater(() -> {
                this.progreso.setVisible(false);
                this.progreso.setProgress(0.0);
                this.cancelar.setVisible(false);
            });

        }
    }

    public void cambiarProgreso(double progreso) throws Exception {
        Platform.runLater(() -> {
            this.progreso.setProgress(Double.parseDouble(new DecimalFormat("#.##").format(progreso).replace(",", ".")));
        });
    }

}
