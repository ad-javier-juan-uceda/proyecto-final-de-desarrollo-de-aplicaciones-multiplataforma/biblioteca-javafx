/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Thread;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

/**
 *
 * @author batoi
 */
public class ThreadAbrirArchivo extends Thread {
    
    private File archivo_a_abrir;
    private boolean directory;

    public ThreadAbrirArchivo(File archivo_a_abrir) {
        this.archivo_a_abrir = archivo_a_abrir;
    }

    public ThreadAbrirArchivo(File archivo, boolean b) {
        this.archivo_a_abrir = archivo;
        directory = b;
        
    }
    
    
    
    @Override
    public void run() {
        super.run(); //To change body of generated methods, choose Tools | Templates.
        try {
            
            if (directory){
                Desktop.getDesktop().browseFileDirectory(archivo_a_abrir);
            }
            else{
                Desktop.getDesktop().open(archivo_a_abrir);
            }
            
        } catch (IOException ex) {
            
        }
    }
    
}
