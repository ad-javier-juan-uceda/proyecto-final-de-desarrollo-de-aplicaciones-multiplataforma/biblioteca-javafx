/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Thread;

import ControladorVista.ControladorGlobal;
import Utilidades.Dialog;
import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author batoi
 */
public class ThreadAbrirPaginaWeb extends Thread {

    @Override
    public void run() {
        super.run(); //To change body of generated methods, choose Tools | Templates.
        try {
            Desktop.getDesktop().browse(new URI(ConexionOdoo.ConexionOdoo.URL));
        } catch (URISyntaxException ex) {
            Dialog.Mensaje_de_excepcion(ex);
        } catch (IOException ex) {
            Dialog.Mensaje_de_excepcion(ex);
        }
    }
    
    
    
}
