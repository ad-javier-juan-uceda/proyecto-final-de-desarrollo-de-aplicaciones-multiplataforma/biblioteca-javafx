/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Thread;

import ConexionOdoo.ConexionObjectDB;
import static ControladorVista.ControladorGlobal.autorDAO;
import static ControladorVista.ControladorGlobal.autorObjectDBDAO;
import static ControladorVista.ControladorGlobal.editorialDAO;
import static ControladorVista.ControladorGlobal.*;
import Utilidades.Dialog;
import com.jfoenix.controls.JFXButton;
import com.mycompany.proyectobiblioteca.App;
import java.io.File;
import java.text.DecimalFormat;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.scene.control.ProgressBar;

/**
 *
 * @author Usuario
 */
public class copiaSeguridad extends Thread {

    private String nombreDelArchivo;
    private ProgressBar progreso;
    private JFXButton cancelar;

    public copiaSeguridad(String nombreDelArchivo) {
        this.nombreDelArchivo = nombreDelArchivo;
    }

    public copiaSeguridad(String archivo, ProgressBar progreso) {
        this.nombreDelArchivo = archivo;
        this.progreso = progreso;
    }

    public copiaSeguridad(String archivo, ProgressBar progreso, JFXButton cancelar) {
        this.nombreDelArchivo = archivo;
        this.progreso = progreso;
        this.cancelar = cancelar;
    }

    @Override
    public void run() {
        super.run();
        
        int total = 11;
        this.progreso.setVisible(true);
        this.cancelar.setVisible(true);
        File archivo = null;
        try {
            this.cancelar.setOnAction((arg0) -> {
                try {
                    Platform.runLater(() -> {
                        this.progreso.setVisible(false);
                        this.progreso.setProgress(0.0);
                        this.cancelar.setVisible(false);
                        this.cancelar.setOnAction((arg1) -> {

                        });
                    });

                    finalize();
                } catch (Throwable ex) {

                }
            });
            archivo = new File(nombreDelArchivo + File.separator + "empresa");
            ConexionObjectDB.setLugar(archivo.getAbsolutePath());
            cambiarProgreso(0.0);
            autorObjectDBDAO.insertarMasDeUnRegistro(autorDAO.buscarTodos());
            cambiarProgreso((double) 1 / total);
            editorialObjectDBDAO.insertarMasDeUnRegistro(editorialDAO.buscarTodos());
            cambiarProgreso((double) 2 / total);
            libroObjectDBDAO.insertarMasDeUnRegistro(libroDAO.buscarTodos());
            cambiarProgreso((double) 3 / total);
            linea_PedidoObjectDBDAO.insertarMasDeUnRegistro(Linea_pedidoDBDAO.buscarTodos());
            cambiarProgreso((double) 4 / total);
            localizacionDBDAO.insertarMasDeUnRegistro(localizacionDAO.buscarTodos());
            cambiarProgreso((double) 5 / total);
            pedidoDAO.insertarMasDeUnRegistro(pedidoDBDAO.buscarTodos());
            cambiarProgreso((double) 6 / total);
            prestamoDAO.insertarMasDeUnRegistro(prestamoDBDAO.buscarTodos());
            cambiarProgreso((double) 7 / total);
            retiradaDAO.insertarMasDeUnRegistro(retiradaDBDAO.buscarTodos());
            cambiarProgreso((double) 8 / total);
            socioDAO.insertarMasDeUnRegistro(socioDBDAO.buscarTodos());
            cambiarProgreso((double) 9 / total);
            tipoDAO.insertarMasDeUnRegistro(tipoDBDAO.buscarTodos());
            cambiarProgreso((double) 10 / total);
            usuarioDAO.insertarMasDeUnRegistro(usuarioDBDAO.buscarTodos());
            cambiarProgreso((double) 11 / total);
            Platform.runLater(() -> {
                ResourceBundle recursos = App.ObtenerRecursos();
                //Dialog.Dialogo_de_confirmacion("Copia de seguridad " + nombreDelArchivo + " completada.");
                Dialog.Dialogo_de_confirmacion(recursos.getString("copiaSeguridad") + nombreDelArchivo + recursos.getString("completada"));
            });

        } catch (Exception ex) {
            archivo.delete();
            Dialog.Mensaje_de_excepcion(ex);
        } finally {
            Platform.runLater(() -> {
                this.progreso.setVisible(false);
                this.progreso.setProgress(0.0);
                this.cancelar.setVisible(false);
            });

        }
    }

    public void cambiarProgreso(double progreso) {
        Platform.runLater(() -> {
            DecimalFormat df = new DecimalFormat("#.##");
            String formatted = df.format(progreso).replace(",", ".");
            double numero = Double.parseDouble(formatted);
            this.progreso.setProgress(numero);
        });
    }

}
