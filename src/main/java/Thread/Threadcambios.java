/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Thread;

import static ControladorVista.ControladorGlobal.libroDAO;
import Utilidades.Dialog;
import javafx.application.Platform;
import org.apache.xmlrpc.XmlRpcException;

/**
 *
 * @author batoi
 */
public class Threadcambios extends Thread {
    
    private final int idLibro;
    private boolean comparar;

   
    public Threadcambios(int idLibro) {
        this.idLibro = idLibro;
    }

    

    @Override
    public void run() {
        super.run();      
        try {
            comparar = libroDAO.buscarPorClavePrimaria(idLibro).isEsta_prestado();
            while (!isInterrupted()){
                boolean esta_prestado = libroDAO.buscarPorClavePrimaria(idLibro).isEsta_prestado();
                if (esta_prestado != comparar) {
                    comparar = esta_prestado;
                    Platform.runLater(new ThreadMostrarReservas(libroDAO.buscarPorClavePrimaria(idLibro)));
                }
            }
        } catch (XmlRpcException ex) {
           Platform.runLater(() -> {
               Dialog.Mensaje_de_excepcion(ex);
           });
        } catch (Exception ex) {
            Platform.runLater(() -> {
               Dialog.Mensaje_de_excepcion(ex);
           });
        }
    }
    
}
