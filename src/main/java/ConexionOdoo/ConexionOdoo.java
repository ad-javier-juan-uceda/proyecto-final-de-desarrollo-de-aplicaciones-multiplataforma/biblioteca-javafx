package ConexionOdoo;

import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;

import java.net.MalformedURLException;
import java.net.URL;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyMap;

public class ConexionOdoo {

    public static final String URL = "http://192.168.1.3:8069";
    public static final String DB = "proyectocurso";
    public static final String USERNAME = "batoi@gmail.com";
    public static final String PASSWORD = "1234";
    public static final String ID = "id";
    public static final String FIELDS = "fields";
    public static final String BUSCAR_EN_LA_TABLA = "search_read";
    public static final String EJECUTAR = "execute_kw";
    public static final String INSERTAR = "create";
    public static final String ACTUALIZAR = "write";
    public static final String ELIMINAR = "unlink";

    private static XmlRpcClient api;
    public static Object uid;

    public ConexionOdoo() {
        api = null;
        uid = 0;
    }

    public static XmlRpcClient getConnectionOdoo() throws MalformedURLException, XmlRpcException {
        if (api == null) {
            api = new XmlRpcClient();
            XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();
            config.setServerURL(new URL(String.format("%s/xmlrpc/2/common", URL)));

            uid = api.execute(config, "authenticate", asList(DB, USERNAME, PASSWORD, emptyMap()));

            XmlRpcClientConfigImpl config2 = new XmlRpcClientConfigImpl();
            config2.setServerURL(new URL(String.format("%s/xmlrpc/2/object", URL)));
            api.setConfig(config2);
        }

        return api;
    }
}
