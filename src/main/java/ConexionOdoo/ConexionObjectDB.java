/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ConexionOdoo;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceException;

/**
 *
 * @author Usuario
 */
public class ConexionObjectDB {

    private static EntityManagerFactory emf;
    private static EntityManager em;

    private static String url = "objectdb:empresa.biblioteca;drop";
    public static final int LIMIT_EVALUATION = 10;
    public static EntityManagerFactory getConnectionemf() throws PersistenceException, Exception {
        emf = Persistence.createEntityManagerFactory(url);

        return emf;

    }

    public static EntityManager getConnectionem() throws PersistenceException, Exception {
        em = Persistence.createEntityManagerFactory(url).createEntityManager();
        return em;
    }

    public static void setLugar(String lugar) throws PersistenceException, Exception {
        url = "objectdb:" + lugar + ".biblioteca;drop";
        getConnectionemf();
        getConnectionem();
    }

    public static void cerrar() throws PersistenceException, Exception {
        em.close();
    }

}
