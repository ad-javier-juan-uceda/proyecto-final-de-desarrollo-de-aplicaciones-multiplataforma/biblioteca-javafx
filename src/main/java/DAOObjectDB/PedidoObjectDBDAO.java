/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAOObjectDB;

import ConexionOdoo.ConexionObjectDB;
import Modelo.Pedido;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import org.GenericoDAO.GenericoDAO;
import org.apache.xmlrpc.XmlRpcException;

/**
 *
 * @author Usuario
 */
public class PedidoObjectDBDAO implements GenericoDAO<Pedido> {

    private static EntityManager em;

    public PedidoObjectDBDAO() throws Exception {
        em = ConexionObjectDB.getConnectionem();
    }

    @Override
    public ArrayList<Pedido> buscarTodos() throws MalformedURLException, XmlRpcException, Exception {
        em.getTransaction().begin();
        ArrayList<Pedido> lista = new ArrayList<>();
        List<Pedido> Articulos_recibidos = new ArrayList<>();
        try {
            ConexionObjectDB.cerrar();
            em = ConexionObjectDB.getConnectionem();
            TypedQuery<Pedido> query = em.createQuery("SELECT c FROM Pedido c", Pedido.class);
            Articulos_recibidos = query.getResultList();
            lista = new ArrayList<>();
            for (Pedido autor : Articulos_recibidos) {
                lista.add(autor);
            }
        } catch (Exception e) {
            ConexionObjectDB.cerrar();
            em = ConexionObjectDB.getConnectionem();
        }
        return lista;
    }

    @Override
    public Pedido buscarPorClavePrimaria(int id) throws MalformedURLException, XmlRpcException, Exception {
        TypedQuery<Pedido> query = em.createQuery("SELECT c FROM Pedido c WHERE c.id= " + id, Pedido.class);
        List<Pedido> Articulos_recibidos = query.getResultList();
        return Articulos_recibidos.get(0);
    }

    @Override
    public ArrayList<Pedido> buscarPorTodasLasClavePrimaria(ArrayList<Integer> ids) throws MalformedURLException, XmlRpcException, Exception {
        ArrayList<Pedido> lista = new ArrayList<>();
        for (Integer autor : ids) {
            lista.add(buscarPorClavePrimaria(autor));
        }
        return lista;
    }

    @Override
    public Integer insertarRegistro(Pedido objeto) throws MalformedURLException, XmlRpcException, Exception {
        em.getTransaction().begin();
        em.persist(objeto);
        em.getTransaction().commit();
        return objeto.getId();
    }

    @Override
    public boolean actualizarRegistro(Pedido objeto) throws MalformedURLException, XmlRpcException, Exception {
        Pedido autor = buscarPorClavePrimaria(objeto.getId());
        em.getTransaction().begin();
        autor.setFecha_de_entrega(objeto.getFecha_de_entrega());
        autor.setFecha_de_realizacion(objeto.getFecha_de_realizacion());
        autor.setLineas_pedido(objeto.getLineas_pedido());
        autor.setTotal_pedido(objeto.getTotal_pedido());
        autor.setUsuario(objeto.getUsuario());
        em.getTransaction().commit();
        return false;
    }

    @Override
    public boolean eliminarRegistro(Pedido objeto) throws MalformedURLException, XmlRpcException, Exception {
        em.getTransaction().begin();
        em.remove(objeto);
        em.getTransaction().commit();
        return false;
    }

    @Override
    public boolean eliminarRegistro(int idObjeto) throws MalformedURLException, XmlRpcException, Exception {
        Pedido employee = buscarPorClavePrimaria(idObjeto);
        em.getTransaction().begin();
        em.remove(employee);
        em.getTransaction().commit();
        return false;
    }

    @Override
    public ArrayList<Pedido> obtenerLista(List<Object> listaObjetos) throws MalformedURLException, XmlRpcException, Exception {
        return null;
    }

    @Override
    public Integer[] insertarMasDeUnRegistro(ArrayList<Pedido> listaObjetos) throws MalformedURLException, XmlRpcException, Exception {
        for (Pedido autor : listaObjetos) {
            ConexionObjectDB.getConnectionem();
            ConexionObjectDB.getConnectionemf();
            insertarRegistro(autor);
            ConexionObjectDB.cerrar();
        }
        return null;
    }

}
