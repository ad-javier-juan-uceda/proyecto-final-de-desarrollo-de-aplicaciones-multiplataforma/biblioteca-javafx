/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAOObjectDB;

import ConexionOdoo.ConexionObjectDB;
import Modelo.Autor;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import org.GenericoDAO.GenericoDAO;
import org.apache.xmlrpc.XmlRpcException;

/**
 *
 * @author Usuario
 */
public class AutorObjectDBDAO implements GenericoDAO<Autor>  {
    
    private static EntityManager em;

    public AutorObjectDBDAO() throws Exception {
        em = ConexionObjectDB.getConnectionem();
    }


    @Override
    public ArrayList<Autor> buscarTodos() throws MalformedURLException, XmlRpcException, Exception {
        TypedQuery<Autor> query = em.createQuery("SELECT c FROM Autor c", Autor.class);
        List<Autor> Articulos_recibidos = query.getResultList();
        ArrayList<Autor> lista = new ArrayList<>();
        for(Autor autor : Articulos_recibidos){
            lista.add(autor);
        }
        return lista;
    }

    @Override
    public Autor buscarPorClavePrimaria(int id) throws MalformedURLException, XmlRpcException, Exception {
        TypedQuery<Autor> query = em.createQuery("SELECT c FROM Autor c WHERE c.id= " + id, Autor.class);
        List<Autor> Articulos_recibidos = query.getResultList();
        return Articulos_recibidos.get(0);
    }

    @Override
    public ArrayList<Autor> buscarPorTodasLasClavePrimaria(ArrayList<Integer> ids) throws MalformedURLException, XmlRpcException, Exception {
        ArrayList<Autor> lista = new ArrayList<>();
        for(Integer autor : ids){
            lista.add(buscarPorClavePrimaria(autor));
        }
        return lista;
    }

    @Override
    public Integer insertarRegistro(Autor objeto) throws MalformedURLException, XmlRpcException, Exception {
        em.getTransaction().begin();
        em.persist(objeto);
        em.getTransaction().commit();
        return objeto.getId();
    }

    @Override
    public boolean actualizarRegistro(Autor objeto) throws MalformedURLException, XmlRpcException, Exception {
        Autor autor = buscarPorClavePrimaria(objeto.getId());
        em.getTransaction().begin();
        autor.setFacebook(objeto.getFacebook());
        autor.setFecha_de_nacimiento(objeto.getFecha_de_nacimiento());
        autor.setInstagram(objeto.getInstagram());
        autor.setNombre(objeto.getNombre());
        autor.setPagina_web(objeto.getPagina_web());
        autor.setTelefono(objeto.getTelefono());
        autor.setTwitter(objeto.getTwitter());
        em.getTransaction().commit();

        return false;
        
    }

    @Override
    public boolean eliminarRegistro(Autor objeto) throws MalformedURLException, XmlRpcException, Exception {
        em.getTransaction().begin();
        em.remove(objeto);
        em.getTransaction().commit();
        return false;
    }

    @Override
    public boolean eliminarRegistro(int idObjeto) throws MalformedURLException, XmlRpcException, Exception {
        Autor employee = buscarPorClavePrimaria(idObjeto);
        em.getTransaction().begin();
        em.remove(employee);
        em.getTransaction().commit();
        return true;

    }

    @Override
    public ArrayList<Autor> obtenerLista(List<Object> listaObjetos) throws MalformedURLException, XmlRpcException, Exception {
        return null;
    }

    @Override
    public Integer[] insertarMasDeUnRegistro(ArrayList<Autor> listaObjetos) throws MalformedURLException, XmlRpcException, Exception {
        for(Autor autor : listaObjetos){
            ConexionObjectDB.getConnectionem();
            ConexionObjectDB.getConnectionemf();
            insertarRegistro(autor);
            ConexionObjectDB.cerrar();
        }
        return null;
    }
    
}
