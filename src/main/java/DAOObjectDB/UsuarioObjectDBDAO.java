/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAOObjectDB;

import ConexionOdoo.ConexionObjectDB;
import Modelo.Usuario;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import org.GenericoDAO.GenericoDAO;
import org.apache.xmlrpc.XmlRpcException;

/**
 *
 * @author Usuario
 */
public class UsuarioObjectDBDAO implements GenericoDAO<Usuario> {
    
    private static EntityManager em;

    public UsuarioObjectDBDAO() throws Exception {
        em = ConexionObjectDB.getConnectionem();
    }

    @Override
    public ArrayList<Usuario> buscarTodos() throws MalformedURLException, XmlRpcException, Exception {
        TypedQuery<Usuario> query = em.createQuery("SELECT c FROM Usuario c", Usuario.class);
        List<Usuario> Articulos_recibidos = query.getResultList();
        ArrayList<Usuario> lista = new ArrayList<>();
        for (Usuario autor : Articulos_recibidos) {
            lista.add(autor);
        }
        return lista;
    }

    @Override
    public Usuario buscarPorClavePrimaria(int id) throws MalformedURLException, XmlRpcException, Exception {
        TypedQuery<Usuario> query = em.createQuery("SELECT c FROM Usuario c WHERE c.id= " + id, Usuario.class);
        List<Usuario> Articulos_recibidos = query.getResultList();
        return Articulos_recibidos.get(0);
    }

    @Override
    public ArrayList<Usuario> buscarPorTodasLasClavePrimaria(ArrayList<Integer> ids) throws MalformedURLException, XmlRpcException, Exception {
        ArrayList<Usuario> lista = new ArrayList<>();
        for (Integer autor : ids) {
            lista.add(buscarPorClavePrimaria(autor));
        }
        return lista;
    }

    @Override
    public Integer insertarRegistro(Usuario objeto) throws MalformedURLException, XmlRpcException, Exception {
        em.getTransaction().begin();
        em.persist(objeto);
        em.getTransaction().commit();
        return objeto.getId();
    }

    @Override
    public boolean actualizarRegistro(Usuario objeto) throws MalformedURLException, XmlRpcException, Exception {
        Usuario autor = buscarPorClavePrimaria(objeto.getId());
        em.getTransaction().begin();
        autor.setDNI(autor.getDNI());
        autor.setDireccion(autor.getDireccion());
        autor.setFecha_de_nacimiento(autor.getFecha_de_nacimiento());
        autor.setLocalidad(autor.getLocalidad());
        autor.setNombre(autor.getNombre());
        em.getTransaction().commit();
        return false;
    }

    @Override
    public boolean eliminarRegistro(Usuario objeto) throws MalformedURLException, XmlRpcException, Exception {
        em.getTransaction().begin();
        em.remove(objeto);
        em.getTransaction().commit();
        return false;
    }

    @Override
    public boolean eliminarRegistro(int idObjeto) throws MalformedURLException, XmlRpcException, Exception {
        Usuario employee = buscarPorClavePrimaria(idObjeto);
        em.getTransaction().begin();
        em.remove(employee);
        em.getTransaction().commit();
        return false;
    }

    @Override
    public ArrayList<Usuario> obtenerLista(List<Object> listaObjetos) throws MalformedURLException, XmlRpcException, Exception {
        return null;
        
    }

    @Override
    public Integer[] insertarMasDeUnRegistro(ArrayList<Usuario> listaObjetos) throws MalformedURLException, XmlRpcException, Exception {
        em.getTransaction().begin();
        for (Usuario autor : listaObjetos) {

            try {
                ConexionObjectDB.cerrar();
                em = ConexionObjectDB.getConnectionem();
                insertarRegistro(autor);
            } catch (Exception e) {
                ConexionObjectDB.cerrar();
                em = ConexionObjectDB.getConnectionem();
            }

        }
        return null;
    }
    
}
