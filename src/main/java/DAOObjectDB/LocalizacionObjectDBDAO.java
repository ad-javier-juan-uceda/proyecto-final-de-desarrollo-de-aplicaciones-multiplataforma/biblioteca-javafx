/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAOObjectDB;

import ConexionOdoo.ConexionObjectDB;
import Modelo.Localizacion;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import org.GenericoDAO.GenericoDAO;
import org.apache.xmlrpc.XmlRpcException;

/**
 *
 * @author Usuario
 */
public class LocalizacionObjectDBDAO implements GenericoDAO<Localizacion> {

    private static EntityManager em;

    public LocalizacionObjectDBDAO() throws Exception {
        em = ConexionObjectDB.getConnectionem();
    }

    @Override
    public ArrayList<Localizacion> buscarTodos() throws MalformedURLException, XmlRpcException, Exception {
        TypedQuery<Localizacion> query = em.createQuery("SELECT c FROM Localizacion c", Localizacion.class);
        List<Localizacion> Articulos_recibidos = query.getResultList();
        ArrayList<Localizacion> lista = new ArrayList<>();
        for (Localizacion autor : Articulos_recibidos) {
            lista.add(autor);
        }
        return lista;
    }

    @Override
    public Localizacion buscarPorClavePrimaria(int id) throws MalformedURLException, XmlRpcException, Exception {
        TypedQuery<Localizacion> query = em.createQuery("SELECT c FROM Localizacion c WHERE c.id= " + id, Localizacion.class);
        List<Localizacion> Articulos_recibidos = query.getResultList();
        return Articulos_recibidos.get(0);
    }

    @Override
    public ArrayList<Localizacion> buscarPorTodasLasClavePrimaria(ArrayList<Integer> ids) throws MalformedURLException, XmlRpcException, Exception {
        ArrayList<Localizacion> lista = new ArrayList<>();
        for (Integer autor : ids) {
            lista.add(buscarPorClavePrimaria(autor));
        }
        return lista;
    }

    @Override
    public Integer insertarRegistro(Localizacion objeto) throws MalformedURLException, XmlRpcException, Exception {
        em.getTransaction().begin();
        em.persist(objeto);
        em.getTransaction().commit();
        return objeto.getId();
    }

    @Override
    public boolean actualizarRegistro(Localizacion objeto) throws MalformedURLException, XmlRpcException, Exception {
        Localizacion autor = buscarPorClavePrimaria(objeto.getId());
        em.getTransaction().begin();
        autor.setNombre(autor.getNombre());
        em.getTransaction().commit();
        return false;
    }

    @Override
    public boolean eliminarRegistro(Localizacion objeto) throws MalformedURLException, XmlRpcException, Exception {
        em.getTransaction().begin();
        em.remove(objeto);
        em.getTransaction().commit();
        return false;
    }

    @Override
    public boolean eliminarRegistro(int idObjeto) throws MalformedURLException, XmlRpcException, Exception {
        Localizacion employee = buscarPorClavePrimaria(idObjeto);
        em.getTransaction().begin();
        em.remove(employee);
        em.getTransaction().commit();
        return false;
    }

    @Override
    public ArrayList<Localizacion> obtenerLista(List<Object> listaObjetos) throws MalformedURLException, XmlRpcException, Exception {
        return null;

    }

    @Override
    public Integer[] insertarMasDeUnRegistro(ArrayList<Localizacion> listaObjetos) throws MalformedURLException, XmlRpcException, Exception {
        for (Localizacion autor : listaObjetos) {
            ConexionObjectDB.getConnectionem();
            ConexionObjectDB.getConnectionemf();
            insertarRegistro(autor);
            ConexionObjectDB.cerrar();
        }
        return null;
    }

}
