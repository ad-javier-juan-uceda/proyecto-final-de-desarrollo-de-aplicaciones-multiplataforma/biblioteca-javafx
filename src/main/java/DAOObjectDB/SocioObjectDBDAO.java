/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAOObjectDB;

import ConexionOdoo.ConexionObjectDB;
import Modelo.Socio;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import org.GenericoDAO.GenericoDAO;
import org.apache.xmlrpc.XmlRpcException;

/**
 *
 * @author Usuario
 */
public class SocioObjectDBDAO implements GenericoDAO<Socio> {

    private static EntityManager em;

    public SocioObjectDBDAO() throws Exception {
        em = ConexionObjectDB.getConnectionem();
    }

    @Override
    public ArrayList<Socio> buscarTodos() throws MalformedURLException, XmlRpcException, Exception {
        TypedQuery<Socio> query = em.createQuery("SELECT c FROM Socio c", Socio.class);
        List<Socio> Articulos_recibidos = query.getResultList();
        ArrayList<Socio> lista = new ArrayList<>();
        for (Socio autor : Articulos_recibidos) {
            lista.add(autor);
        }
        return lista;
    }

    @Override
    public Socio buscarPorClavePrimaria(int id) throws MalformedURLException, XmlRpcException, Exception {
        TypedQuery<Socio> query = em.createQuery("SELECT c FROM Socio c WHERE c.id= " + id, Socio.class);
        List<Socio> Articulos_recibidos = query.getResultList();
        return Articulos_recibidos.get(0);
    }

    @Override
    public ArrayList<Socio> buscarPorTodasLasClavePrimaria(ArrayList<Integer> ids) throws MalformedURLException, XmlRpcException, Exception {
        ArrayList<Socio> lista = new ArrayList<>();
        for (Integer autor : ids) {
            lista.add(buscarPorClavePrimaria(autor));
        }
        return lista;
    }

    @Override
    public Integer insertarRegistro(Socio objeto) throws MalformedURLException, XmlRpcException, Exception {
        em.getTransaction().begin();
        em.persist(objeto);
        em.getTransaction().commit();
        return objeto.getId();
    }

    @Override
    public boolean actualizarRegistro(Socio objeto) throws MalformedURLException, XmlRpcException, Exception {
        Socio autor = buscarPorClavePrimaria(objeto.getId());
        em.getTransaction().begin();
        autor.setDNI(autor.getDNI());
        autor.setDireccion(autor.getDireccion());
        autor.setFecha_de_nacimiento(autor.getFecha_de_nacimiento());
        autor.setContrasenya(autor.getContrasenya());
        autor.setLocalidad(autor.getLocalidad());
        autor.setNombre(autor.getNombre());
        em.getTransaction().commit();
        return false;
    }

    @Override
    public boolean eliminarRegistro(Socio objeto) throws MalformedURLException, XmlRpcException, Exception {
        em.getTransaction().begin();
        em.remove(objeto);
        em.getTransaction().commit();
        return false;
    }

    @Override
    public boolean eliminarRegistro(int idObjeto) throws MalformedURLException, XmlRpcException, Exception {
        Socio employee = buscarPorClavePrimaria(idObjeto);
        em.getTransaction().begin();
        em.remove(employee);
        em.getTransaction().commit();
        return false;
    }

    @Override
    public ArrayList<Socio> obtenerLista(List<Object> listaObjetos) throws MalformedURLException, XmlRpcException, Exception {
        return null;

    }

    @Override
    public Integer[] insertarMasDeUnRegistro(ArrayList<Socio> listaObjetos) throws MalformedURLException, XmlRpcException, Exception {
        for (Socio autor : listaObjetos) {
            ConexionObjectDB.getConnectionem();
            ConexionObjectDB.getConnectionemf();
            insertarRegistro(autor);
            ConexionObjectDB.cerrar();
        }
        return null;
    }

}
