/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAOObjectDB;

import ConexionOdoo.ConexionObjectDB;
import Modelo.Editorial;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import org.GenericoDAO.GenericoDAO;
import org.apache.xmlrpc.XmlRpcException;

/**
 *
 * @author Usuario
 */
public class EditorialObjectDBDAO implements GenericoDAO<Editorial> {

    private static EntityManager em;

    public EditorialObjectDBDAO() throws Exception {
        em = ConexionObjectDB.getConnectionem();
    }

    @Override
    public ArrayList<Editorial> buscarTodos() throws MalformedURLException, XmlRpcException, Exception {
        TypedQuery<Editorial> query = em.createQuery("SELECT c FROM Editorial c", Editorial.class);
        List<Editorial> Articulos_recibidos = query.getResultList();
        ArrayList<Editorial> lista = new ArrayList<>();
        for (Editorial autor : Articulos_recibidos) {
            lista.add(autor);
        }
        return lista;
    }

    @Override
    public Editorial buscarPorClavePrimaria(int id) throws MalformedURLException, XmlRpcException, Exception {
        TypedQuery<Editorial> query = em.createQuery("SELECT c FROM Editorial c WHERE c.id= " + id, Editorial.class);
        List<Editorial> Articulos_recibidos = query.getResultList();
        return Articulos_recibidos.get(0);
    }

    @Override
    public ArrayList<Editorial> buscarPorTodasLasClavePrimaria(ArrayList<Integer> ids) throws MalformedURLException, XmlRpcException, Exception {
        ArrayList<Editorial> lista = new ArrayList<>();
        for (Integer autor : ids) {
            lista.add(buscarPorClavePrimaria(autor));
        }
        return lista;
    }

    @Override
    public Integer insertarRegistro(Editorial objeto) throws MalformedURLException, XmlRpcException, Exception {
        em.getTransaction().begin();
        em.persist(objeto);
        em.getTransaction().commit();
        return objeto.getId();
    }

    @Override
    public boolean actualizarRegistro(Editorial objeto) throws MalformedURLException, XmlRpcException, Exception {
        Editorial autor = buscarPorClavePrimaria(objeto.getId());
        em.getTransaction().begin();
        autor.setCIF(objeto.getCIF());
        autor.setDireccion(objeto.getFacebook());
        autor.setFacebook(objeto.getFacebook());
        autor.setInstagram(objeto.getInstagram());
        autor.setLocalidad(objeto.getLocalidad());
        autor.setNombre(objeto.getNombre());
        autor.setPagina_web(objeto.getPagina_web());
        autor.setTelefono(objeto.getTelefono());
        autor.setTwitter(objeto.getTwitter());
        em.getTransaction().commit();
        return false;
    }

    @Override
    public boolean eliminarRegistro(Editorial objeto) throws MalformedURLException, XmlRpcException, Exception {
        em.getTransaction().begin();
        em.remove(objeto);
        em.getTransaction().commit();
        return false;
    }

    @Override
    public boolean eliminarRegistro(int idObjeto) throws MalformedURLException, XmlRpcException, Exception {
        Editorial employee = buscarPorClavePrimaria(idObjeto);
        em.getTransaction().begin();
        em.remove(employee);
        em.getTransaction().commit();
        return true;
    }

    @Override
    public ArrayList<Editorial> obtenerLista(List<Object> listaObjetos) throws MalformedURLException, XmlRpcException, Exception {
        return null;

    }

    @Override
    public Integer[] insertarMasDeUnRegistro(ArrayList<Editorial> listaObjetos) throws MalformedURLException, XmlRpcException, Exception {
        for (Editorial autor : listaObjetos) {
            ConexionObjectDB.getConnectionem();
            ConexionObjectDB.getConnectionemf();
            insertarRegistro(autor);
            ConexionObjectDB.cerrar();
        }
        return null;
    }

}
