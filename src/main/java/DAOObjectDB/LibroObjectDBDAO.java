/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAOObjectDB;

import ConexionOdoo.ConexionObjectDB;
import Modelo.Editorial;
import Modelo.Libro;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import org.GenericoDAO.GenericoDAO;
import org.apache.xmlrpc.XmlRpcException;

/**
 *
 * @author Usuario
 */
public class LibroObjectDBDAO implements GenericoDAO<Libro> {

    private static EntityManager em;

    public LibroObjectDBDAO() throws Exception {
        em = ConexionObjectDB.getConnectionem();
    }

    @Override
    public ArrayList<Libro> buscarTodos() throws MalformedURLException, XmlRpcException, Exception {
        TypedQuery<Libro> query = em.createQuery("SELECT c FROM Libro c", Libro.class);
        List<Libro> Articulos_recibidos = query.getResultList();
        ArrayList<Libro> lista = new ArrayList<>();
        for (Libro autor : Articulos_recibidos) {
            lista.add(autor);
        }
        return lista;
    }

    @Override
    public Libro buscarPorClavePrimaria(int id) throws MalformedURLException, XmlRpcException, Exception {
        TypedQuery<Libro> query = em.createQuery("SELECT c FROM Libro c WHERE c.id= " + id, Libro.class);
        List<Libro> Articulos_recibidos = query.getResultList();
        return Articulos_recibidos.get(0);
    }

    @Override
    public ArrayList<Libro> buscarPorTodasLasClavePrimaria(ArrayList<Integer> ids) throws MalformedURLException, XmlRpcException, Exception {
        ArrayList<Libro> lista = new ArrayList<>();
        for (Integer autor : ids) {
            lista.add(buscarPorClavePrimaria(autor));
        }
        return lista;
    }

    @Override
    public Integer insertarRegistro(Libro objeto) throws MalformedURLException, XmlRpcException, Exception {
        em.getTransaction().begin();
        em.persist(objeto);
        em.getTransaction().commit();
        return objeto.getId();
    }

    @Override
    public boolean actualizarRegistro(Libro objeto) throws MalformedURLException, XmlRpcException, Exception {
        Libro autor = buscarPorClavePrimaria(objeto.getId());
        em.getTransaction().begin();
        autor.setAutor(objeto.getAutor());
        autor.setCantidad(objeto.getCantidad());
        autor.setEditorial(objeto.getEditorial());
        autor.setEstado_del_libro(objeto.getEstado_del_libro());
        autor.setISBN(objeto.getISBN());
        autor.setLocalizacion(objeto.getLocalizacion());
        autor.setPaginas(objeto.getPaginas());
        autor.setTipo_del_libro(objeto.getTipo_del_libro());
        autor.setTitulo(objeto.getTitulo());
        em.getTransaction().commit();
        return false;
    }

    @Override
    public boolean eliminarRegistro(Libro objeto) throws MalformedURLException, XmlRpcException, Exception {
        em.getTransaction().begin();
        em.remove(objeto);
        em.getTransaction().commit();
        return false;
    }

    @Override
    public boolean eliminarRegistro(int idObjeto) throws MalformedURLException, XmlRpcException, Exception {
        Libro employee = buscarPorClavePrimaria(idObjeto);
        em.getTransaction().begin();
        em.remove(employee);
        em.getTransaction().commit();
        return false;
    }

    @Override
    public ArrayList<Libro> obtenerLista(List<Object> listaObjetos) throws MalformedURLException, XmlRpcException, Exception {
        return null;
    }

    @Override
    public Integer[] insertarMasDeUnRegistro(ArrayList<Libro> listaObjetos) throws MalformedURLException, XmlRpcException, Exception {
        for (Libro autor : listaObjetos) {
            ConexionObjectDB.getConnectionem();
            ConexionObjectDB.getConnectionemf();
            insertarRegistro(autor);
            ConexionObjectDB.cerrar();
        }
        return null;
    }

}
