/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAOObjectDB;

import ConexionOdoo.ConexionObjectDB;
import Modelo.Retirada;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import org.GenericoDAO.GenericoDAO;
import org.apache.xmlrpc.XmlRpcException;

/**
 *
 * @author Usuario
 */
public class RetiradaObjectDBDAO implements GenericoDAO<Retirada> {

    private static EntityManager em;

    public RetiradaObjectDBDAO() throws Exception {
        em = ConexionObjectDB.getConnectionem();
    }

    @Override
    public ArrayList<Retirada> buscarTodos() throws MalformedURLException, XmlRpcException, Exception {
        em.getTransaction().begin();
        ArrayList<Retirada> lista = new ArrayList<>();
        List<Retirada> Articulos_recibidos = new ArrayList<>();
        try {
            ConexionObjectDB.cerrar();
            em = ConexionObjectDB.getConnectionem();
            TypedQuery<Retirada> query = em.createQuery("SELECT c FROM Retirada c", Retirada.class);
            Articulos_recibidos = query.getResultList();
            lista = new ArrayList<>();
            for (Retirada autor : Articulos_recibidos) {
                lista.add(autor);
            }
        } catch (Exception e) {
            ConexionObjectDB.cerrar();
            em = ConexionObjectDB.getConnectionem();
        }
        return lista;
    }

    @Override
    public Retirada buscarPorClavePrimaria(int id) throws MalformedURLException, XmlRpcException, Exception {
        TypedQuery<Retirada> query = em.createQuery("SELECT c FROM Retirada c WHERE c.id= " + id, Retirada.class);
        List<Retirada> Articulos_recibidos = query.getResultList();
        return Articulos_recibidos.get(0);
    }

    @Override
    public ArrayList<Retirada> buscarPorTodasLasClavePrimaria(ArrayList<Integer> ids) throws MalformedURLException, XmlRpcException, Exception {
        ArrayList<Retirada> lista = new ArrayList<>();
        for (Integer autor : ids) {
            lista.add(buscarPorClavePrimaria(autor));
        }
        return lista;
    }

    @Override
    public Integer insertarRegistro(Retirada objeto) throws MalformedURLException, XmlRpcException, Exception {
        em.getTransaction().begin();
        em.persist(objeto);
        em.getTransaction().commit();
        return objeto.getId();
    }

    @Override
    public boolean actualizarRegistro(Retirada objeto) throws MalformedURLException, XmlRpcException, Exception {
        Retirada autor = buscarPorClavePrimaria(objeto.getId());
        em.getTransaction().begin();
        autor.setEstado_del_libro(objeto.getEstado_del_libro());
        autor.setFecha_que_se_devuelve_el_carnet(objeto.getFecha_que_se_devuelve_el_carnet());
        autor.setFecha_que_se_retira_el_carnet(objeto.getFecha_que_se_retira_el_carnet());
        autor.setSocio(objeto.getSocio());
        em.getTransaction().commit();
        return false;
    }

    @Override
    public boolean eliminarRegistro(Retirada objeto) throws MalformedURLException, XmlRpcException, Exception {
        em.getTransaction().begin();
        em.remove(objeto);
        em.getTransaction().commit();
        return false;
    }

    @Override
    public boolean eliminarRegistro(int idObjeto) throws MalformedURLException, XmlRpcException, Exception {
        Retirada employee = buscarPorClavePrimaria(idObjeto);
        em.getTransaction().begin();
        em.remove(employee);
        em.getTransaction().commit();
        return false;
    }

    @Override
    public ArrayList<Retirada> obtenerLista(List<Object> listaObjetos) throws MalformedURLException, XmlRpcException, Exception {
        return null;

    }

    @Override
    public Integer[] insertarMasDeUnRegistro(ArrayList<Retirada> listaObjetos) throws MalformedURLException, XmlRpcException, Exception {
        em.getTransaction().begin();
        for (Retirada autor : listaObjetos) {

            try {
                ConexionObjectDB.cerrar();
                em = ConexionObjectDB.getConnectionem();
                insertarRegistro(autor);
            } catch (Exception e) {
                ConexionObjectDB.cerrar();
                em = ConexionObjectDB.getConnectionem();
            }

        }
        return null;
    }

}
