/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAOObjectDB;

import ConexionOdoo.ConexionObjectDB;
import Modelo.Tipo;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import org.GenericoDAO.GenericoDAO;
import org.apache.xmlrpc.XmlRpcException;

/**
 *
 * @author Usuario
 */
public class TipoObjectDBDAO implements GenericoDAO<Tipo> {
    
    private static EntityManager em;

    public TipoObjectDBDAO() throws Exception {
        em = ConexionObjectDB.getConnectionem();
    }

    @Override
    public ArrayList<Tipo> buscarTodos() throws MalformedURLException, XmlRpcException, Exception {
        TypedQuery<Tipo> query = em.createQuery("SELECT c FROM Tipo c", Tipo.class);
        List<Tipo> Articulos_recibidos = query.getResultList();
        ArrayList<Tipo> lista = new ArrayList<>();
        for (Tipo autor : Articulos_recibidos) {
            lista.add(autor);
        }
        return lista;
    }

    @Override
    public Tipo buscarPorClavePrimaria(int id) throws MalformedURLException, XmlRpcException, Exception {
        TypedQuery<Tipo> query = em.createQuery("SELECT c FROM Tipo c WHERE c.id= " + id, Tipo.class);
        List<Tipo> Articulos_recibidos = query.getResultList();
        return Articulos_recibidos.get(0);
    }

    @Override
    public ArrayList<Tipo> buscarPorTodasLasClavePrimaria(ArrayList<Integer> ids) throws MalformedURLException, XmlRpcException, Exception {
        ArrayList<Tipo> lista = new ArrayList<>();
        for (Integer autor : ids) {
            lista.add(buscarPorClavePrimaria(autor));
        }
        return lista;
    }

    @Override
    public Integer insertarRegistro(Tipo objeto) throws MalformedURLException, XmlRpcException, Exception {
        em.getTransaction().begin();
        em.persist(objeto);
        em.getTransaction().commit();
        return objeto.getId();
    }

    @Override
    public boolean actualizarRegistro(Tipo objeto) throws MalformedURLException, XmlRpcException, Exception {
        Tipo autor = buscarPorClavePrimaria(objeto.getId());
        em.getTransaction().begin();
        autor.setDescripcion(autor.getDescripcion());
        autor.setNombre(autor.getNombre());
        em.getTransaction().commit();
        return false;
    }

    @Override
    public boolean eliminarRegistro(Tipo objeto) throws MalformedURLException, XmlRpcException, Exception {
        em.getTransaction().begin();
        em.remove(objeto);
        em.getTransaction().commit();
        return false;
    }

    @Override
    public boolean eliminarRegistro(int idObjeto) throws MalformedURLException, XmlRpcException, Exception {
        Tipo employee = buscarPorClavePrimaria(idObjeto);
        em.getTransaction().begin();
        em.remove(employee);
        em.getTransaction().commit();
        return false;
    }

    @Override
    public ArrayList<Tipo> obtenerLista(List<Object> listaObjetos) throws MalformedURLException, XmlRpcException, Exception {
        return null;
        
    }

    @Override
    public Integer[] insertarMasDeUnRegistro(ArrayList<Tipo> listaObjetos) throws MalformedURLException, XmlRpcException, Exception {
        for (Tipo autor : listaObjetos) {
            ConexionObjectDB.getConnectionem();
            ConexionObjectDB.getConnectionemf();
            insertarRegistro(autor);
            ConexionObjectDB.cerrar();
        }
        return null;
    }
    
}
