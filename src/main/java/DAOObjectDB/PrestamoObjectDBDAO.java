/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAOObjectDB;

import ConexionOdoo.ConexionObjectDB;
import Modelo.Prestamo;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import org.GenericoDAO.GenericoDAO;
import org.apache.xmlrpc.XmlRpcException;

/**
 *
 * @author Usuario
 */
public class PrestamoObjectDBDAO implements GenericoDAO<Prestamo> {

    private static EntityManager em;

    public PrestamoObjectDBDAO() throws Exception {
        em = ConexionObjectDB.getConnectionem();
    }

    @Override
    public ArrayList<Prestamo> buscarTodos() throws MalformedURLException, XmlRpcException, Exception {
        TypedQuery<Prestamo> query = em.createQuery("SELECT c FROM Prestamo c", Prestamo.class);
        List<Prestamo> Articulos_recibidos = query.getResultList();
        ArrayList<Prestamo> lista = new ArrayList<>();
        for (Prestamo autor : Articulos_recibidos) {
            lista.add(autor);
        }
        return lista;
    }

    @Override
    public Prestamo buscarPorClavePrimaria(int id) throws MalformedURLException, XmlRpcException, Exception {
        TypedQuery<Prestamo> query = em.createQuery("SELECT c FROM Prestamo c WHERE c.id= " + id, Prestamo.class);
        List<Prestamo> Articulos_recibidos = query.getResultList();
        return Articulos_recibidos.get(0);
    }

    @Override
    public ArrayList<Prestamo> buscarPorTodasLasClavePrimaria(ArrayList<Integer> ids) throws MalformedURLException, XmlRpcException, Exception {
        ArrayList<Prestamo> lista = new ArrayList<>();
        for (Integer autor : ids) {
            lista.add(buscarPorClavePrimaria(autor));
        }
        return lista;
    }

    @Override
    public Integer insertarRegistro(Prestamo objeto) throws MalformedURLException, XmlRpcException, Exception {
        em.getTransaction().begin();
        em.persist(objeto);
        em.getTransaction().commit();
        return objeto.getId();
    }

    @Override
    public boolean actualizarRegistro(Prestamo objeto) throws MalformedURLException, XmlRpcException, Exception {
        Prestamo autor = buscarPorClavePrimaria(objeto.getId());
        em.getTransaction().begin();
        autor.setEsta_devuelto(objeto.isEsta_devuelto());
        autor.setFecha_de_prestamo(objeto.getFecha_de_prestamo());
        autor.setFecha_que_se_ha_devuelto(objeto.getFecha_que_se_ha_devuelto());
        autor.setFecha_que_se_tiene_que_devolver(objeto.getFecha_que_se_tiene_que_devolver());
        autor.setLibro_prestado(objeto.getLibro_prestado());
        autor.setSocio(objeto.getSocio());
        em.getTransaction().commit();
        return false;
    }

    @Override
    public boolean eliminarRegistro(Prestamo objeto) throws MalformedURLException, XmlRpcException, Exception {
        em.getTransaction().begin();
        em.remove(objeto);
        em.getTransaction().commit();
        return false;
    }

    @Override
    public boolean eliminarRegistro(int idObjeto) throws MalformedURLException, XmlRpcException, Exception {
        Prestamo employee = buscarPorClavePrimaria(idObjeto);
        em.getTransaction().begin();
        em.remove(employee);
        em.getTransaction().commit();
        return false;
    }

    @Override
    public ArrayList<Prestamo> obtenerLista(List<Object> listaObjetos) throws MalformedURLException, XmlRpcException, Exception {
        return null;

    }

    @Override
    public Integer[] insertarMasDeUnRegistro(ArrayList<Prestamo> listaObjetos) throws MalformedURLException, XmlRpcException, Exception {
        for (Prestamo autor : listaObjetos) {
            ConexionObjectDB.getConnectionem();
            ConexionObjectDB.getConnectionemf();
            insertarRegistro(autor);
            ConexionObjectDB.cerrar();
        }
        return null;
    }

}
