/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAOObjectDB;

import ConexionOdoo.ConexionObjectDB;
import Modelo.Libro;
import Modelo.Linea_pedido;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import org.GenericoDAO.GenericoDAO;
import org.apache.xmlrpc.XmlRpcException;

/**
 *
 * @author Usuario
 */
public class Linea_PedidoObjectDBDAO implements GenericoDAO<Linea_pedido> {

    private static EntityManager em;

    public Linea_PedidoObjectDBDAO() throws Exception {
        em = ConexionObjectDB.getConnectionem();
    }

    @Override
    public ArrayList<Linea_pedido> buscarTodos() throws MalformedURLException, XmlRpcException, Exception {
        em.getTransaction().begin();
        ArrayList<Linea_pedido> lista = new ArrayList<>();
        List<Linea_pedido> Articulos_recibidos = new ArrayList<>();
        try {
            ConexionObjectDB.cerrar();
            em = ConexionObjectDB.getConnectionem();
            TypedQuery<Linea_pedido> query = em.createQuery("SELECT c FROM Linea_pedido c", Linea_pedido.class);
            Articulos_recibidos = query.getResultList();
            lista = new ArrayList<>();
            for (Linea_pedido autor : Articulos_recibidos) {
                lista.add(autor);
            }
        } catch (Exception e) {
            ConexionObjectDB.cerrar();
            em = ConexionObjectDB.getConnectionem();
        }
        return lista;
    }

    @Override
    public Linea_pedido buscarPorClavePrimaria(int id) throws MalformedURLException, XmlRpcException, Exception {
        TypedQuery<Linea_pedido> query = em.createQuery("SELECT c FROM Linea_pedido c WHERE c.id= " + id, Linea_pedido.class);
        List<Linea_pedido> Articulos_recibidos = query.getResultList();
        return Articulos_recibidos.get(0);
    }

    @Override
    public ArrayList<Linea_pedido> buscarPorTodasLasClavePrimaria(ArrayList<Integer> ids) throws MalformedURLException, XmlRpcException, Exception {
        ArrayList<Linea_pedido> lista = new ArrayList<>();
        for (Integer autor : ids) {
            lista.add(buscarPorClavePrimaria(autor));
        }
        return lista;
    }

    @Override
    public Integer insertarRegistro(Linea_pedido objeto) throws MalformedURLException, XmlRpcException, Exception {
        em.getTransaction().begin();
        em.persist(objeto);
        em.getTransaction().commit();
        return objeto.getId();
    }

    @Override
    public boolean actualizarRegistro(Linea_pedido objeto) throws MalformedURLException, XmlRpcException, Exception {
        Linea_pedido autor = buscarPorClavePrimaria(objeto.getId());
        em.getTransaction().begin();
        autor.setCantidad(autor.getCantidad());
        autor.setLibro(autor.getLibro());
        autor.setPedido(autor.getPedido());
        autor.setPrecio(autor.getPrecio());
        autor.setTotal_linea(autor.getTotal_linea());
        em.getTransaction().commit();
        return false;
    }

    @Override
    public boolean eliminarRegistro(Linea_pedido objeto) throws MalformedURLException, XmlRpcException, Exception {
        em.getTransaction().begin();
        em.remove(objeto);
        em.getTransaction().commit();
        return false;
    }

    @Override
    public boolean eliminarRegistro(int idObjeto) throws MalformedURLException, XmlRpcException, Exception {
        Linea_pedido employee = buscarPorClavePrimaria(idObjeto);
        em.getTransaction().begin();
        em.remove(employee);
        em.getTransaction().commit();
        return false;
    }

    @Override
    public ArrayList<Linea_pedido> obtenerLista(List<Object> listaObjetos) throws MalformedURLException, XmlRpcException, Exception {
        return null;

    }

    @Override
    public Integer[] insertarMasDeUnRegistro(ArrayList<Linea_pedido> listaObjetos) throws MalformedURLException, XmlRpcException, Exception {
        for (Linea_pedido autor : listaObjetos) {
            ConexionObjectDB.getConnectionem();
            ConexionObjectDB.getConnectionemf();
            insertarRegistro(autor);
            ConexionObjectDB.cerrar();
        }
        return null;
    }

}
