/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utilidades;

import Modelo.*;
import DAO.LibroDAO;
import DAO.PedidoDAO;
import DAO.UsuarioDAO;
import java.io.File;
import java.util.Date;
import javafx.scene.image.Image;
import java.util.ArrayList;
import java.util.HashMap;
import org.GenericoDAO.GenericoDAO;
import DAO.*;
import DAOObjectDB.*;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 *
 * @author batoi
 */
public class DAOMetodos {

    private static final PedidoDAO pedidoDAO = new PedidoDAO();

    public static final Image NO_IMAGE = new Image(new File("src/main/resources/imagenes/found.png").toURI().toString());
    
    public static String fecha_correcta(Date fecha) {
        return (fecha.getYear() + 1900) + "-" + (fecha.getMonth() + 1) + "-" + fecha.getDate();

    }

    public static String transformar_texto(Object texto) {
        String paginaweb = Constantes.VACIO;
        try {
            paginaweb = (String) texto;
        } catch (ClassCastException c) {
            paginaweb = Constantes.VACIO;
        }
        return paginaweb;
    }

    public static Date obtener_fecha(Object fecha_a_obtener) {
        Date fecha;
        try {
            fecha = java.sql.Date.valueOf((String) fecha_a_obtener);
        } catch (Exception e) {
            fecha = new Date();
        }
        return fecha;
    }

    public static Date obtener_fecha(String fecha_a_obtener) {
        Date fecha = new Date();
        String[] valores = fecha_a_obtener.split("-");
        fecha.setDate(Integer.parseInt(valores[2]));
        fecha.setMonth(Integer.parseInt(valores[1]) - 1);
        fecha.setYear(Integer.parseInt(valores[0]) - 1900);
        return fecha;
    }

//    public static Image obtener_imagen(Object field) {
//        Image value = NO_IMAGE;
//
//        if (field instanceof String) {
//            String base64String = (String) field;
//            String[] strings = base64String.split(",");
//            value = new Image(new ByteArrayInputStream(DatatypeConverter.parseBase64Binary(strings[0])));
//        }
//
//        return value;
//    }

    public static ArrayList<Integer> relacion_uno_a_muchos(Object map) {
        ArrayList<Integer> values = new ArrayList();
        Object[] objects = (Object[]) map;
        if (objects.length > 0) {
            for (Object object : objects) {
                values.add((Integer) object);
            }
        }
        return values;
    }

    public static Object[] insertar_relacion_uno_a_muchos(ArrayList<Linea_pedido> lineas_pedido) {
        int tamanyo = lineas_pedido.size();
        Object[] objetos = new Object[tamanyo];

        for (int contador = 0; contador < tamanyo; contador++) {
            objetos[contador] = lineas_pedido.get(contador).getId();
        }

        return objetos;
    }

    public static Object[] insertar_relacion_uno_a_muchos_libro(ArrayList<Libro> lineas_pedido) {
        int tamanyo = lineas_pedido.size();
        Object[] objetos = new Object[tamanyo];

        for (int contador = 0; contador < tamanyo; contador++) {
            objetos[contador] = lineas_pedido.get(contador).getId();
        }

        return objetos;
    }

    public static Object Relacion_uno_a_muchos(HashMap map, String field, String classNameDAO) throws Exception {
        int value = 0;

        Object[] objects = (Object[]) map.get(field);
        if (objects.length > 0) {
            value = (Integer) objects[0];
        }

        return ((GenericoDAO) Class.forName(classNameDAO).getDeclaredConstructor().newInstance())
                .buscarPorClavePrimaria(value);
    }

    public static Object Relacion_uno_a_muchos(HashMap map, String field, UsuarioDAO classNameDAO) throws Exception {
        int value = 0;

        Object[] objects = (Object[]) map.get(field);
        if (objects.length > 0) {
            value = (Integer) objects[0];
        }

        return classNameDAO.buscarPorClavePrimaria(value);
    }

    public static Object Relacion_uno_a_muchos(HashMap map, String field, LibroDAO classNameDAO) throws Exception {
        int value = 0;

        Object[] objects = (Object[]) map.get(field);
        if (objects.length > 0) {
            value = (Integer) objects[0];
        }

        return classNameDAO.buscarPorClavePrimaria(value);
    }

    public static Object Relacion_uno_a_muchos(HashMap map, String field, PedidoDAO classNameDAO) throws Exception {
        int value = 0;

        Object[] objects = (Object[]) map.get(field);
        if (objects.length > 0) {
            value = (Integer) objects[0];
        }

        return pedidoDAO.buscarPorClavePrimaria(value);
    }
    
    public static int Relacion_uno_a_muchos2(HashMap map, String field, PedidoDAO classNameDAO) throws Exception {
        int value = 0;

        Object[] objects = (Object[]) map.get(field);
        if (objects.length > 0) {
            value = (Integer) objects[0];
        }

        return value;
    }
    
    public static String desencriptar_contrasenya(String texto) {
		String desencripcion="";
		MessageDigest digest;
		try {
			digest = MessageDigest.getInstance("SHA-1");
		
		digest.reset();
		digest.update(texto.getBytes("utf8"));
		desencripcion = String.format("%040x", new BigInteger(1, digest.digest()));
		} catch (NoSuchAlgorithmException e) {
			// TODO Bloque catch generado autom�ticamente
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Bloque catch generado autom�ticamente
			e.printStackTrace();
		}
		return desencripcion;
		
	}


}
