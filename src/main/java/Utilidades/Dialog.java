/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utilidades;

import static ControladorVista.ControladorGlobal.ALTO;
import static ControladorVista.ControladorGlobal.ANCHO;
import static ControladorVista.ControladorGlobal.imagen_menu_bar;
import static ControladorVista.ControladorGlobal.libroDAO;
import Modelo.Libro;
import Thread.ThreadAbrirArchivo;
import com.jfoenix.controls.JFXButton;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextInputDialog;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.paint.Color;
import org.apache.xmlrpc.XmlRpcException;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import com.jfoenix.controls.JFXTextArea;
import com.mycompany.proyectobiblioteca.App;
import java.awt.Desktop;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.io.File;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Hyperlink;
import javafx.scene.input.Clipboard;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.controlsfx.control.Notifications;

/**
 *
 * @author batoi
 */
public class Dialog {

    public static boolean Dialogo_de_confirmacion(String contenido_dialogo) {
        ResourceBundle recursos = App.ObtenerRecursos();
        Alert alert = new Alert(AlertType.CONFIRMATION);
        //alert.setTitle("Confirmation Dialog");
        alert.setTitle(recursos.getString("tituloConfirmacion"));
        //alert.setHeaderText("Look, a Confirmation Dialog");
        alert.setHeaderText(recursos.getString("cabeceraConfirmacion"));
        alert.setContentText(contenido_dialogo);

        Optional<ButtonType> result = alert.showAndWait();
        return result.get() == ButtonType.OK;
    }

    public synchronized static void notificacion(Libro libro) {
        //System.out.println(libro);
        ResourceBundle recursos = App.ObtenerRecursos();
        Notifications notification = Notifications.create()
                //.title("titulo " + libro.getTitulo())
                .title(recursos.getString("tituloNotificacion") + libro.getTitulo())
                //.text("El libro " + libro.getTitulo() + System.lineSeparator() + (libro.isEsta_prestado() ? " ha sido prestado " : " ha sido devuelto"))
                .text(recursos.getString("tituloLibro") + libro.getTitulo() + System.lineSeparator() + (libro.isEsta_prestado() ? recursos.getString("prestado") : recursos.getString("noprestado")))
                .graphic(null)
                .position(Pos.BASELINE_RIGHT).hideAfter(Duration.INDEFINITE)
                .onAction((ActionEvent arg0) -> {

                });

        notification.showConfirm();
    }

    public static boolean Dialogo_de_confirmacion(String contenido_dialogo, File archivo) {
        ResourceBundle recursos = App.ObtenerRecursos();
        Alert alert = new Alert(AlertType.CONFIRMATION);
        //alert.setTitle("Confirmation Dialog");
        alert.setTitle(recursos.getString("tituloConfirmacion"));
        alert.setHeaderText(recursos.getString("cabeceraConfirmacion"));
        alert.setContentText(contenido_dialogo);
        //"Abrir archivo"
        Label label = new Label(recursos.getString("abrirArchivo"));
        Hyperlink link = new Hyperlink(archivo.getName());
        //"Carpeta "
        Hyperlink link_archivo = new Hyperlink(recursos.getString("carpeta") + archivo.getName());
        StackPane stackPane = new StackPane();
        HBox hbox = new HBox();
        hbox.getChildren().setAll(label, link, link_archivo);
        link_archivo.setVisible(Desktop.getDesktop().isSupported(Desktop.Action.BROWSE_FILE_DIR));
        stackPane.getChildren().setAll(hbox);
        alert.getDialogPane().setContent(stackPane);
        link.setOnAction((ActionEvent e) -> {
            new ThreadAbrirArchivo(archivo, Constantes.BOOLEAN_POR_DEFECTO).start();
        });
        link_archivo.setOnAction((ActionEvent e) -> {
            new ThreadAbrirArchivo(archivo, true).start();
        });

        Optional<ButtonType> result = alert.showAndWait();
        return result.get() == ButtonType.OK;
    }

    public static Libro Dialogo_de_eleccion(String contenido_dialogo, ObservableList<Libro> socio) throws XmlRpcException, Exception {
        ResourceBundle recursos = App.ObtenerRecursos();
        List<Libro> choices = new ArrayList<>();
        ArrayList<Libro> lista_libros_temporal = libroDAO.buscarTodos();
        for (Libro libro : socio) {
            lista_libros_temporal.remove(libro);
        }

        choices = lista_libros_temporal;

        ChoiceDialog<Libro> dialog_result = new ChoiceDialog<>(choices.get(0), choices);
        //dialog_result.setTitle("Choice Dialog");
        dialog_result.setTitle(recursos.getString("dialogoDeEleccion"));
        //dialog_result.setHeaderText("Look, a Choice Dialog");
        dialog_result.setHeaderText(recursos.getString("cuadroDeDialogo"));
        //dialog_result.setContentText("Choose your letter:");
        dialog_result.setContentText(recursos.getString("eligeUnaOpcion"));

// Traditional way to get the response value.
        Optional<Libro> result = dialog_result.showAndWait();
        return result.isPresent() ? result.get() : null;
    }

    public static void Dialogo_de_jfoenix3() {
        JFXDialog dialog = new JFXDialog();
        dialog.setContent(new Label("Content"));
        dialog.show();
    }

    public static void Dialogo_de_jfoenix2() {
        JFXDialogLayout layout = new JFXDialogLayout();
        layout.setHeading(new Text("Confirmar"));
        layout.setBody(new Text("¿Está seguro que desea salir?"));
        StackPane stackpane = new StackPane();
        JFXDialog dialog = new JFXDialog(stackpane, layout, JFXDialog.DialogTransition.CENTER);
        JFXButton yeap = new JFXButton("OK");
        yeap.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                dialog.close();
                App.stage.hide();
            }
        });
        JFXButton nope = new JFXButton("Cancelar");
        nope.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                dialog.close();
            }
        });
        layout.setActions(nope, yeap);
        dialog.show();
    }

    public static void Dialogo_de_jfoenix() {
        Stage primarystage = new Stage();
        StackPane root = new StackPane();
        JFXButton jfxButton = new JFXButton("SHOW!");
        jfxButton.setStyle("-fx-background-color: green; -fx-text-fill: white");
        BorderPane borderPane = new BorderPane();
        borderPane.setCenter(jfxButton);
        root.getChildren().addAll(borderPane);

        JFXDialog jfxDialog = new JFXDialog();
        JFXDialogLayout content = new JFXDialogLayout();
        VBox vBox = new VBox();
        vBox.setSpacing(10);
        HBox hBox = new HBox();
        Label label = new Label("Are you sure want to delete!");
        hBox.getChildren().addAll(label);
        HBox hBox2 = new HBox();
        JFXButton jfxButton1 = new JFXButton("Cancel");
        jfxButton1.setStyle("-fx-background-color: #eee; -fx-text-fill: #333");
        JFXButton jfxButton2 = new JFXButton("Yes");
        jfxButton2.setStyle("-fx-background-color: red; -fx-text-fill: white");
        hBox2.getChildren().addAll(jfxButton1, jfxButton2);
        vBox.getChildren().addAll(hBox, hBox2);
        content.setBody(vBox);
        jfxDialog.setContent(content);
        jfxDialog.setDialogContainer(root);

        Scene scene = new Scene(root, 700, 600);
//        primaryStage.setMaximized(true);
        jfxButton.setOnAction(act -> {
            jfxDialog.show();
        });
        primarystage.setScene(scene);
        primarystage.setTitle("JavaFX dialog Material Design");
        primarystage.show();
    }

    public static Color Dialogo_de_color() {
        ResourceBundle recursos = App.ObtenerRecursos();
        Alert alert = new Alert(AlertType.INFORMATION);
        //alert.setTitle("Seleccion de color");
        //alert.setHeaderText("Fondo de la aplicacion");
        //alert.setContentText("Elige un color para el fondo de la aplicacion.");

        alert.setTitle(recursos.getString("seleccionDeColor"));
        alert.setHeaderText(recursos.getString("fondoDeLaAplicacion"));
        alert.setContentText(recursos.getString("eligeUnColor"));

        ColorPicker color_picker = new ColorPicker();
        alert.getDialogPane().setContent(color_picker);
        alert.showAndWait();
        Color colorSeleccionado = color_picker.getValue();
        return colorSeleccionado;
    }

    public static boolean es_doble_click(MouseEvent evento) {

        return evento.getClickCount() == 2;

    }

    public static boolean es_boton_izquierdo(MouseEvent evento) {

        return evento.getButton() == MouseButton.PRIMARY;

    }

    public static String Dialogo_de_textField() {
        ResourceBundle recursos = App.ObtenerRecursos();
        TextInputDialog dialog = new TextInputDialog(Constantes.VACIO);
//        dialog.setTitle("Cambiar de nombre al título de la aplicación");
//        dialog.setHeaderText("Cambio de nombre");
//        dialog.setContentText("Pon otro nombre paar la aplicación:");

        dialog.setTitle(recursos.getString("cambiarTitulo"));
        dialog.setHeaderText(recursos.getString("cambioDeNombre"));
        dialog.setContentText(recursos.getString("ponerOtroNombre"));

// Traditional way to get the response value.
        Optional<String> result = dialog.showAndWait();

        return result.isPresent() ? result.get() : null;
    }

    public static void Mensaje_de_excepcion(Exception excepcion) {
        ResourceBundle recursos = App.ObtenerRecursos();
        Alert alert = new Alert(AlertType.ERROR);
        //alert.setTitle("Exception Dialog");
        //alert.setHeaderText("Look, an Exception Dialog");

        alert.setTitle(recursos.getString("tituloExcepcion"));
        alert.setHeaderText(recursos.getString("excepcion"));
        alert.setContentText(excepcion.getMessage());

        Exception ex = new Exception(excepcion.getLocalizedMessage());

// Create expandable Exception.
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        ex.printStackTrace(pw);
        String exceptionText = sw.toString();

        //Label label = new Label("The exception stacktrace was:");
        Label label = new Label(recursos.getString("mensajeCompletoExcepcion"));
        JFXTextArea textArea = new JFXTextArea(exceptionText);
        Color color = Color.rgb(255, 218, 209);
        //Color color_letra = Color.rgb(148, 29, 0);
        textArea.setBackground(new Background(new BackgroundFill(color, null, null)));

        textArea.setEditable(Constantes.BOOLEAN_POR_DEFECTO);
        textArea.setWrapText(true);

        textArea.setMaxWidth(Double.MAX_VALUE);
        textArea.setMaxHeight(Double.MAX_VALUE);

        JFXButton copiarPortapapeles = new JFXButton("Copiar");
        copiarPortapapeles.setGraphic(imagen_menu_bar(App.MENU_BAR + "portapapeles.png", ANCHO, ALTO));
        copiarPortapapeles.setOnAction((arg0) -> {
            copiarEnPortapapeles(exceptionText);
        });
        GridPane.setVgrow(textArea, Priority.ALWAYS);
        GridPane.setHgrow(textArea, Priority.ALWAYS);

        GridPane expContent = new GridPane();
        expContent.setMaxWidth(Double.MAX_VALUE);
        expContent.add(label, 0, 0);
        expContent.add(textArea, 0, 1);
        expContent.add(copiarPortapapeles, 0, 2);

// Set expandable Exception into the dialog_result pane.
        alert.getDialogPane().setExpandableContent(expContent);

        alert.showAndWait();
    }

    public static void copiarEnPortapapeles(String copiar) {
        java.awt.datatransfer.Clipboard cb = Toolkit.getDefaultToolkit().getSystemClipboard();
        StringSelection ss = new StringSelection(copiar);
        cb.setContents(ss, ss);
    }

}
