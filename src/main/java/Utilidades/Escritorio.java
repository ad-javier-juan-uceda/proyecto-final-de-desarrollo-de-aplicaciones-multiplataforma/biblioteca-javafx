/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utilidades;

import java.awt.Desktop;

/**
 *
 * @author batoi
 */
public class Escritorio {
    
    private static Desktop escritorio;
    
    public Desktop getescritorio(){
        
        if (escritorio == null){
            escritorio = Desktop.getDesktop();
        }
        return escritorio;
    }
    
}
