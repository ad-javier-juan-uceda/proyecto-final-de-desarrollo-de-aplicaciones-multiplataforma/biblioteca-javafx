package com.mycompany.proyectobiblioteca;

//package com.mycompany.proyectobiblioteca;
import static ControladorVista.ControladorGlobal.copiaseguridad;
import static ControladorVista.ControladorGlobal.*;
import Modelo.Libro;
import Thread.ThreadCerrarDialog;
import Thread.ThreadProgreso;
import Thread.Threadcambios;
import Utilidades.Constantes;
import java.io.File;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.prefs.Preferences;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.image.Image;
import javafx.stage.FileChooser;
import javafx.stage.StageStyle;
import org.apache.xmlrpc.XmlRpcException;

/**
 * JavaFX App
 */
public class App extends Application {

    static Scene scene;
    static Preferences preferencias;
    public static Stage stage;
    public static Stage stage2;
    private static ArrayList<Threadcambios> listaThreads;
    public static final String SEPARADOR_ARCHIVOS = File.separator;
    //public static final String RUTA_IMAGENES = (new File(Constantes.VACIO).getAbsolutePath()) + SEPARADOR_ARCHIVOS + "src" + SEPARADOR_ARCHIVOS + "main" + SEPARADOR_ARCHIVOS + "resources" + SEPARADOR_ARCHIVOS + "imagenes" + SEPARADOR_ARCHIVOS;
    public static final String RUTA_IMAGENES = (new File("lib").getAbsolutePath()) + SEPARADOR_ARCHIVOS + "imagenes" + SEPARADOR_ARCHIVOS;
    public static final String ICONO = RUTA_IMAGENES + "icono" + SEPARADOR_ARCHIVOS;
    public static final String MENU_BAR = RUTA_IMAGENES + "menu-bar" + SEPARADOR_ARCHIVOS;
    public static final String MENU_BAR_ARCHIVO = MENU_BAR + "archivo" + SEPARADOR_ARCHIVOS;
    public static final String MENU_BAR_TIPO_IMPORTAR = MENU_BAR + "tipo_importar" + SEPARADOR_ARCHIVOS;
    public static final String MENU_BAR_PERSONALIZACION = MENU_BAR + "personalizacion" + SEPARADOR_ARCHIVOS;
    public static final String ICONO_IMAGEN = "libreria.jpg";

    @Override
    public void start(Stage stage) throws IOException {
        App.stage = stage;
        preferencias = Preferences.userRoot().node(Constantes.ARCHIVO_PREFERENCIAS);

        if (System.getProperty("os.name").toLowerCase().contains("windows")) {
            FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("splash.fxml"), ResourceBundle.getBundle("com.mycompany.proyectobiblioteca.traduccion", App.obtenerIdioma(App.preferencias.get(Constantes.IDIOMA, Constantes.IDIOMA_POR_DEFECTO))));
            Scene scene = new Scene((Parent) fxmlLoader.load());
            App.stage2 = new Stage();
            App.stage2.initStyle(StageStyle.UNDECORATED);
            App.stage2.setScene(scene);
            App.stage2.show();
        } else {
            preferencias = Preferences.userRoot().node(Constantes.ARCHIVO_PREFERENCIAS);
            scene = new Scene(loadFXML("autor"));
            App.stage.getIcons().setAll(new Image(new File(ICONO + ICONO_IMAGEN).toURI().toString()));
            App.stage.setScene(scene);
            App.stage.setMaximized(preferencias.getBoolean(Constantes.MAXIMIZADO, Constantes.BOOLEAN_POR_DEFECTO));
            App.stage.setFullScreen(preferencias.getBoolean(Constantes.PANTALLA_COMPLETA, Constantes.BOOLEAN_POR_DEFECTO));
            App.stage.setTitle(preferencias.get(Constantes.TITULO_APLICACION, Constantes.TITULO_APLICACION_POR_DEFECTO));
            App.stage.centerOnScreen();
            App.stage.maximizedProperty().addListener((ObservableValue<? extends Boolean> ov, Boolean t, Boolean t1) -> {
                preferencias.putBoolean(Constantes.MAXIMIZADO, t1);
            });

            App.stage.show();
        }
    }

    public synchronized static void cambiar_progreso(int progreso) {
        new ThreadProgreso(progreso).start();
    }

    public synchronized static void cerrar_dialog() {
        ThreadCerrarDialog thread = new ThreadCerrarDialog();
        thread.setPriority(Thread.MAX_PRIORITY);
        thread.start();
    }

    public static ResourceBundle ObtenerRecursos() {
        return ResourceBundle.getBundle("com.mycompany.proyectobiblioteca.traduccion", App.obtenerIdioma(Preferences.userRoot().node(Constantes.ARCHIVO_PREFERENCIAS).get(Constantes.IDIOMA, Constantes.IDIOMA_POR_DEFECTO)));
    }

    public static void Cambiar_Pantalla(String nombreFXML, Locale idioma) throws IOException {
        FXMLLoader fxmlLoader = null;
        StringBuilder archivo = new StringBuilder();

        archivo.append(nombreFXML).append(".fxml");

        fxmlLoader = new FXMLLoader(App.class.getResource(archivo.toString()), ResourceBundle.getBundle("com.mycompany.proyectobiblioteca.traduccion", idioma));
        scene = new Scene((Parent) fxmlLoader.load());

        App.stage.setScene(scene);
        App.stage.setMaximized(preferencias.getBoolean(Constantes.MAXIMIZADO, Constantes.BOOLEAN_POR_DEFECTO));
        App.stage.getIcons().setAll(new Image(new File(ICONO + ICONO_IMAGEN).toURI().toString()));
        App.stage.setFullScreen(preferencias.getBoolean(Constantes.PANTALLA_COMPLETA, Constantes.BOOLEAN_POR_DEFECTO));
        App.stage.setTitle(preferencias.get(Constantes.TITULO_APLICACION, Constantes.TITULO_APLICACION_POR_DEFECTO));
        App.stage.centerOnScreen();

        App.stage.maximizedProperty().addListener((ObservableValue<? extends Boolean> ov, Boolean t, Boolean t1) -> {
            preferencias.putBoolean(Constantes.MAXIMIZADO, t1);
        });

        stage.setScene(scene);
        stage.show();

    }

    public static File fileChooser(String titulo, ObservableList<FileChooser.ExtensionFilter> filtros) {

        FileChooser fc = new FileChooser();
        fc.setTitle(titulo);
        fc.setInitialDirectory(Constantes.CARPETA_PERSONAL);
        fc.getExtensionFilters().setAll(filtros);
        return fc.showOpenDialog(App.stage);
    }

    public static File fileChooser(String ELIGE_UNA_IMAGEN) {
        FileChooser fc = new FileChooser();
        fc.setTitle(ELIGE_UNA_IMAGEN);
        fc.setInitialDirectory(Constantes.CARPETA_PERSONAL);
        return fc.showOpenDialog(App.stage);
    }

    public static File guardar_fileChooser(String titulo) {

        FileChooser fc = new FileChooser();
        fc.setTitle(titulo);

        fc.setInitialDirectory(Constantes.CARPETA_PERSONAL);
        return fc.showSaveDialog(App.stage);
    }

    public static File guardar_fileChooser(String titulo, String tipo) {

        FileChooser fc = new FileChooser();
        fc.setTitle(titulo);
        fc.setInitialFileName(tipo);
        fc.setInitialDirectory(Constantes.CARPETA_PERSONAL);
        ObservableList<FileChooser.ExtensionFilter> filtros = FXCollections.observableArrayList();
        filtros.setAll(new FileChooser.ExtensionFilter("Archivos del tipo " + tipo, tipo));
        fc.getExtensionFilters().setAll(filtros);
        return fc.showSaveDialog(App.stage);
    }

    public static void putTitleAndIcons(String title) {
        App.stage.setTitle(title);
        App.stage.getIcons().add(new Image(title));
        App.stage.setScene(scene);
        App.stage.centerOnScreen();
    }

    static void setRoot(String fxml) throws IOException {
        scene.setRoot(loadFXML(fxml));
    }

    public static Locale obtenerIdioma(String locale) {
        Locale idioma;
        switch (locale) {
            case Constantes.IDIOMA_POR_DEFECTO:
                idioma = Locale.getDefault();
                break;
            case Constantes.INGLES:
                idioma = Locale.ENGLISH;
                break;
            case Constantes.FRANCES:
                idioma = Locale.FRENCH;
                break;
            case Constantes.ALEMAN:
                idioma = Locale.GERMANY;
                break;
            case Constantes.ITALIANO:
                idioma = Locale.ITALY;
                break;

            case Constantes.CATALAN:
                idioma = Locale.forLanguageTag("ca");
                break;

            case Constantes.VASCO:
                idioma = Locale.forLanguageTag("eu");
                break;

            case Constantes.SUECO:
                idioma = Locale.forLanguageTag("sv-SE");
                break;

            case Constantes.GALLEGO:
                idioma = Locale.forLanguageTag("gl");
                break;

            default:
                idioma = Locale.getDefault();
                break;
        }
        return idioma;
    }

    public static Parent loadFXML(String fxml) throws IOException {
        Locale idioma = obtenerIdioma(Preferences.userRoot().node(Constantes.ARCHIVO_PREFERENCIAS).get(Constantes.IDIOMA, Constantes.IDIOMA_POR_DEFECTO));
        return FXMLLoader.load(App.class.getResource(fxml + ".fxml"), ResourceBundle.getBundle("com.mycompany.proyectobiblioteca.traduccion", idioma));
    }

    public static void main(String[] args) {
        launch();
    }

    public static void crear_threads() {
        try {
            if (listaThreads != null) {
                for (Threadcambios libro : listaThreads) {
                    libro.interrupt();
                }
                listaThreads.clear();
            }
            listaThreads = new ArrayList();
            ArrayList<Libro> lista_libros = libroDAO.buscarTodos();

            for (Libro libro : lista_libros) {
                listaThreads.add(new Threadcambios(libro.getId()));
            }

            for (Threadcambios libro : listaThreads) {

                libro.setDaemon(true);
                libro.start();
            }

            App.stage.setOnCloseRequest(e -> {
                copiaseguridad.interrupt();
                if (listaThreads != null) {
                    for (Threadcambios libro : listaThreads) {
                        libro.interrupt();
                    }
                }
                Platform.exit();
                System.exit(0);
                App.stage.close();
                App.stage2.close();
            });

        } catch (XmlRpcException ex) {
        } catch (Exception ex) {
        }

    }

}
