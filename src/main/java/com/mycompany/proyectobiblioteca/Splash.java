/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.proyectobiblioteca;

import Utilidades.Constantes;
import Utilidades.Dialog;
import static com.mycompany.proyectobiblioteca.App.Cambiar_Pantalla;
import static com.mycompany.proyectobiblioteca.App.ICONO;
import static com.mycompany.proyectobiblioteca.App.ICONO_IMAGEN;
import static com.mycompany.proyectobiblioteca.App.cambiar_progreso;
import static com.mycompany.proyectobiblioteca.App.loadFXML;
import static com.mycompany.proyectobiblioteca.App.obtenerIdioma;
import static com.mycompany.proyectobiblioteca.App.preferencias;
import static com.mycompany.proyectobiblioteca.App.scene;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;
import javafx.application.Platform;
import javafx.application.Preloader;
import javafx.application.Preloader.StateChangeNotification.Type;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import javafx.application.Preloader;

import javafx.application.Preloader;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;

import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 *
 * @author batoi
 */
public class Splash implements Initializable {

    @FXML
    private AnchorPane fondoAplicacion;
    public static Stage stage;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        new SplashScreen().start();
    }

    class SplashScreen extends Thread {

        @Override
        public void run() {
            super.run(); //To change body of generated methods, choose Tools | Templates.
            try {
                Thread.sleep(5000);
                Platform.runLater(() -> {
                    try {
                        cambiar_progreso(10);
                        App.scene = new Scene(loadFXML("autor"));
                        App.crear_threads();
                        App.stage.getIcons().setAll(new Image(new File(ICONO + ICONO_IMAGEN).toURI().toString()));

                        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("autor.fxml"), ResourceBundle.getBundle("com.mycompany.proyectobiblioteca.traduccion", App.obtenerIdioma(App.preferencias.get(Constantes.IDIOMA, Constantes.IDIOMA_POR_DEFECTO))));
                        App.stage.setScene(new Scene((Parent) fxmlLoader.load()));
                        App.stage.setMaximized(App.preferencias.getBoolean(Constantes.MAXIMIZADO, Constantes.BOOLEAN_POR_DEFECTO));
                        App.stage.setFullScreen(App.preferencias.getBoolean(Constantes.PANTALLA_COMPLETA, Constantes.BOOLEAN_POR_DEFECTO));
                        App.stage.setTitle(App.preferencias.get(Constantes.TITULO_APLICACION, Constantes.TITULO_APLICACION_POR_DEFECTO));
                        App.stage.centerOnScreen();
                        App.stage.maximizedProperty().addListener((ObservableValue<? extends Boolean> ov, Boolean t, Boolean t1) -> {
                            App.preferencias.putBoolean(Constantes.MAXIMIZADO, t1);
                        });

                        fondoAplicacion.getScene().getWindow().hide();
                        App.stage2.close();
                    } catch (IOException ex) {
                        Dialog.Mensaje_de_excepcion(ex);
                    }

                });
            } catch (InterruptedException ex) {
                Dialog.Mensaje_de_excepcion(ex);
            }
        }

    }

}
