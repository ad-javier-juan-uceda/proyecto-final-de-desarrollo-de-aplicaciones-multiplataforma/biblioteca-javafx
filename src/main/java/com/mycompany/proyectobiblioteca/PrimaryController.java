package com.mycompany.proyectobiblioteca;

import ControladorVista.ControladorGlobal;
import Modelo.Libro;
import java.net.URL;
import java.util.ResourceBundle;

public class PrimaryController extends ControladorGlobal<Libro> {

    @Override
    public void Cargar_datos() {

    }

    @Override
    public void insertar() {

    }

    @Override
    public void modificar() {

    }

    @Override
    public void eliminar() {

    }

    @Override
    public void Exportar() {

    }

    @Override
    public void actualizar() {

    }

}
