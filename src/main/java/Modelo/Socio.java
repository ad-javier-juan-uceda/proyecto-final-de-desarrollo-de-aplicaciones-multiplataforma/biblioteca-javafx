package Modelo;

import Utilidades.Constantes;
import Utilidades.DAOMetodos;
import java.util.ArrayList;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.Id;
@Entity
public class Socio {
    private String nombre;
    private String DNI;
    private Date fecha_de_nacimiento;
    private String direccion;
    private String localidad;
    @Id
    private int id;
private String contrasenya;
    public Socio(String nombre, String DNI, Date fecha_de_nacimiento, String direccion, String localidad, String contrasenya) {
        this.nombre = nombre;
        this.DNI = DNI;
        this.fecha_de_nacimiento = fecha_de_nacimiento;
        this.direccion = direccion;
        this.localidad = localidad;
        this.contrasenya = DAOMetodos.desencriptar_contrasenya(contrasenya);
    }
    public Socio() {
        this.nombre = Constantes.VACIO;
        this.DNI = Constantes.VACIO;
        this.fecha_de_nacimiento = new Date();
        this.direccion = Constantes.VACIO;
        this.localidad = Constantes.VACIO;
        id = 0;
        this.contrasenya = DAOMetodos.desencriptar_contrasenya("1234");
    }
    
    public Socio(Socio socio) {
        this.nombre = socio.getNombre();
        this.DNI = socio.getDNI();
        this.fecha_de_nacimiento = socio.getFecha_de_nacimiento();
        this.direccion = socio.getDireccion();
        this.localidad = socio.getLocalidad();
        id = socio.getId();
        this.contrasenya = socio.getContrasenya();
    }
    
    public String getContrasenya() {
        return contrasenya;
    }

    public void setContrasenya(String contrasenya) {
        this.contrasenya = contrasenya;
        //this.contrasenya = DAOMetodos.desencriptar_contrasenya(this.contrasenya);
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDNI() {
        return DNI;
    }

    public void setDNI(String DNI) {
        this.DNI = DNI;
    }

    public Date getFecha_de_nacimiento() {
        return fecha_de_nacimiento;
    }

    public void setFecha_de_nacimiento(Date fecha_de_nacimiento) {
        this.fecha_de_nacimiento = fecha_de_nacimiento;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getLocalidad() {
        return localidad;
    }

    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }

    public boolean equals(Socio obj) {
        
        return this.id == obj.getId();
    }

    public boolean Equals(Socio objeto) {
        boolean igual = true;
        try{
            if (!(
                    objeto.getDNI().equals(this.getDNI()) 
                    && objeto.getDireccion().equals(this.getDireccion()) 
                    && objeto.getLocalidad().equals(this.getLocalidad())
                    && objeto.getNombre().equals(this.getNombre())
                    && objeto.getFecha_de_nacimiento().equals(this.getFecha_de_nacimiento())
                    )){
                throw new Exception();
            }
        }
        catch (Exception e){
            igual = false;
        }
        return igual;
    }


    @Override
    public String toString() {
        return nombre;
    }
    
    
}
