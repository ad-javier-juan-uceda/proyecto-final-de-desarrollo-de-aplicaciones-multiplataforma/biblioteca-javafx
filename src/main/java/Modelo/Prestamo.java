package Modelo;

import java.util.Date;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.Id;
@Entity
public class Prestamo {
    @Id
    private int id;
    private Libro libro_prestado;	
    private Socio socio;
    private Date fecha_de_prestamo;
    private Date fecha_que_se_tiene_que_devolver;
    private Date fecha_que_se_ha_devuelto;
    private boolean esta_devuelto;

    public Prestamo(Libro libro_prestado, Socio socio, Date fecha_de_prestamo, Date fecha_que_se_tiene_que_devolver, Date fecha_que_se_ha_devuelto, boolean esta_devuelto) {
        this.libro_prestado = libro_prestado;
        this.socio = socio;
        this.fecha_de_prestamo = fecha_de_prestamo;
        this.fecha_que_se_tiene_que_devolver = fecha_que_se_tiene_que_devolver;
        this.fecha_que_se_ha_devuelto = fecha_que_se_ha_devuelto;
        this.esta_devuelto = esta_devuelto;
    }
    
    public Prestamo() {
        id = 0;
        this.libro_prestado = new Libro();
        this.socio = new Socio();
        this.fecha_de_prestamo = new Date();
        this.fecha_que_se_tiene_que_devolver = new Date();
        this.fecha_que_se_ha_devuelto = new Date();
        this.esta_devuelto = false;
    }
    
    public Prestamo(Prestamo prestamo) {
        id = prestamo.getId();
        this.libro_prestado = prestamo.getLibro_prestado();
        this.socio = prestamo.getSocio();
        this.fecha_de_prestamo = prestamo.getFecha_de_prestamo();
        this.fecha_que_se_tiene_que_devolver = prestamo.getFecha_que_se_tiene_que_devolver();
        this.fecha_que_se_ha_devuelto = prestamo.getFecha_que_se_ha_devuelto();
        this.esta_devuelto = prestamo.isEsta_devuelto();
    }

    public Libro getLibro_prestado() {
        return libro_prestado;
    }

    public void setLibro_prestado(Libro libro_prestado) {
        this.libro_prestado = libro_prestado;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Socio getSocio() {
        return socio;
    }

    public void setSocio(Socio socio) {
        this.socio = socio;
    }

    public Date getFecha_de_prestamo() {
        return fecha_de_prestamo;
    }

    public void setFecha_de_prestamo(Date fecha_de_prestamo) {
        this.fecha_de_prestamo = fecha_de_prestamo;
    }

    public Date getFecha_que_se_tiene_que_devolver() {
        return fecha_que_se_tiene_que_devolver;
    }

    public void setFecha_que_se_tiene_que_devolver(Date fecha_que_se_tiene_que_devolver) {
        this.fecha_que_se_tiene_que_devolver = fecha_que_se_tiene_que_devolver;
    }

    public Date getFecha_que_se_ha_devuelto() {
        return fecha_que_se_ha_devuelto;
    }

    public void setFecha_que_se_ha_devuelto(Date fecha_que_se_ha_devuelto) {
        this.fecha_que_se_ha_devuelto = fecha_que_se_ha_devuelto;
    }

    public boolean isEsta_devuelto() {
        return esta_devuelto;
    }

    public void setEsta_devuelto(boolean esta_devuelto) {
        this.esta_devuelto = esta_devuelto;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Prestamo other = (Prestamo) obj;
        if (this.esta_devuelto != other.esta_devuelto) {
            return false;
        }
        if (!Objects.equals(this.libro_prestado, other.libro_prestado)) {
            return false;
        }
        if (!Objects.equals(this.socio, other.socio)) {
            return false;
        }
        if (!Objects.equals(this.fecha_de_prestamo, other.fecha_de_prestamo)) {
            return false;
        }
        if (!Objects.equals(this.fecha_que_se_tiene_que_devolver, other.fecha_que_se_tiene_que_devolver)) {
            return false;
        }
        if (!Objects.equals(this.fecha_que_se_ha_devuelto, other.fecha_que_se_ha_devuelto)) {
            return false;
        }
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    public boolean Equals(Prestamo objeto) {
        boolean igual = true;
        try{
            if (!(
                    objeto.getFecha_de_prestamo().equals(this.getFecha_de_prestamo()) 
                    && objeto.getFecha_que_se_ha_devuelto().equals(this.getFecha_que_se_ha_devuelto()) 
                    && objeto.getFecha_que_se_tiene_que_devolver().equals(this.getFecha_que_se_tiene_que_devolver())
                    && objeto.getLibro_prestado().equals(this.getLibro_prestado())
                    && objeto.getSocio().equals(this.getSocio())
                    )){
                throw new Exception();
            }
        }
        catch (Exception e){
            igual = false;
        }
        return igual;
    }
    
    @Override
    public String toString() {
        return libro_prestado.toString();
    }
    
}
