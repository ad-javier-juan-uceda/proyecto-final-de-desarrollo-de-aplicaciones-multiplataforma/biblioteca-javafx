package Modelo;

import Utilidades.Constantes;
import javax.persistence.Entity;
import javax.persistence.Id;
@Entity
public class Libro {
@Id
    private int id;
    private String Titulo;
    private String ISBN;
    private String estado_del_libro;
    private int cantidad;
    private int paginas;
    private boolean esta_prestado;
    private Editorial editorial;
    private Localizacion localizacion;
    private Tipo tipo_del_libro;
    private Autor autor;

    public Libro(String Titulo, String ISBN, String estado_del_libro, int cantidad, int paginas, boolean esta_prestado, Editorial editorial, Localizacion localizacion, Tipo tipo_del_libro) {
        this.Titulo = Titulo;
        this.ISBN = ISBN;
        this.estado_del_libro = estado_del_libro;
        this.cantidad = cantidad;
        this.paginas = paginas;
        this.esta_prestado = esta_prestado;
        this.editorial = editorial;
        this.localizacion = localizacion;
        this.tipo_del_libro = tipo_del_libro;
    }

    public Libro() {
        this.id = 0;
        this.Titulo = Constantes.VACIO;
        this.ISBN = Constantes.VACIO;
        this.estado_del_libro = Constantes.VACIO;
        this.cantidad = 0;
        this.paginas = 0;
        this.esta_prestado = false;
        this.editorial = new Editorial();
        this.localizacion = new Localizacion();
        this.autor = new Autor();
        this.tipo_del_libro = new Tipo();
    }

    public Libro(Libro libro) {
        this.Titulo = libro.getTitulo();
        this.ISBN = libro.getISBN();
        this.estado_del_libro = libro.getEstado_del_libro();
        this.cantidad = libro.getCantidad();
        this.paginas = libro.getPaginas();
        this.esta_prestado = libro.isEsta_prestado();
        this.editorial = libro.getEditorial();
        this.localizacion = libro.getLocalizacion();
        this.tipo_del_libro = libro.getTipo_del_libro();
        id = libro.getId();
        autor = libro.getAutor();
    }

    public String getTitulo() {
        return Titulo;
    }

    public void setTitulo(String Titulo) {
        this.Titulo = Titulo;
    }

    public String getISBN() {
        return ISBN;
    }

    public void setISBN(String ISBN) {
        this.ISBN = ISBN;
    }

    public Autor getAutor() {
        return autor;
    }

    public void setAutor(Autor autor) {
        this.autor = autor;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEstado_del_libro() {
        return estado_del_libro;
    }

    public void setEstado_del_libro(String estado_del_libro) {
        this.estado_del_libro = estado_del_libro;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public int getPaginas() {
        return paginas;
    }

    public void setPaginas(int paginas) {
        this.paginas = paginas;
    }

    public boolean isEsta_prestado() {
        return esta_prestado;
    }

    public void setEsta_prestado(boolean esta_prestado) {
        this.esta_prestado = esta_prestado;
    }

    public Editorial getEditorial() {
        return editorial;
    }

    public void setEditorial(Editorial editorial) {
        this.editorial = editorial;
    }

    public Localizacion getLocalizacion() {
        return localizacion;
    }

    public void setLocalizacion(Localizacion localizacion) {
        this.localizacion = localizacion;
    }

    public Tipo getTipo_del_libro() {
        return tipo_del_libro;
    }

    public void setTipo_del_libro(Tipo tipo_del_libro) {
        this.tipo_del_libro = tipo_del_libro;
    }

    /**
     *
     * @param obj
     * @return
     */
    public boolean equals(Libro obj) {
        return this.id == obj.getId();

    }
    
    public boolean Equals(Libro objeto) {
        boolean igual = true;
        try{
            if (!(
                    objeto.getTitulo().equals(this.getTitulo()) 
                    && objeto.getISBN().equals(this.getISBN()) 
                    && objeto.getEstado_del_libro().equals(this.getEstado_del_libro())
                    && objeto.getPaginas() == this.getPaginas() 
                    && objeto.getCantidad() == this.getCantidad() 
                    && objeto.isEsta_prestado() == this.isEsta_prestado()
                    && objeto.getAutor().equals(this.getAutor())
                    && objeto.getEditorial().equals(this.getEditorial())
                    && objeto.getLocalizacion().equals(this.getLocalizacion())
                    && objeto.getTipo_del_libro().equals(this.getTipo_del_libro())
                    )){
                throw new Exception();
            }
        }
        catch (Exception e){
            igual = false;
        }
        return igual;
    }

    @Override
    public String toString() {
        return Titulo;
    }
}
