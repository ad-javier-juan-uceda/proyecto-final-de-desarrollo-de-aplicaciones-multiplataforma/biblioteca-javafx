package Modelo;

import Utilidades.Constantes;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.Id;
@Entity
public class Autor {
    @Id
    private int id;
    private String nombre;
    private int telefono;
    private Date fecha_de_nacimiento;
    private String pagina_web;
    private String facebook;	
    private String twitter;	
    private String instagram;	

    public Autor(String nombre, int telefono, Date fecha_de_nacimiento, String pagina_web, String facebook, String twitter, String instagram) {
        this.nombre = nombre;
        this.telefono = telefono;
        this.fecha_de_nacimiento = fecha_de_nacimiento;
        this.pagina_web = pagina_web;
        this.facebook = facebook;
        this.twitter = twitter;
        this.instagram = instagram;
    }
    public Autor() {
        this.nombre = Constantes.VACIO;
        this.telefono = 0;
        this.fecha_de_nacimiento = new Date();
        this.pagina_web = Constantes.VACIO;
        this.facebook = Constantes.VACIO;
        this.twitter = Constantes.VACIO;
        this.instagram = Constantes.VACIO;
        this.id = 0;
    }
    
    public Autor(Autor autor) {
        this.nombre = autor.getNombre();
        this.telefono = autor.getTelefono();
        this.fecha_de_nacimiento = autor.getFecha_de_nacimiento();
        this.pagina_web = autor.getPagina_web();
        this.facebook = autor.getFacebook();
        this.twitter = autor.getTwitter();
        this.instagram = autor.getInstagram();
        id = autor.getId();
    }
    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    public Date getFecha_de_nacimiento() {
        return fecha_de_nacimiento;
    }

    public void setFecha_de_nacimiento(Date fecha_de_nacimiento) {
        this.fecha_de_nacimiento = fecha_de_nacimiento;
    }

    public String getPagina_web() {
        return pagina_web;
    }

    public void setPagina_web(String pagina_web) {
        this.pagina_web = pagina_web;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public String getInstagram() {
        return instagram;
    }

    public void setInstagram(String instagram) {
        this.instagram = instagram;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public boolean equals(Autor objeto) {
        
        boolean igual = true;
        try{
            if (!(
                    objeto.getFacebook().equals(this.getFacebook()) 
                    && objeto.getTwitter().equals(this.getTwitter()) 
                    && objeto.getInstagram().equals(this.getInstagram()) 
                    && objeto.getFecha_de_nacimiento().equals(this.getFecha_de_nacimiento())
                    && objeto.getNombre().equals(this.getNombre()) 
                    && objeto.getPagina_web().equals(this.getPagina_web()) 
                    && objeto.getTelefono() == this.getTelefono()
                )){
                throw new Exception();
            }
        }
        catch (Exception e){
            igual = false;
        }
        return igual;
        
    }

    /**
     *
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Autor other = (Autor) obj;
        if (this.telefono != other.telefono) {
            return false;
        }
        if (!Objects.equals(this.nombre, other.nombre)) {
            return false;
        }
        if (!Objects.equals(this.pagina_web, other.pagina_web)) {
            return false;
        }
        if (!Objects.equals(this.facebook, other.facebook)) {
            return false;
        }
        if (!Objects.equals(this.twitter, other.twitter)) {
            return false;
        }
        if (!Objects.equals(this.instagram, other.instagram)) {
            return false;
        }
        if (!Objects.equals(this.fecha_de_nacimiento, other.fecha_de_nacimiento)) {
            return false;
        }
        return true;
    }
    
    

    @Override
    public String toString() {
        return nombre;
    }
    
    

}
