package Modelo;

import Utilidades.Constantes;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.Id;
@Entity
public class Tipo {
    private String nombre;
    private String descripcion;
    @Id
    private int id;
    
    public Tipo(String nombre, String descripcion) {
        this.nombre = nombre;
        this.descripcion = descripcion;
    }
    
    public Tipo() {
        this.nombre = Constantes.VACIO;
        this.descripcion = Constantes.VACIO;
        id = 0;
    }
    
    public Tipo(Tipo tipo) {
        this.nombre = tipo.getNombre();
        this.descripcion = tipo.getDescripcion();
        id = tipo.getId();
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Tipo other = (Tipo) obj;
        if (!Objects.equals(this.nombre, other.nombre)) {
            return false;
        }
        if (!Objects.equals(this.descripcion, other.descripcion)) {
            return false;
        }
        return true;
    }
    
    public boolean equals(Tipo objeto) {
        
        boolean igual = true;
        try{
            if (!(
                    objeto.getNombre().equals(this.getNombre()) 
                    && objeto.getDescripcion().equals(this.getDescripcion()) 
                )){
                throw new Exception();
            }
        }
        catch (Exception e){
            igual = false;
        }
        return igual;
        
    }

    @Override
    public String toString() {
        return nombre;
    }
    
}
