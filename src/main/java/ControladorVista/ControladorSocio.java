/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ControladorVista;

import static ControladorVista.ControladorGlobal.COLOR_EXITO;
import DAO.SocioDAO;
import DAO.EditorialDAO;
import DAO.LibroDAO;
import DAO.LocalizacionDAO;
import DAO.SocioDAO;
import DAO.TipoDAO;
import Modelo.Socio;
import Modelo.Editorial;
import Modelo.Libro;
import Modelo.Localizacion;
import Modelo.Socio;
import Modelo.Tipo;
import Thread.ThreadPaginaWebEscrito;
import Utilidades.Constantes;
import Utilidades.DAOMetodos;
import Utilidades.Dialog;
import Utilidades.Toast;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.controls.JFXToggleButton;
import com.mycompany.proyectobiblioteca.App;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Date;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Cursor;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.SingleSelectionModel;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
import org.apache.xmlrpc.XmlRpcException;

/**
 *
 * @author batoi
 */
public class ControladorSocio extends ControladorGlobal<Socio> {

    private SocioDAO librosDAO;
    private ObservableList<Socio> lista_libros;

    @FXML
    private Label label_titulo;
    @FXML
    private Label label_isbn;
    @FXML
    private Label elegir_editorial;
    @FXML
    private Label prestado;
    @FXML
    private JFXToggleButton button_prestado;
    @FXML
    private Label elegir_autor;
    @FXML
    private Label elegir_tipo;
    @FXML
    private Label elegir_localizaicion;
    @FXML
    private Label elegir_estado;
    @FXML
    private GridPane gridpane;
    @FXML
    private GridPane opciones_modificar;
    @FXML
    private HBox opciones_buscar;
    @FXML
    private HBox opciones_buscar_2;
    @FXML
    private HBox opciones_buscar_3;
    @FXML
    private VBox fondo;
    @FXML
    private JFXTextField textfield_buscar_2;
    @FXML
    private JFXTextField textfield_cantidad;
    @FXML
    private JFXTextField textfield_paginas;
    @FXML
    private Tab tabinsertar;
    @FXML
    private Tab tabmodificar;

    @FXML
    private Label label_contenido;
    @FXML
    private Label label_autor;
    @FXML
    private Label label_editorial;
    @FXML
    private Label label_tipo;
    @FXML
    private Label label_estado;
    @FXML
    private Label label_localizacion;
    @FXML
    private Label label_cantidad;
    @FXML
    private Label label_paginas;

    @FXML
    private Label label_nombre;
    @FXML
    private Label label_telefono;
    @FXML
    private Label label_fecha;
    @FXML
    private Label label_pagina_web;
    @FXML
    private Label label_facebook;
    @FXML
    private Label label_twitter;
    @FXML
    private Label label_instagram;

    @FXML
    private JFXTextField texto_nombre;
    @FXML
    private JFXTextField texto_telefono;
    @FXML
    private JFXTextField texto_pagina_web;
    @FXML
    private JFXTextField texto_facebook;
    @FXML
    private JFXTextField texto_twitter;
    @FXML
    private JFXTextField texto_instagram;
    @FXML
    private DatePicker texto_fecha;

    @FXML
    private JFXButton button_limpiar;
    @FXML
    private JFXButton cancelar;
    @FXML
    private JFXButton actualizar;
    @FXML
    private Label cantidad;
    @FXML
    private JFXTextField texto_cantidad;
    @FXML
    private JFXTextField texto_titulo;
    @FXML
    private JFXTextField texto_isbn;
    @FXML
    private Label paginas;
    @FXML
    private JFXTextField texto_paginas;

    @FXML
    private TableColumn<Socio, String> Titulo;
    @FXML
    private TableColumn<Socio, String> ISBN;
    @FXML
    private TableColumn<Socio, String> estado_del_libro;
    @FXML
    private TableColumn<Socio, String> cantidad_scenebuilder;
    @FXML
    private TableColumn<Socio, String> paginas_scenebuilder;
    @FXML
    private TableColumn<Socio, String> editorial;
    @FXML
    private TableColumn<Socio, String> localizacion;
    @FXML
    private TableColumn<Socio, String> tipo_del_libro;
    @FXML
    private TableColumn<Socio, String> autor;

    @FXML
    private TableView<Socio> tabla_modificar_libros;
    public static Socio libro;
    private ContextMenu menu;
    private JFXComboBox<String> combobox_tipos_atributo;

    @Override
    public void insertar() {
        cambiar_a_cursor_esperando();
        //Socio autor_actualizacion = ControladorSocio.libro;
        Socio autor_seleccionado = new Socio();
        int telefono = 0;

        try {
            telefono = Integer.parseInt(texto_telefono.getText());
        } catch (Exception e) {
            telefono = 0;
        }

        autor_seleccionado.setNombre(texto_nombre.getText());
        autor_seleccionado.setDNI(texto_telefono.getText());
        autor_seleccionado.setDireccion(texto_pagina_web.getText());
        autor_seleccionado.setLocalidad(texto_facebook.getText());
        autor_seleccionado.setContrasenya(DAOMetodos.desencriptar_contrasenya(texto_twitter.getText()));
       
        autor_seleccionado.setFecha_de_nacimiento(Date.valueOf(texto_fecha.getValue()));
        try {
            socioDBDAO.insertarRegistro(autor_seleccionado);
            Toast.makeText(App.stage, resource.getString(Constantes.INSERCION_AUTOR_CORRECTA), Constantes.TOAST_DELAY, Constantes.FADE_IN_DELAY0, Constantes.FADE_IN_DELAY0, COLOR_EXITO);

        } catch (XmlRpcException ex) {
            Toast.makeText(App.stage, resource.getString(Constantes.INSERCION_AUTOR_NO_CORRECTA), Constantes.TOAST_DELAY, Constantes.FADE_IN_DELAY0, Constantes.FADE_IN_DELAY0, COLOR_RED);
        } catch (Exception ex) {
            Toast.makeText(App.stage, resource.getString(Constantes.INSERCION_AUTOR_NO_CORRECTA), Constantes.TOAST_DELAY, Constantes.FADE_IN_DELAY0, Constantes.FADE_IN_DELAY0, COLOR_RED);
        }

        cambiar_a_cursor_normal();
        actualizar_datos_tabla();
        panel.getSelectionModel().select(tabmodificar);
        tabla_modificar_libros.scrollTo(tabla_modificar_libros.getItems().size() - 1);
        tabla_modificar_libros.getSelectionModel().select(tabla_modificar_libros.getItems().size() - 1);
        cambiar_a_cursor_normal();
    }

    @Override
    public void modificar() {
        
    }

    @Override
    public void eliminar() {
        
    }

    @Override
    public void Exportar() {
        File archivo_a_guardar = App.guardar_fileChooser(resource.getString(Constantes.EXPORTAR_FILE_CHOOSER), Constantes.CSV);

        if (archivo_a_guardar != null) {

            try ( BufferedWriter escribir = new BufferedWriter(new FileWriter(archivo_a_guardar))) {
                ObservableList<Socio> libros_a_eliminar = tabla_modificar_libros.getSelectionModel().getSelectedItems();
                StringBuilder texto = new StringBuilder();
                for (Socio libro_a_eliminar : libros_a_eliminar) {
                    texto.append(libro_a_eliminar.getNombre()).append(Constantes.SEPARADOR_EXPORTACION);
                    texto.append(libro_a_eliminar.getDNI()).append(Constantes.SEPARADOR_EXPORTACION);
                    texto.append(libro_a_eliminar.getFecha_de_nacimiento()).append(Constantes.SEPARADOR_EXPORTACION);
                    texto.append(libro_a_eliminar.getDireccion()).append(Constantes.SEPARADOR_EXPORTACION);
                    texto.append(libro_a_eliminar.getLocalidad()).append(Constantes.SEPARADOR_EXPORTACION);
                    texto.append(System.lineSeparator());
                }
                escribir.write(texto.toString());

            } catch (IOException ex) {
                Dialog.Mensaje_de_excepcion(ex);
            }
            Dialog.Dialogo_de_confirmacion(resource.getString(Constantes.CONFIRMACION_EXPORTACION), archivo_a_guardar);
        }

    }

    @Override
    public void Cargar_datos() {
        try {
            librosDAO = new SocioDAO();
            lista_libros = FXCollections.observableArrayList();
            menu = new ContextMenu();
            Anular_exportar();
            actualizar_datos_tabla();
        } catch (XmlRpcException ex) {
            Dialog.Mensaje_de_excepcion(ex);
        } catch (Exception ex) {
            Dialog.Mensaje_de_excepcion(ex);
        }
    }

    public void actualizar_datos_tabla() {

        try {
            lista_libros.setAll(librosDAO.buscarTodos());
            tabla_modificar_libros.setItems(lista_libros);

            Titulo.setCellValueFactory(o -> new SimpleStringProperty(o.getValue().getNombre()));
            ISBN.setCellValueFactory(o -> new SimpleStringProperty(String.valueOf(o.getValue().getFecha_de_nacimiento())));
            estado_del_libro.setCellValueFactory(o -> new SimpleStringProperty(o.getValue().getDNI()));
            editorial.setCellValueFactory(o -> new SimpleStringProperty(o.getValue().getDireccion()));
            localizacion.setCellValueFactory(o -> new SimpleStringProperty(o.getValue().getLocalidad()));
            //tipo_del_libro = addButtonToTable("twitter");
            //autor = addButtonToTable("instagram");
            Titulo.setSortable(true);
            ISBN.setSortable(true);
            //estado_del_libro.setSortable(true);

            //editorial.setSortable(true);
            //localizacion.setSortable(true);
            //tipo_del_libro.setSortable(true);
            //autor.setSortable(true);
            MenuItem menuItem1 = new MenuItem(resource.getString(Constantes.ELIMINAR));
            MenuItem menuItem2 = new MenuItem(resource.getString(Constantes.ELIMINAR_TODOS_LOS_SELECCIONADOS));

            menuItem1.setOnAction((event) -> {
                try {

                    if (Dialog.Dialogo_de_confirmacion(resource.getString(Constantes.PREGUNTA_ELIMINAR))) {
                        librosDAO.eliminarRegistro(tabla_modificar_libros.getSelectionModel().getSelectedItem());
                        actualizar_datos_tabla();
                    }

                } catch (XmlRpcException ex) {
                    Dialog.Mensaje_de_excepcion(ex);
                } catch (Exception ex) {
                    Dialog.Mensaje_de_excepcion(ex);
                }
            });

            menuItem2.setOnAction((event) -> {
                try {

                    if (Dialog.Dialogo_de_confirmacion(resource.getString(Constantes.PREGUNTA_ELIMINAR))) {
                        ObservableList<Socio> libros_a_eliminar = tabla_modificar_libros.getSelectionModel().getSelectedItems();
                        for (Socio libro_a_eliminar : libros_a_eliminar) {
                            librosDAO.eliminarRegistro(libro_a_eliminar);
                        }
                        actualizar_datos_tabla();
                    }

                } catch (XmlRpcException ex) {
                    Dialog.Mensaje_de_excepcion(ex);
                } catch (Exception ex) {
                    Dialog.Mensaje_de_excepcion(ex);
                }
            });

            menu.getItems().setAll(menuItem1, menuItem2);
            tabla_modificar_libros.setContextMenu(menu);
            tabla_modificar_libros.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

            Socio autor_seleccionado = new Socio();

            texto_nombre.setText(autor_seleccionado.getNombre());
            texto_telefono.setText(autor_seleccionado.getDNI());
            texto_pagina_web.setText(autor_seleccionado.getDireccion());
            texto_facebook.setText(autor_seleccionado.getLocalidad());

            try {
                Instant instant = Instant.ofEpochMilli(autor_seleccionado.getFecha_de_nacimiento().getTime());
                LocalDateTime localDateTime = LocalDateTime.ofInstant(instant, ZoneId.systemDefault());
                LocalDate localDate = localDateTime.toLocalDate();

                texto_fecha.setValue(localDate);
            } catch (Exception e) {
                texto_fecha.setValue((new java.util.Date()).toInstant().atZone(ZoneId.systemDefault()).toLocalDate());

            }
        } catch (XmlRpcException ex) {
            Dialog.Mensaje_de_excepcion(ex);
        } catch (Exception ex) {
            Dialog.Mensaje_de_excepcion(ex);
        }
    }

    @Override
    public void actualizar() {
        actualizar_datos_tabla();
    }

    public void pantalla_modificar_libro(MouseEvent mouse) {

        if (Dialog.es_doble_click(mouse) && Dialog.es_boton_izquierdo(mouse)) {
            Parent parent = null;
            try {
                libro = tabla_modificar_libros.getSelectionModel().getSelectedItem();
                if (libro != null) {
                    FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("dialogmodificarsocio.fxml"),resource);
                    parent = fxmlLoader.load();
                    Scene scene = new Scene(parent);
                    Stage stage = new Stage();
                    stage.initModality(Modality.APPLICATION_MODAL);
                    stage.setScene(scene);
                    stage.showAndWait();
                    actualizar_datos_tabla();
                    cambiar_a_cursor_normal();
                }

            } catch (IOException ex) {
                Dialog.Mensaje_de_excepcion(ex);
            } catch (Exception ex) {
                Dialog.Mensaje_de_excepcion(ex);
            }
        }
    }

    public void Limpiar_busqueda() {
        textfield_buscar_2.setText(Constantes.VACIO);

        try {
            tabla_modificar_libros.getItems().setAll(librosDAO.buscarTodos());
        } catch (XmlRpcException ex) {
            Dialog.Mensaje_de_excepcion(ex);
        } catch (Exception ex) {
            Dialog.Mensaje_de_excepcion(ex);
        }

    }

    public void Buscar_contenido() {

        String texto_a_buscar = textfield_buscar_2.getText();
        ObservableList<Socio> libros_a_buscar = tabla_modificar_libros.getItems();
        if ((texto_a_buscar.length() > 0)) {

            ObservableList<Socio> libros_encontrados = FXCollections.observableArrayList();

            for (Socio autor : libros_a_buscar) {
                if (autor.getNombre().toLowerCase().contains(texto_a_buscar)) {
                    libros_encontrados.add(autor);
                }
            }

            tabla_modificar_libros.getItems().setAll(libros_encontrados);
        }

    }

    public void Anular_exportar() {
        try {
            exportar.setDisable(!tabmodificar.isSelected());
        } catch (NullPointerException n) {

        }
    }

}
