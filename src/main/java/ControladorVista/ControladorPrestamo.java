/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ControladorVista;

import static ControladorVista.ControladorGlobal.COLOR_EXITO;
import DAO.LibroDAO;
import DAO.PrestamoDAO;
import DAO.SocioDAO;
import Modelo.Editorial;
import Modelo.Libro;
import Modelo.Prestamo;
import Modelo.Localizacion;
import Modelo.Socio;
import Modelo.Tipo;
import Utilidades.Constantes;
import Utilidades.Dialog;
import Utilidades.Toast;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.controls.JFXToggleButton;
import com.mycompany.proyectobiblioteca.App;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.apache.xmlrpc.XmlRpcException;

/**
 *
 * @author batoi
 */
public class ControladorPrestamo extends ControladorGlobal<Prestamo> {

    private PrestamoDAO librosDAO;
    private ObservableList<Prestamo> lista_libros;

    

    @FXML
    private Label label_titulo;
    @FXML
    private Label label_isbn;
    @FXML
    private Label elegir_editorial;
    @FXML
    private Label prestado;
    @FXML
    private JFXToggleButton button_prestado;
    @FXML
    private Label elegir_autor;
    @FXML
    private Label elegir_tipo;
    @FXML
    private Label elegir_localizaicion;
    @FXML
    private Label elegir_estado;
    @FXML
    private GridPane gridpane;
    @FXML
    private GridPane opciones_modificar;
    @FXML
    private HBox opciones_buscar;
    @FXML
    private HBox opciones_buscar_2;
    @FXML
    private HBox opciones_buscar_3;
    @FXML
    private VBox fondo_prestamos;
    @FXML
    private JFXTextField textfield_buscar_2;
    @FXML
    private JFXTextField textfield_cantidad;
    @FXML
    private JFXTextField textfield_paginas;
    @FXML
    private Tab tabinsertar;
    @FXML
    private Tab tabmodificar;

    @FXML
    private Label label_contenido;
    @FXML
    private Label label_autor;
    @FXML
    private Label label_editorial;
    @FXML
    private Label label_tipo;
    @FXML
    private Label label_estado;
    @FXML
    private Label label_localizacion;
    @FXML
    private Label label_cantidad;
    @FXML
    private Label label_paginas;

    @FXML
    private JFXComboBox<String> combobox_estado;
    @FXML
    private JFXComboBox<Editorial> combobox_editorial;
    @FXML
    private JFXComboBox<Localizacion> combobox_localizacion;
    @FXML
    private JFXComboBox<Tipo> combobox_tipo;
    @FXML
    private JFXComboBox<Socio> combobox_autor;
    @FXML
    private JFXComboBox<Libro> combobox_tipos_atributo;
    @FXML
    private JFXToggleButton combobox_prestado;

    @FXML
    private JFXButton button_limpiar;
    @FXML
    private JFXButton cancelar;
    @FXML
    private JFXButton actualizar;
    @FXML
    private Label cantidad;
    @FXML
    private DatePicker texto_cantidad;
    @FXML
    private DatePicker texto_titulo;
    @FXML
    private DatePicker texto_isbn;
    @FXML
    private Label paginas;
    @FXML
    private JFXTextField texto_paginas;
    @FXML
    private JFXComboBox<String> texto_estado;
    @FXML
    private JFXComboBox<Localizacion> texto_localizacion;
    @FXML
    private JFXComboBox<Tipo> texto_tipo;

    @FXML
    private JFXComboBox<Socio> texto_autor;
    @FXML
    private JFXComboBox<Libro> texto_editorial;

    @FXML
    private TableColumn<Prestamo, String> Titulo;
    @FXML
    private TableColumn<Prestamo, String> ISBN;
    @FXML
    private TableColumn<Prestamo, String> estado_del_libro;
    @FXML
    private TableColumn<Prestamo, String> cantidad_scenebuilder;
    @FXML
    private TableColumn<Prestamo, String> paginas_scenebuilder;
    @FXML
    private TableColumn<Prestamo, String> editorial;
    @FXML
    private TableColumn<Prestamo, String> localizacion;
    @FXML
    private TableColumn<Prestamo, String> tipo_del_libro;
    @FXML
    private TableColumn<Prestamo, String> autor;

    @FXML
    private TableView<Prestamo> tabla_modificar_libros;
    public static Prestamo libro;
    private ContextMenu menu;

    

    @Override
    public void insertar() {
        cambiar_a_cursor_esperando();
        Prestamo libro_actualizacion = new Prestamo();
        if (texto_editorial.getSelectionModel().getSelectedItem() != null) {
            libro_actualizacion.setLibro_prestado(texto_editorial.getSelectionModel().getSelectedItem());
            if (texto_autor.getSelectionModel().getSelectedItem() != null) {
                libro_actualizacion.setSocio(texto_autor.getSelectionModel().getSelectedItem());
                if (texto_cantidad.getValue() != null) {
                    libro_actualizacion.setFecha_de_prestamo(Date.valueOf(texto_cantidad.getValue()));
                }
                if (texto_titulo.getValue() != null) {
                    libro_actualizacion.setFecha_que_se_ha_devuelto(Date.valueOf(texto_titulo.getValue()));
                }
                if (texto_isbn.getValue() != null) {
                    libro_actualizacion.setFecha_que_se_tiene_que_devolver(Date.valueOf(texto_isbn.getValue()));
                }
                libro_actualizacion.setEsta_devuelto(button_prestado.selectedProperty().getValue());
            }
        }

        try {

            if (!libro_actualizacion.getLibro_prestado().isEsta_prestado()) {
                prestamoDBDAO.insertarRegistro(libro_actualizacion);
                libro_actualizacion.setEsta_devuelto(Constantes.BOOLEAN_POR_DEFECTO);
                libro_actualizacion.getLibro_prestado().setEsta_prestado(true);
                libroDAO.actualizarRegistro(libro_actualizacion.getLibro_prestado());
                Toast.makeText(App.stage, resource.getString(Constantes.ACTUALIZACION_AUTOR_CORRECTA), Constantes.TOAST_DELAY, Constantes.FADE_IN_DELAY0, Constantes.FADE_IN_DELAY0, COLOR_EXITO);

            } else {
                Toast.makeText(App.stage, resource.getString(Constantes.INUTILIZABLE), Constantes.TOAST_DELAY, Constantes.FADE_IN_DELAY0, Constantes.FADE_IN_DELAY0, COLOR_RED);
            }

        } catch (XmlRpcException ex) {
            Toast.makeText(App.stage, resource.getString(Constantes.INSERCION_AUTOR_NO_CORRECTA), Constantes.TOAST_DELAY, Constantes.FADE_IN_DELAY0, Constantes.FADE_IN_DELAY0, COLOR_RED);
        } catch (Exception ex) {
            Toast.makeText(App.stage, resource.getString(Constantes.INSERCION_AUTOR_NO_CORRECTA), Constantes.TOAST_DELAY, Constantes.FADE_IN_DELAY0, Constantes.FADE_IN_DELAY0, COLOR_RED);
        }

        actualizar_datos_tabla();
        panel.getSelectionModel().select(tabmodificar);
        tabla_modificar_libros.scrollTo(tabla_modificar_libros.getItems().size() - 1);
        tabla_modificar_libros.getSelectionModel().select(tabla_modificar_libros.getItems().size() - 1);
        cambiar_a_cursor_normal();
    }

    @Override
    public void modificar() {

    }

    @Override
    public void eliminar() {

    }

    @Override
    public void Exportar() {
        File archivo_a_guardar = App.guardar_fileChooser(resource.getString(Constantes.EXPORTAR_FILE_CHOOSER), Constantes.CSV);

        if (archivo_a_guardar != null) {

            try ( BufferedWriter escribir = new BufferedWriter(new FileWriter(archivo_a_guardar))) {
                ObservableList<Prestamo> libros_a_eliminar = tabla_modificar_libros.getSelectionModel().getSelectedItems();
                StringBuilder texto = new StringBuilder();
                for (Prestamo libro_a_eliminar : libros_a_eliminar) {
                    texto.append(libro_a_eliminar.getFecha_de_prestamo().toString()).append(Constantes.SEPARADOR_EXPORTACION);
                    texto.append(libro_a_eliminar.getFecha_que_se_ha_devuelto().toString()).append(Constantes.SEPARADOR_EXPORTACION);
                    texto.append(libro_a_eliminar.getFecha_que_se_tiene_que_devolver().toString()).append(Constantes.SEPARADOR_EXPORTACION);
                    texto.append(libro_a_eliminar.getLibro_prestado().getId()).append(Constantes.SEPARADOR_EXPORTACION);
                    texto.append(libro_a_eliminar.getSocio().getId()).append(Constantes.SEPARADOR_EXPORTACION);
                    texto.append(System.lineSeparator());
                }
                escribir.write(texto.toString());

            } catch (IOException ex) {
                Dialog.Mensaje_de_excepcion(ex);
            }
            Dialog.Dialogo_de_confirmacion(resource.getString(Constantes.CONFIRMACION_EXPORTACION), archivo_a_guardar);
        }

    }

    @Override
    public void Cargar_datos() {
        try {
            librosDAO = new PrestamoDAO();
            lista_libros = FXCollections.observableArrayList();
            menu = new ContextMenu();
            Anular_exportar();
            actualizar_datos_tabla();
        } catch (XmlRpcException ex) {
            Dialog.Mensaje_de_excepcion(ex);
        } catch (Exception ex) {
            Dialog.Mensaje_de_excepcion(ex);
        }
    }

    public void actualizar_datos_tabla() {

        try {
            lista_libros.setAll(librosDAO.buscarTodos());
            tabla_modificar_libros.setItems(lista_libros);
            Titulo.setCellValueFactory(o -> new SimpleStringProperty(o.getValue().getLibro_prestado().toString()));
            ISBN.setCellValueFactory(o -> new SimpleStringProperty(o.getValue().getSocio().toString()));
            estado_del_libro.setCellValueFactory(o -> new SimpleStringProperty(o.getValue().getFecha_de_prestamo().toString()));
            cantidad_scenebuilder.setCellValueFactory(o -> new SimpleStringProperty(o.getValue().getFecha_que_se_ha_devuelto().toString()));

            paginas_scenebuilder.setCellValueFactory(o -> new SimpleStringProperty(o.getValue().getFecha_que_se_tiene_que_devolver().toString()));

            Titulo.setSortable(true);
            ISBN.setSortable(true);
            estado_del_libro.setSortable(true);
            cantidad_scenebuilder.setSortable(true);
            paginas_scenebuilder.setSortable(true);

            combobox_tipos_atributo.getItems().setAll(libroDAO.buscarTodos());
            combobox_autor.getItems().setAll(socioDBDAO.buscarTodos());

            MenuItem menuItem1 = new MenuItem(resource.getString(Constantes.ELIMINAR));
            MenuItem menuItem2 = new MenuItem(resource.getString(Constantes.ELIMINAR_TODOS_LOS_SELECCIONADOS));

            menuItem1.setOnAction((event) -> {
                try {

                    if (Dialog.Dialogo_de_confirmacion(resource.getString(Constantes.PREGUNTA_ELIMINAR))) {
                        librosDAO.eliminarRegistro(tabla_modificar_libros.getSelectionModel().getSelectedItem());
                        actualizar_datos_tabla();
                    }

                } catch (XmlRpcException ex) {
                    Dialog.Mensaje_de_excepcion(ex);
                } catch (Exception ex) {
                    Dialog.Mensaje_de_excepcion(ex);
                }
            });

            menuItem2.setOnAction((event) -> {
                try {

                    if (Dialog.Dialogo_de_confirmacion(resource.getString(Constantes.PREGUNTA_ELIMINAR))) {
                        ObservableList<Prestamo> libros_a_eliminar = tabla_modificar_libros.getSelectionModel().getSelectedItems();
                        for (Prestamo libro_a_eliminar : libros_a_eliminar) {
                            librosDAO.eliminarRegistro(libro_a_eliminar);
                        }
                        actualizar_datos_tabla();
                    }

                } catch (XmlRpcException ex) {
                    Dialog.Mensaje_de_excepcion(ex);
                } catch (Exception ex) {
                    Dialog.Mensaje_de_excepcion(ex);
                }
            });

            menu.getItems().setAll(menuItem1, menuItem2);
            tabla_modificar_libros.setContextMenu(menu);
            tabla_modificar_libros.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

            texto_editorial.getSelectionModel().clearSelection();
            texto_autor.getSelectionModel().clearSelection();
            texto_editorial.getItems().setAll(libroDAO.buscarTodos());
            texto_editorial.getItems().removeIf(n -> (n.isEsta_prestado()));
            texto_autor.getItems().setAll(socioDBDAO.buscarTodos());
            button_prestado.selectedProperty().setValue(Constantes.BOOLEAN_POR_DEFECTO);

        } catch (XmlRpcException ex) {
            Dialog.Mensaje_de_excepcion(ex);
        } catch (Exception ex) {
            Dialog.Mensaje_de_excepcion(ex);
        }
    }

    @Override
    public void actualizar() {
        actualizar_datos_tabla();
    }

    public void pantalla_modificar_libro(MouseEvent mouse) {

        if (Dialog.es_doble_click(mouse) && Dialog.es_boton_izquierdo(mouse)) {

            try {
                dialogo();
            } catch (IOException ex) {
                Dialog.Mensaje_de_excepcion(ex);
            } catch (Exception ex) {
                Dialog.Mensaje_de_excepcion(ex);
            }
        }
    }

    public void dialogo() throws IOException, Exception {
        Parent parent = null;
        libro = tabla_modificar_libros.getSelectionModel().getSelectedItem();
        if (libro != null) {
            FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("dialogmodificarprestamo.fxml"), resource);
            parent = fxmlLoader.load();
            Scene scene = new Scene(parent);
            Stage stage = new Stage();
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setScene(scene);
            stage.showAndWait();
            actualizar_datos_tabla();
            cambiar_a_cursor_normal();
        }
    }

    public void Limpiar_busqueda() {
        combobox_autor.getSelectionModel().clearSelection();
        combobox_tipos_atributo.getSelectionModel().clearSelection();

        try {
            tabla_modificar_libros.getItems().setAll(librosDAO.buscarTodos());
        } catch (XmlRpcException ex) {
            Dialog.Mensaje_de_excepcion(ex);
        } catch (Exception ex) {
            Dialog.Mensaje_de_excepcion(ex);
        }

    }

    public void Buscar_autor() {
        Socio autor = combobox_autor.getSelectionModel().getSelectedItem();
        ObservableList<Prestamo> libros_a_buscar = tabla_modificar_libros.getItems();
        if (autor != null) {
            ObservableList<Prestamo> libros_encontrados = FXCollections.observableArrayList();

            for (Prestamo libro_a_buscar : libros_a_buscar) {
                if (libro_a_buscar.getSocio().equals(autor)) {
                    libros_encontrados.add(libro_a_buscar);
                }
            }
            tabla_modificar_libros.getItems().setAll(libros_encontrados);
        } else {
            tabla_modificar_libros.getItems().setAll(libros_a_buscar);
        }
    }

    public void Buscar_editorial() {
        Libro editorial = combobox_tipos_atributo.getSelectionModel().getSelectedItem();
        ObservableList<Prestamo> libros_a_buscar = tabla_modificar_libros.getItems();
        if (editorial != null) {
            ObservableList<Prestamo> libros_encontrados = FXCollections.observableArrayList();

            for (Prestamo libro_a_buscar : libros_a_buscar) {
                if (libro_a_buscar.getLibro_prestado().equals(editorial)) {
                    libros_encontrados.add(libro_a_buscar);
                }
            }
            tabla_modificar_libros.getItems().setAll(libros_encontrados);
        } else {
            tabla_modificar_libros.getItems().setAll(libros_a_buscar);
        }
    }

    public void Anular_exportar() {
        try {
            exportar.setDisable(!tabmodificar.isSelected());
        } catch (NullPointerException n) {

        }
    }

}
