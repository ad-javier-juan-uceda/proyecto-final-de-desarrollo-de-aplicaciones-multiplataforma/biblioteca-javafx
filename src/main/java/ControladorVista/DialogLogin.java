/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ControladorVista;

import static ControladorVista.ControladorGlobal.COLOR_EXITO;
import static ControladorVista.ControladorGlobal.COLOR_RED;
import DAO.UsuarioDAO;
import Modelo.Usuario;
import Utilidades.Constantes;
import static Utilidades.Constantes.MOSTRAR_CONTRASENYA;
import static Utilidades.Constantes.OCULTAR_CONTRASENYA;
import Utilidades.Toast;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.controls.JFXToggleButton;
import com.mycompany.proyectobiblioteca.App;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.prefs.Preferences;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import org.apache.xmlrpc.XmlRpcException;

/**
 *
 * @author batoi
 */
public class DialogLogin implements Initializable {
    
    private ResourceBundle resource;
    
    @FXML
    private Label label_nombre_usuario;
    @FXML
    private Label label_contrasenya;
    
    @FXML
    private JFXTextField Text_nombre_usuario;
    @FXML
    private JFXTextField Text_contrasenya_mostrar;
    @FXML
    private JFXPasswordField Text_contrasenya;
    @FXML
    private JFXCheckBox recordar_en_la_cuenta;
    @FXML
    private JFXToggleButton mostrar_contrasenya;
    
    @FXML
    private JFXButton entrar;
    @FXML
    private JFXButton salir;
    
    @Override
    public void initialize(URL arg0, ResourceBundle pResource) {
        resource = pResource;
        Text_contrasenya_mostrar.setVisible(false);
        mostrar_contrasenya.setText(resource.getString(OCULTAR_CONTRASENYA));
    }
    
    @FXML
    public void entrar(ActionEvent event){
        String contrasenya = mostrar_contrasenya.isSelected() ? Text_contrasenya_mostrar.getText() : Text_contrasenya.getText();
        try {
            Usuario usuario = new UsuarioDAO().Iniciar_sesion(Text_nombre_usuario.getText(), contrasenya);
            if (usuario != null) {
                Toast.makeText(App.stage, resource.getString(Constantes.LOGUEADO_CORRECTO) + usuario.getNombre(), Constantes.TOAST_DELAY, Constantes.FADE_IN_DELAY0, Constantes.FADE_IN_DELAY0, COLOR_EXITO);
                ControladorGlobal.usuario = usuario;
                if (recordar_en_la_cuenta.isSelected()) {
                    Preferences.userRoot().node(Constantes.ARCHIVO_PREFERENCIAS).putInt(Constantes.USUARIO_RECORDADO, usuario.getId());
                }
            }
            else{
                Toast.makeText(App.stage, resource.getString(Constantes.LOGUEADO_INCORRECTO), Constantes.TOAST_DELAY, Constantes.FADE_IN_DELAY0, Constantes.FADE_IN_DELAY0, COLOR_RED);
            }
            closeStage(event);
        } catch (XmlRpcException ex) {
            Toast.makeText(App.stage, resource.getString(Constantes.INSERCION_AUTOR_NO_CORRECTA), Constantes.TOAST_DELAY, Constantes.FADE_IN_DELAY0, Constantes.FADE_IN_DELAY0, COLOR_RED);
        } catch (Exception ex) {
            Toast.makeText(App.stage, resource.getString(Constantes.INSERCION_AUTOR_NO_CORRECTA), Constantes.TOAST_DELAY, Constantes.FADE_IN_DELAY0, Constantes.FADE_IN_DELAY0, COLOR_RED);
        }
    }
    
    public void mostrar_contrasenya(ActionEvent event) {
        boolean contrasenya_visible = mostrar_contrasenya.isSelected();
        String contrasenya = contrasenya_visible ? Text_contrasenya.getText() : Text_contrasenya_mostrar.getText();
        Text_contrasenya.setVisible(!contrasenya_visible);
        Text_contrasenya.setText(contrasenya);
        Text_contrasenya_mostrar.setVisible(contrasenya_visible);
        Text_contrasenya_mostrar.setText(contrasenya);
        mostrar_contrasenya.setText(resource.getString(contrasenya_visible ? OCULTAR_CONTRASENYA : MOSTRAR_CONTRASENYA));
    }
    
    public void closeStage(ActionEvent event) {
        Node  source = (Node)  event.getSource(); 
        Stage stage  = (Stage) source.getScene().getWindow();
        stage.close();
    }
    
}
