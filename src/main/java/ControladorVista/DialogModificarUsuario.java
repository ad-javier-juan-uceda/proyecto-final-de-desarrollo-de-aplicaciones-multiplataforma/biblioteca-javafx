/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ControladorVista;

import static ControladorVista.ControladorGlobal.COLOR_EXITO;
import static ControladorVista.ControladorGlobal.COLOR_RED;
import static ControladorVista.ControladorGlobal.usuarioDBDAO;
import DAO.UsuarioDAO;
import Modelo.Libro;
import Modelo.Usuario;
import Utilidades.Constantes;
import Utilidades.DAOMetodos;
import Utilidades.Dialog;
import Utilidades.Toast;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import com.mycompany.proyectobiblioteca.App;
import java.net.URL;
import java.time.ZoneId;
import java.sql.Date;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import org.apache.xmlrpc.XmlRpcException;

/**
 *
 * @author batoi
 */
public class DialogModificarUsuario implements Initializable {
    
    private ResourceBundle resource;

    @FXML
    private GridPane gridpane;

    @FXML
    private Label label_nombre;
    @FXML
    private Label label_telefono;
    @FXML
    private Label label_fecha;
    @FXML
    private Label label_pagina_web;
    @FXML
    private Label label_facebook;
    @FXML
    private Label label_twitter;
    @FXML
    private Label label_instagram;

    @FXML
    private JFXTextField texto_nombre;
    @FXML
    private JFXTextField texto_telefono;
    @FXML
    private JFXTextField texto_pagina_web;
    @FXML
    private JFXTextField texto_facebook;
    @FXML
    private JFXTextField texto_twitter;
    @FXML
    private JFXTextField texto_instagram;
    @FXML
    private DatePicker texto_fecha;
    @FXML
    private JFXButton actualizar;
    @FXML
    private JFXButton cancelar;

    @Override
    public void initialize(URL arg0, ResourceBundle pResource) {
        resource = pResource;
        //Usuario libro_seleccionado = ControladorUsuario.autor;
        Usuario autor_seleccionado = ControladorUsuario.libro;

        texto_nombre.setText(autor_seleccionado.getNombre());
        texto_telefono.setText(autor_seleccionado.getDNI());
        texto_pagina_web.setText(autor_seleccionado.getDireccion());
        texto_facebook.setText(autor_seleccionado.getLocalidad());

        try {
            Instant instant = Instant.ofEpochMilli(autor_seleccionado.getFecha_de_nacimiento().getTime());
            LocalDateTime localDateTime = LocalDateTime.ofInstant(instant, ZoneId.systemDefault());
            LocalDate localDate = localDateTime.toLocalDate();
            texto_fecha.setValue(localDate);
        } catch (Exception e) {
            texto_fecha.setValue((new java.util.Date()).toInstant().atZone(ZoneId.systemDefault()).toLocalDate());

        }

    }

    protected void cambiar_a_cursor_esperando() {
        App.stage.getScene().setCursor(Cursor.WAIT);
    }

    @FXML
    public void actualizar(ActionEvent event) {
        cambiar_a_cursor_esperando();
        //Usuario autor_actualizacion = ControladorUsuario.libro;
        Usuario autor_seleccionado = ControladorUsuario.libro;
        int telefono = 0;

        try {
            telefono = Integer.parseInt(texto_telefono.getText());
        } catch (Exception e) {
            telefono = 0;
        }

        autor_seleccionado.setNombre(texto_nombre.getText());
        autor_seleccionado.setDNI(texto_telefono.getText());
        autor_seleccionado.setDireccion(texto_pagina_web.getText());
        autor_seleccionado.setLocalidad(texto_facebook.getText());
        autor_seleccionado.setFecha_de_nacimiento(Date.valueOf(texto_fecha.getValue()));
        autor_seleccionado.setContrasenya(DAOMetodos.desencriptar_contrasenya(texto_twitter.getText()));
        try {
            usuarioDBDAO.actualizarRegistro(autor_seleccionado);
            Toast.makeText(App.stage, resource.getString(Constantes.INSERCION_AUTOR_CORRECTA), Constantes.TOAST_DELAY, Constantes.FADE_IN_DELAY0, Constantes.FADE_IN_DELAY0, COLOR_EXITO);

        } catch (XmlRpcException ex) {
            Toast.makeText(App.stage, resource.getString(Constantes.INSERCION_AUTOR_NO_CORRECTA), Constantes.TOAST_DELAY, Constantes.FADE_IN_DELAY0, Constantes.FADE_IN_DELAY0, COLOR_RED);
        } catch (Exception ex) {
            Toast.makeText(App.stage, resource.getString(Constantes.INSERCION_AUTOR_NO_CORRECTA), Constantes.TOAST_DELAY, Constantes.FADE_IN_DELAY0, Constantes.FADE_IN_DELAY0, COLOR_RED);
        }

        cambiar_a_cursor_normal();
        closeStage(event);
    }

    protected void cambiar_a_cursor_normal() {
        App.stage.getScene().setCursor(Cursor.DEFAULT);
    }

    public void closeStage(ActionEvent event) {
        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        stage.close();
    }

}
