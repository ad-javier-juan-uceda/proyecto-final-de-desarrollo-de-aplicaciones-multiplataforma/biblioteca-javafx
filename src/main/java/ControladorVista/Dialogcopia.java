/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ControladorVista;

import Utilidades.Constantes;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXRadioButton;
import com.jfoenix.controls.JFXTextField;
import java.io.File;
import java.net.URL;
import java.util.Arrays;
import java.util.ResourceBundle;
import java.util.prefs.Preferences;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;

/**
 *
 * @author batoi
 */
public class Dialogcopia implements Initializable {

    @FXML
    private Label hora;
    @FXML
    private JFXTextField horas;
    @FXML
    private JFXTextField minutos;
    @FXML
    private JFXTextField segundos;
    @FXML
    private JFXRadioButton lunes;
    @FXML
    private JFXRadioButton martes;
    @FXML
    private JFXRadioButton miercoles;
    @FXML
    private JFXRadioButton jueves;
    @FXML
    private JFXRadioButton viernes;
    @FXML
    private JFXRadioButton sabado;
    @FXML
    private JFXRadioButton domingo;
    @FXML
    private JFXButton salir;
    @FXML
    private JFXButton cambiarcarpeta;
    private Preferences preferences;

    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        preferences = Preferences.userRoot().node(Constantes.ARCHIVO_PREFERENCIAS);
        String[] diasDeLaSemana = preferences.get("dias", "-1 -1 -1 -1 -1 -1 -1").split(" ");
        lunes.setSelected(diasDeLaSemana[0].equals(lunes.getId()));
        martes.setSelected(diasDeLaSemana[1].equals(martes.getId()));
        miercoles.setSelected(diasDeLaSemana[2].equals(miercoles.getId()));
        jueves.setSelected(diasDeLaSemana[3].equals(jueves.getId()));
        viernes.setSelected(diasDeLaSemana[4].equals(viernes.getId()));
        sabado.setSelected(diasDeLaSemana[5].equals(sabado.getId()));
        domingo.setSelected(diasDeLaSemana[6].equals(domingo.getId()));
        horas.setText(String.valueOf(preferences.getInt("hora", 0)));
        minutos.setText(String.valueOf(preferences.getInt("minuto", 0)));
        segundos.setText(String.valueOf(preferences.getInt("segundo", 0)));

    }

    public void salir(ActionEvent event) {
        String dia1 = (lunes.isSelected()) ? "0" : "-1";
        String dia2 = (martes.isSelected()) ? "1" : "-1";
        String dia3 = (miercoles.isSelected()) ? "2" : "-1";
        String dia4 = (jueves.isSelected()) ? "3" : "-1";
        String dia5 = (viernes.isSelected()) ? "4" : "-1";
        String dia6 = (sabado.isSelected()) ? "5" : "-1";
        String dia7 = (domingo.isSelected()) ? "6" : "-1";
        String dias = dia1 + " " + dia2 + " " + dia3 + " " + dia4 + " " + dia5 + " " + dia6 + " " + dia7;
        preferences.put("dias", dias);
        int hora = obtener(horas ,24);
        int Minutos = obtener(minutos ,60);
        int segundo = obtener(segundos ,60);
        preferences.putInt("hora", hora);
        preferences.putInt("minuto", Minutos);
        preferences.putInt("segundo", segundo);
        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        stage.close();

    }
    
    public void cambiarcarpeta(ActionEvent event) {
        DirectoryChooser directoryChooser = new DirectoryChooser();
        directoryChooser.setInitialDirectory(new File(System.getProperty("user.home")));
        Node source = (Node) event.getSource();
        File selectedDirectory = directoryChooser.showDialog((Stage) source.getScene().getWindow());
        if (selectedDirectory != null){
            preferences.put("carpeta", selectedDirectory.getAbsolutePath());
        }
    }

    public int obtener(JFXTextField campo_de_texto ,int limite) {
        
        int numero = 0;
        
        
        try{
            numero = Integer.parseInt(campo_de_texto.getText());
            if (numero >= limite){
                throw new Exception();
            }
        }
        catch (Exception e){
            numero = 0;
        }
        
        return numero;

    }

}
