/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ControladorVista;

import static ControladorVista.ControladorGlobal.COLOR_EXITO;
import static ControladorVista.ControladorGlobal.COLOR_RED;
import static ControladorVista.ControladorGlobal.tipoDBDAO;
import DAO.TipoDAO;
import Modelo.Tipo;
import Utilidades.Constantes;
import Utilidades.Toast;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import com.mycompany.proyectobiblioteca.App;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import org.apache.xmlrpc.XmlRpcException;

/**
 *
 * @author batoi
 */
public class DialogModificarTipo implements Initializable {
    
    private ResourceBundle resource;

    @FXML
    private GridPane gridpane;

    @FXML
    private Label label_nombre;
    @FXML
    private Label label_telefono;
    @FXML
    private Label label_fecha;
    @FXML
    private Label label_pagina_web;
    @FXML
    private Label label_facebook;
    @FXML
    private Label label_twitter;
    @FXML
    private Label label_instagram;
    
    @FXML
    private JFXTextField text_direccion;
    @FXML
    private JFXTextField text_localidad;

    @FXML
    private JFXTextField texto_nombre;
    @FXML
    private JFXTextArea texto_telefono;
    @FXML
    private JFXTextField texto_pagina_web;
    @FXML
    private JFXTextField texto_facebook;
    @FXML
    private JFXTextField texto_twitter;
    @FXML
    private JFXTextField texto_instagram;
    @FXML
    private JFXTextField texto_fecha;
    @FXML
    private JFXButton actualizar;
    @FXML
    private JFXButton cancelar;

    @Override
    public void initialize(URL arg0, ResourceBundle pResource) {
        resource = pResource;
        //Editorial autor_seleccionado = ControladorAutor.autor;
        Tipo autor_seleccionado = ControladorTipo.libro;

        texto_nombre.setText(autor_seleccionado.getNombre());
        texto_telefono.setText(autor_seleccionado.getDescripcion());
    }

    protected void cambiar_a_cursor_esperando() {
        App.stage.getScene().setCursor(Cursor.WAIT);
    }

    @FXML
    public void actualizar(ActionEvent event) {
        cambiar_a_cursor_esperando();
        Tipo autor_seleccionado = ControladorTipo.libro;
        
        autor_seleccionado.setNombre(texto_nombre.getText());
        autor_seleccionado.setDescripcion(texto_telefono.getText());
        
        try {
            tipoDBDAO.actualizarRegistro(autor_seleccionado);
            Toast.makeText(App.stage, resource.getString(Constantes.INSERCION_AUTOR_CORRECTA), Constantes.TOAST_DELAY, Constantes.FADE_IN_DELAY0, Constantes.FADE_IN_DELAY0, COLOR_EXITO);

        } catch (XmlRpcException ex) {
            Toast.makeText(App.stage, resource.getString(Constantes.INSERCION_AUTOR_NO_CORRECTA), Constantes.TOAST_DELAY, Constantes.FADE_IN_DELAY0, Constantes.FADE_IN_DELAY0, COLOR_RED);
        } catch (Exception ex) {
            Toast.makeText(App.stage, resource.getString(Constantes.INSERCION_AUTOR_NO_CORRECTA), Constantes.TOAST_DELAY, Constantes.FADE_IN_DELAY0, Constantes.FADE_IN_DELAY0, COLOR_RED);
        }

        cambiar_a_cursor_normal();
        closeStage(event);
    }

    protected void cambiar_a_cursor_normal() {
        App.stage.getScene().setCursor(Cursor.DEFAULT);
    }

    public void closeStage(ActionEvent event) {
        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        stage.close();
    }

}
