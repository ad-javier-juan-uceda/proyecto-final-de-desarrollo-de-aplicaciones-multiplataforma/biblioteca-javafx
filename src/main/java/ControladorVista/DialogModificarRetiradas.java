/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ControladorVista;

import static ControladorVista.ControladorGlobal.COLOR_EXITO;
import static ControladorVista.ControladorGlobal.COLOR_RED;
import static ControladorVista.ControladorGlobal.retiradaDBDAO;
import static ControladorVista.ControladorGlobal.socioDBDAO;
import DAO.AutorDAO;
import DAO.EditorialDAO;
import DAO.RetiradaDAO;
import DAO.LocalizacionDAO;
import DAO.SocioDAO;
import DAO.TipoDAO;
import Modelo.Autor;
import Modelo.Editorial;
import Modelo.Retirada;
import Modelo.Localizacion;
import Modelo.Socio;
import Modelo.Tipo;
import Utilidades.Constantes;
import Utilidades.Dialog;
import Utilidades.Toast;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.controls.JFXToggleButton;
import com.mycompany.proyectobiblioteca.App;
import java.net.URL;
import java.sql.Date;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import org.apache.xmlrpc.XmlRpcException;

/**
 *
 * @author batoi
 */
public class DialogModificarRetiradas implements Initializable {

    private ResourceBundle resource;

    @FXML
    private GridPane gridpane;

    @FXML
    private Label label_contenido;
    @FXML
    private Label label_autor;
    @FXML
    private Label label_editorial;
    @FXML
    private Label label_tipo;
    @FXML
    private Label label_estado;
    @FXML
    private Label label_localizacion;
    @FXML
    private Label label_cantidad;
    @FXML
    private Label label_paginas;

    @FXML
    private JFXTextField texto_paginas;
    @FXML
    private JFXToggleButton button_prestado;
    @FXML
    private JFXComboBox<String> texto_estado;
    @FXML
    private JFXComboBox<Socio> texto_editorial;
    @FXML
    private JFXComboBox<Localizacion> texto_localizacion;
    @FXML
    private JFXComboBox<Tipo> texto_tipo;
    @FXML
    private JFXComboBox<Autor> texto_autor;

    @FXML
    private JFXTextField texto_cantidad;
    @FXML
    private DatePicker texto_titulo;
    @FXML
    private DatePicker texto_isbn;

    @Override
    public void initialize(URL arg0, ResourceBundle pResource) {
        resource = pResource;
        Retirada libro_actualizacion = ControladorRetiradas.libro;

        try {
            texto_editorial.getItems().setAll(socioDBDAO.buscarTodos());
            texto_titulo.setValue(transformar(libro_actualizacion.getFecha_que_se_retira_el_carnet()));
            texto_isbn.setValue(transformar(libro_actualizacion.getFecha_que_se_retira_el_carnet()));
        } catch (XmlRpcException ex) {
            Dialog.Mensaje_de_excepcion(ex);
        } catch (Exception ex) {
            Dialog.Mensaje_de_excepcion(ex);        
        }

        texto_estado.getItems().clear();
        texto_editorial.getSelectionModel().select(libro_actualizacion.getSocio());

        String[] estados_posibles = {
            resource.getString(Constantes.BUEN_ESTADO),
            resource.getString(Constantes.ESTROPEADO),
            resource.getString(Constantes.INUTILIZABLE),
            resource.getString(Constantes.PAGINAS_ROTAS)

        };

        for (String estado : estados_posibles) {
            texto_estado.getItems().add(estado);
        }
        texto_estado.getSelectionModel().select(Integer.parseInt(libro_actualizacion.getEstado_del_libro()));

    }

    private LocalDate transformar(java.util.Date fecha_de_prestamo) {
        Instant instant = Instant.ofEpochMilli(fecha_de_prestamo.getTime());
        LocalDateTime localDateTime = LocalDateTime.ofInstant(instant, ZoneId.systemDefault());
        LocalDate localDate = localDateTime.toLocalDate();
        return localDate;
    }

    protected void cambiar_a_cursor_esperando() {
        App.stage.getScene().setCursor(Cursor.WAIT);
    }

    @FXML
    public void actualizar(ActionEvent event) {

        cambiar_a_cursor_esperando();
        Retirada libro_actualizacion = ControladorRetiradas.libro;

        libro_actualizacion.setEstado_del_libro(String.valueOf(texto_estado.getSelectionModel().getSelectedIndex()));
        libro_actualizacion.setFecha_que_se_devuelve_el_carnet(Date.valueOf(texto_titulo.getValue()));
        libro_actualizacion.setFecha_que_se_retira_el_carnet(Date.valueOf(texto_isbn.getValue()));
        libro_actualizacion.setSocio(texto_editorial.getSelectionModel().getSelectedItem());

        try {
            retiradaDBDAO.actualizarRegistro(libro_actualizacion);
            Toast.makeText(App.stage, resource.getString(Constantes.ACTUALIZACION_AUTOR_CORRECTA), Constantes.TOAST_DELAY, Constantes.FADE_IN_DELAY0, Constantes.FADE_IN_DELAY0, COLOR_EXITO);

        } catch (XmlRpcException ex) {
            Toast.makeText(App.stage, resource.getString(Constantes.INSERCION_AUTOR_NO_CORRECTA), Constantes.TOAST_DELAY, Constantes.FADE_IN_DELAY0, Constantes.FADE_IN_DELAY0, COLOR_RED);
        } catch (Exception ex) {
            Toast.makeText(App.stage, resource.getString(Constantes.INSERCION_AUTOR_NO_CORRECTA), Constantes.TOAST_DELAY, Constantes.FADE_IN_DELAY0, Constantes.FADE_IN_DELAY0, COLOR_RED);
        }

        cambiar_a_cursor_normal();
        closeStage(event);
    }

    protected void cambiar_a_cursor_normal() {
        App.stage.getScene().setCursor(Cursor.DEFAULT);
    }

    public void closeStage(ActionEvent event) {
        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        stage.close();
    }

}
