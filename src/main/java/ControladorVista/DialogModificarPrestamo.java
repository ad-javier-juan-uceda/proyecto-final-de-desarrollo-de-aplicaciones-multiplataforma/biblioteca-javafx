/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ControladorVista;

import static ControladorVista.ControladorGlobal.COLOR_EXITO;
import static ControladorVista.ControladorGlobal.COLOR_RED;
import static ControladorVista.ControladorGlobal.*;
import Modelo.Libro;
import Modelo.Socio;
import Modelo.Prestamo;
import Modelo.Localizacion;
import Modelo.Tipo;
import Utilidades.Constantes;
import Utilidades.Dialog;
import Utilidades.Toast;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.controls.JFXToggleButton;
import com.mycompany.proyectobiblioteca.App;
import java.net.URL;
import java.sql.Date;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import org.apache.xmlrpc.XmlRpcException;

/**
 *
 * @author batoi
 */
public class DialogModificarPrestamo implements Initializable {
    
    private ResourceBundle resource;

    @FXML
    private GridPane gridpane;

    @FXML
    private Label label_contenido;
    @FXML
    private Label label_autor;
    @FXML
    private Label label_editorial;
    @FXML
    private Label label_tipo;
    @FXML
    private Label label_estado;
    @FXML
    private Label label_localizacion;
    @FXML
    private Label label_cantidad;
    @FXML
    private Label label_paginas;

    @FXML
    private JFXTextField texto_paginas;
    @FXML
    private JFXToggleButton button_prestado;
    @FXML
    private JFXComboBox<String> texto_estado;

    @FXML
    private JFXComboBox<Socio> texto_autor;
    @FXML
    private JFXComboBox<Libro> texto_editorial;
    @FXML
    private JFXComboBox<Localizacion> texto_localizacion;
    @FXML
    private JFXComboBox<Tipo> texto_tipo;

    @FXML
    private DatePicker texto_cantidad;
    @FXML
    private DatePicker texto_titulo;
    @FXML
    private DatePicker texto_isbn;

    @Override
    public void initialize(URL arg0, ResourceBundle pResource) {
        resource = pResource;
        //Prestamo libro_seleccionado = ControladorPrestamos.libro;
        Prestamo libro_seleccionado = ControladorPrestamo.libro;

        try {
            texto_editorial.getItems().setAll(libroDAO.buscarTodos());
            texto_autor.getItems().setAll(socioDBDAO.buscarTodos());

        } catch (XmlRpcException ex) {
            Dialog.Mensaje_de_excepcion(ex);
        } catch (Exception ex) {
            Dialog.Mensaje_de_excepcion(ex);
        }
        transformar(libro_seleccionado.getFecha_de_prestamo());
        texto_cantidad.setValue(transformar(libro_seleccionado.getFecha_de_prestamo()));
        texto_titulo.setValue(transformar(libro_seleccionado.getFecha_que_se_ha_devuelto()));
        texto_isbn.setValue(transformar(libro_seleccionado.getFecha_que_se_tiene_que_devolver()));
        button_prestado.selectedProperty().setValue(libro_seleccionado.isEsta_devuelto());
        texto_editorial.getSelectionModel().select(libro_seleccionado.getLibro_prestado());
        texto_editorial.getItems().removeIf(n -> (n.isEsta_prestado() == true)); 
        texto_autor.getSelectionModel().select(libro_seleccionado.getSocio());
    }

    protected void cambiar_a_cursor_esperando() {
        App.stage.getScene().setCursor(Cursor.WAIT);
    }

    @FXML
    public void actualizar(ActionEvent event) {

        cambiar_a_cursor_esperando();
        Prestamo libro_actualizacion = ControladorPrestamo.libro;
        libro_actualizacion.setFecha_de_prestamo(Date.valueOf(texto_cantidad.getValue()));
        libro_actualizacion.setFecha_que_se_ha_devuelto(Date.valueOf(texto_titulo.getValue()));
        libro_actualizacion.setFecha_que_se_tiene_que_devolver(Date.valueOf(texto_isbn.getValue()));
        libro_actualizacion.setSocio(texto_autor.getSelectionModel().getSelectedItem());
        libro_actualizacion.setEsta_devuelto(button_prestado.selectedProperty().getValue());

        try {
            
            if (ControladorPrestamo.libro.getId() == 0) {
                prestamoDBDAO.insertarRegistro(libro_actualizacion);
            }
            else{
                libro_actualizacion.setLibro_prestado(texto_editorial.getSelectionModel().getSelectedItem());
                prestamoDBDAO.actualizarRegistro(libro_actualizacion);
            }
            libro_actualizacion.getLibro_prestado().setEsta_prestado(libro_actualizacion.isEsta_devuelto());
            libroDAO.actualizarRegistro(libro_actualizacion.getLibro_prestado());
            Toast.makeText(App.stage, resource.getString(Constantes.ACTUALIZACION_AUTOR_CORRECTA), Constantes.TOAST_DELAY, Constantes.FADE_IN_DELAY0, Constantes.FADE_IN_DELAY0, COLOR_EXITO);

        } catch (XmlRpcException ex) {
            Toast.makeText(App.stage, resource.getString(Constantes.INSERCION_AUTOR_NO_CORRECTA), Constantes.TOAST_DELAY, Constantes.FADE_IN_DELAY0, Constantes.FADE_IN_DELAY0, COLOR_RED);
        } catch (Exception ex) {
            Toast.makeText(App.stage, resource.getString(Constantes.INSERCION_AUTOR_NO_CORRECTA), Constantes.TOAST_DELAY, Constantes.FADE_IN_DELAY0, Constantes.FADE_IN_DELAY0, COLOR_RED);
        }

        cambiar_a_cursor_normal();
        closeStage(event);
    }

    protected void cambiar_a_cursor_normal() {
        App.stage.getScene().setCursor(Cursor.DEFAULT);
    }

    public void closeStage(ActionEvent event) {
        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        stage.close();
    }

    private LocalDate transformar(java.util.Date fecha_de_prestamo) {
        Instant instant = Instant.ofEpochMilli(fecha_de_prestamo.getTime()); 
            LocalDateTime localDateTime = LocalDateTime.ofInstant(instant, ZoneId.systemDefault()); 
            LocalDate localDate = localDateTime.toLocalDate();
            return localDate;
    }

}
