/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ControladorVista;

import DAO.AutorDAO;
import DAO.EditorialDAO;
import DAO.LibroDAO;
import DAO.Linea_pedidoDAO;
import DAO.LocalizacionDAO;
import DAO.PedidoDAO;
import DAO.PrestamoDAO;
import DAO.RetiradaDAO;
import DAO.SocioDAO;
import DAO.TipoDAO;
import DAO.UsuarioDAO;
import DAOObjectDB.*;
import Modelo.Usuario;
import Thread.ThreadAbrirPaginaWeb;
import Thread.ThreadEsperaCopiaSeguridad;
import Thread.copiaSeguridad;
import Thread.restaurarCopia;
import Utilidades.Constantes;
import static Utilidades.Constantes.CARPETA_PERSONAL;
import Utilidades.DAOMetodos;
import Utilidades.Dialog;
import Utilidades.Toast;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXToggleButton;
import com.mycompany.proyectobiblioteca.App;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.BackingStoreException;
import java.util.prefs.InvalidPreferencesFormatException;
import java.util.prefs.Preferences;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import com.jfoenix.controls.JFXTextField;
import static com.mycompany.proyectobiblioteca.App.cerrar_dialog;
import static com.mycompany.proyectobiblioteca.App.crear_threads;
import java.io.BufferedReader;
import java.io.FileReader;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.ProgressBar;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.stage.DirectoryChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.apache.xmlrpc.XmlRpcException;

/**
 *
 * @author Javier Juan Uceda
 * @param <Tipo>
 */
public abstract class ControladorGlobal<Tipo> implements Initializable {

    /**
     * Fondo de la aplicación.
     */
    @FXML
    protected VBox fondo;
    /**
     * Conjunto de los elementos dentro de la aplicación.
     */
    @FXML
    protected GridPane conjunto;
    /**
     * Opciones de la parte superior de la aplicación
     */
    @FXML
    protected MenuBar menuBar;
    /**
     * Opciones relacionadas con el control de la aplicacion.
     */
    @FXML
    protected Menu archivo;
    /**
     * Boton de salir de la aplicacion
     */
    @FXML
    protected MenuItem salir;
    /**
     * Contirne los botones para cambiar entre pantallas.
     */
    @FXML
    protected Menu tipos;
    /**
     * Cambia a la pantalla libro.
     */
    @FXML
    protected MenuItem libro_tipo;
    /**
     * Cambia a la pantalla editorial.
     */
    @FXML
    protected MenuItem editorial_tipo;

    /**
     * Cambia a la pantalla prestamo.
     */
    @FXML
    protected MenuItem prestamo_tipo;
    /**
     * Realiza una copia de seguridad.
     */
    @FXML
    protected MenuItem copiadeseguridadmenuitem;
    /**
     * Cambia a la pantalla localizacion.
     */
    @FXML
    protected MenuItem localizacion_tipo;
    /**
     * Cambia a la pantalla tipo.
     */
    @FXML
    protected MenuItem tipo_tipo;
    /**
     * Cambia a la pantalla socio.
     */
    @FXML
    protected MenuItem socio_tipo;
    /**
     * Cambia a la pantalla retirada.
     */
    @FXML
    protected MenuItem retirada_tipo;
    /**
     * Cambia a la pantalla pedido.
     */
    @FXML
    protected MenuItem pedido_tipo;
    /**
     * Cambia a la pantalla editorial.
     */
    @FXML
    protected MenuItem lineapedido_tipo;
    /**
     * Cambia a la pantalla usuario si se ha iniciado sesion.
     */
    @FXML
    protected MenuItem usuario_tipo;
    /**
     * Cambia a la pantalla tipo.
     */
    @FXML
    protected MenuItem autor_tipo;
    /**
     * Son todas las opciones de importar los contenidos relacionados con la
     * aplicacion.
     */

    @FXML
    protected Menu importar;
    /**
     * Importa registros de libro.
     */
    @FXML
    protected MenuItem libro_importar;
    /**
     * Importa registros de editorial.
     */
    @FXML
    protected MenuItem editorial_importar;
    /**
     * Importa registros de prestamo.
     */
    @FXML
    protected MenuItem prestamo_importar;
    /**
     * Importa registros de localizacion.
     */
    @FXML
    protected MenuItem localizacion_importar;
    /**
     * Importa registros de tipo.
     */
    @FXML
    protected MenuItem tipo_importar;

    /**
     * Importa registros de socio.
     */
    @FXML
    protected MenuItem socio_importar;
    /**
     * Importa registros de retirada.
     */
    @FXML
    protected MenuItem retirada_importar;
    /**
     * Importa registros de pedido.
     */
    @FXML
    protected MenuItem pedido_importar;

    @FXML
    protected MenuItem autor_importar;
    /**
     * Todas las opciones de <b>cambiar el idioma</b> a la aplicación.
     */

    @FXML
    protected Menu cambiaridioma;
    /**
     * Cambia el idioma a español.
     */
    @FXML
    protected MenuItem espanyol;
    /**
     * Cambia el idioma a ingles.
     */
    @FXML
    protected MenuItem ingles;
    /**
     * Cambia el idioma a frances.
     */
    @FXML
    protected MenuItem frances;
    /**
     * Cambia el idioma a aleman.
     */
    @FXML
    protected MenuItem aleman;
    /**
     * Cambia el idioma a italiano.
     */
    @FXML
    protected MenuItem italiano;
    /**
     * Cambia el idioma a ruso.
     */
    @FXML
    protected MenuItem ruso;
    /**
     * Cambia el idioma a polaco.
     */
    @FXML
    protected MenuItem polaco;
    /**
     * Personaliza la aplicacion.
     */

    @FXML
    protected Menu personalizacion;
    /**
     * Cambia el color de fondo de la aplicacion.
     */
    @FXML
    protected MenuItem cambiarcolor;
    /**
     * Cambia el logo de la aplicacion.
     */
    @FXML
    protected MenuItem cambiarlogo;
    /**
     * Pone una imagen de fondo en la aplicacion.
     */
    @FXML
    protected MenuItem ponerimagendefondo;
    /**
     * Importa las preferencias.
     */
    @FXML
    protected MenuItem importarpreferencias;
    /**
     * Cambia el nombre a la aplicacion
     */
    @FXML
    protected MenuItem cambiarnombre;
    /**
     * Guarda las preferencias.
     */
    @FXML
    protected MenuItem guardarprferencias;
    /**
     * Donde esta la pantalla de insertar registros o modificar registros.
     */

    @FXML
    protected TabPane panel;
    /**
     * Pantalla de insertar registros.
     */
    @FXML
    protected Tab insertar;
    /**
     * Pantalla de insertar registros.
     */
    @FXML
    protected Tab modificar;
    /**
     * Pantalla de modificar registros.
     */
    @FXML
    protected Tab eliminar;
    /**
     * Imagen de la aplicacion de fondo.
     */
    @FXML
    protected ImageView imagen_aplicacion;
    /**
     * Exporta los datos de los registros seleccionados.
     */
    @FXML
    protected JFXButton exportar;
    /**
     * Inicia sesion o la cierra.
     */
    @FXML
    protected JFXButton boton_iniciar_sesion;
    /**
     * Opciones inferiores.
     */
    @FXML
    protected HBox opciones_abajo;
    /**
     * Actualiza los registros.
     */
    @FXML
    protected JFXButton actualizar_contenido;
    /**
     * Cambia a modo día o noche.
     */
    @FXML
    protected JFXToggleButton modo_dia_noche;
    /**
     * Muestra el progreso.
     */
    @FXML
    protected ProgressBar progreso;
    /**
     * Cancela el progreso.
     */
    @FXML
    protected JFXButton cancelar;
    /**
     * Programa las copias de seguridad.
     */
    @FXML
    protected MenuItem programarcopia;
    /**
     * Cambia al color de fondo o a la imagen de fondo.
     */
    @FXML
    protected JFXToggleButton color_imagen_fondo;
    /**
     * Cambia a pantalla completa.
     */
    @FXML
    protected MenuItem pantallacompleta;
    /**
     * Pone la pagina web de odoo.
     */

    @FXML
    protected Hyperlink paginaweb;
    /**
     * Pone el archivo de internalizacion.
     */

    protected ResourceBundle resource;
    /**
     * Pone el archivo de las preferencias.
     */
    protected Preferences preferences;
    /**
     * Guarda el usuario actual.
     */
    protected static Usuario usuario;
    /**
     * Dice si la sesion esta iniciada.
     */
    protected static boolean sesion_iniciada;
    /**
     * Guarda el idioma.
     */
    private String idioma_seleciconado;
    /**
     * <p>
     * Constante del
     * <b style="color:green;">color</b>
     * que tiene las operaciones que han sido un exito.
     * </p>
     */
    static final Color COLOR_EXITO = Color.GREEN;
    /**
     * <p>
     * Constante del
     * <b style="color:red;">color</b>
     * que tiene las operaciones que han sido un error.
     * </p>
     */
    static final Color COLOR_RED = Color.RED;
    /**
     * <p>
     * Constante del
     * <b style="color:yellow;">color</b>
     * que tiene las operaciones que han sido una advertencia.
     * </p>
     */

    static final Color COLOR_ADVERTENCIA = Color.YELLOW;

    /**
     * Constante que dice el ancho de las imagenes.
     */
    public static final double ANCHO = 30.0;

    /**
     * Constante que dice el alto de las imagenes.
     */
    public static final double ALTO = 30.0;

    /**
     * Elemento para los dialog.
     */
    @FXML
    private StackPane stackPane;
    /**
     * Thread que comprueba el tiempo hasta hacer una copia de seguridad.
     */
    public static ThreadEsperaCopiaSeguridad copiaseguridad;

    /**
     *
     * Código para inicializar el ControladorGlobal.
     *
     * @param arg0
     * @param pResource
     */
    public static AutorDAO autorDAO;
    public static AutorObjectDBDAO autorObjectDBDAO;
    public static EditorialDAO editorialDAO;
    public static EditorialObjectDBDAO editorialObjectDBDAO;
    public static LibroDAO libroDAO;
    public static LibroObjectDBDAO libroObjectDBDAO;
    public static Linea_PedidoObjectDBDAO linea_PedidoObjectDBDAO;
    public static Linea_pedidoDAO Linea_pedidoDBDAO;
    public static LocalizacionDAO localizacionDAO;
    public static LocalizacionObjectDBDAO localizacionDBDAO;
    public static PedidoObjectDBDAO pedidoDAO;
    public static PedidoDAO pedidoDBDAO;
    public static PrestamoObjectDBDAO prestamoDAO;
    public static PrestamoDAO prestamoDBDAO;
    public static RetiradaObjectDBDAO retiradaDAO;
    public static RetiradaDAO retiradaDBDAO;
    public static SocioObjectDBDAO socioDAO;
    public static SocioDAO socioDBDAO;
    public static TipoObjectDBDAO tipoDAO;
    public static TipoDAO tipoDBDAO;
    public static UsuarioObjectDBDAO usuarioDAO;
    public static UsuarioDAO usuarioDBDAO;

    @Override
    public void initialize(URL arg0, ResourceBundle pResource) {

        try {
            autorDAO = new AutorDAO();
            autorObjectDBDAO = new AutorObjectDBDAO();
            editorialDAO = new EditorialDAO();
            editorialObjectDBDAO = new EditorialObjectDBDAO();
            libroDAO = new LibroDAO();
            libroObjectDBDAO = new LibroObjectDBDAO();
            linea_PedidoObjectDBDAO = new Linea_PedidoObjectDBDAO();
            Linea_pedidoDBDAO = new Linea_pedidoDAO();
            localizacionDAO = new LocalizacionDAO();
            localizacionDBDAO = new LocalizacionObjectDBDAO();
            pedidoDAO = new PedidoObjectDBDAO();
            pedidoDBDAO = new PedidoDAO();
            prestamoDAO = new PrestamoObjectDBDAO();
            prestamoDBDAO = new PrestamoDAO();
            retiradaDAO = new RetiradaObjectDBDAO();
            retiradaDBDAO = new RetiradaDAO();
            socioDAO = new SocioObjectDBDAO();
            socioDBDAO = new SocioDAO();
            tipoDAO = new TipoObjectDBDAO();
            tipoDBDAO = new TipoDAO();
            usuarioDAO = new UsuarioObjectDBDAO();
            usuarioDBDAO = new UsuarioDAO();
        } catch (XmlRpcException ex) {
            Dialog.Mensaje_de_excepcion(ex);
        } catch (Exception ex) {
            Dialog.Mensaje_de_excepcion(ex);
        }

        preferences = Preferences.userRoot().node(Constantes.ARCHIVO_PREFERENCIAS);
        modo_dia_noche.selectedProperty().setValue(preferences.getBoolean(Constantes.MODO_DIA, Constantes.BOOLEAN_POR_DEFECTO));
        color_imagen_fondo.selectedProperty().setValue(preferences.getBoolean(Constantes.MODO_IMAGEN, Constantes.BOOLEAN_POR_DEFECTO));
        idioma_seleciconado = preferences.get(Constantes.IDIOMA, Constantes.IDIOMA_POR_DEFECTO);
        resource = pResource;
        copiaseguridad = new ThreadEsperaCopiaSeguridad();
        copiaseguridad.start();
        exportar.setTooltip(new Tooltip(resource.getString(Constantes.EXPORTAR_AYUDA_TOOLTIP)));
        String image = preferences.get(Constantes.IMAGEN_DE_LA_APLICACION, Constantes.VACIO);
        String imageFondo = preferences.get(Constantes.IMAGEN_DE_FONDO, Constantes.VACIO);
        Color color = Color.web(preferences.get(Constantes.COLOR, Constantes.COLOR_POR_DEFECTO));
        fondo.setBackground(new Background(new BackgroundFill(color, null, null)));
        usuario_tipo.setDisable(!sesion_iniciada);
        fondo.setOnKeyPressed((KeyEvent ke) -> {
            if (ke.getCode().toString().equals("F5")) {
                actualizar();
            }
        });

        //cambiar_progreso(70);
        int id = preferences.getInt(Constantes.USUARIO_RECORDADO, 0);
        try {
            if (id != 0) {

                Usuario usuario_t = new UsuarioDAO().buscarPorClavePrimaria(id);

                if (usuario_t != null) {
                    usuario = usuario_t;
                    boton_iniciar_sesion.setBackground(new Background(new BackgroundFill(COLOR_RED, null, null)));
                    boton_iniciar_sesion.setText(resource.getString(Constantes.CERRAR_SESION));

                    usuario_tipo.setDisable(sesion_iniciada || usuarioDBDAO.buscarTodos().isEmpty());
                }

            } else {
                boton_iniciar_sesion.setBackground(new Background(new BackgroundFill(COLOR_RED, null, null)));
                boton_iniciar_sesion.setText(resource.getString(Constantes.INICIAR_SESION));

            }
        } catch (XmlRpcException ex) {
            Dialog.Mensaje_de_excepcion(ex);
        } catch (Exception ex) {
            Dialog.Mensaje_de_excepcion(ex);
        }
        //cambiar_progreso(80);

        pantallacompleta.setGraphic(imagen_menu_bar(App.MENU_BAR_ARCHIVO + "pantalla_completa.png", ANCHO, ALTO));
        salir.setGraphic(imagen_menu_bar(App.MENU_BAR_ARCHIVO + "salir.jpg", ANCHO, ALTO));
        libro_tipo.setGraphic(imagen_menu_bar(App.MENU_BAR_TIPO_IMPORTAR + "tipodelibro.png", ANCHO, ALTO));
        editorial_tipo.setGraphic(imagen_menu_bar(App.MENU_BAR_TIPO_IMPORTAR + "editorial.jpg", ANCHO, ALTO));
        prestamo_tipo.setGraphic(imagen_menu_bar(App.MENU_BAR_TIPO_IMPORTAR + "prestamo.jpg", ANCHO, ALTO));
        localizacion_tipo.setGraphic(imagen_menu_bar(App.MENU_BAR_TIPO_IMPORTAR + "localizacion.jpg", ANCHO, ALTO));
        tipo_tipo.setGraphic(imagen_menu_bar(App.MENU_BAR_TIPO_IMPORTAR + "tipodelibro.png", ANCHO, ALTO));
        socio_tipo.setGraphic(imagen_menu_bar(App.MENU_BAR_TIPO_IMPORTAR + "socio.png", ANCHO, ALTO));
        retirada_tipo.setGraphic(imagen_menu_bar(App.MENU_BAR_TIPO_IMPORTAR + "socio.png", ANCHO, ALTO));
        pedido_tipo.setGraphic(imagen_menu_bar(App.MENU_BAR_TIPO_IMPORTAR + "pedido.png", ANCHO, ALTO));
//        importar.setGraphic(new ImageView("file:.png"));
        try {
            libro_importar.setGraphic(imagen_menu_bar(App.MENU_BAR_TIPO_IMPORTAR + "tipodelibro.png", ANCHO, ALTO));
            editorial_importar.setGraphic(imagen_menu_bar(App.MENU_BAR_TIPO_IMPORTAR + "editorial.jpg", ANCHO, ALTO));
            prestamo_importar.setGraphic(imagen_menu_bar(App.MENU_BAR_TIPO_IMPORTAR + "prestamo.jpg", ANCHO, ALTO));
            localizacion_importar.setGraphic(imagen_menu_bar(App.MENU_BAR_TIPO_IMPORTAR + "localizacion.jpg", ANCHO, ALTO));
            tipo_importar.setGraphic(imagen_menu_bar(App.MENU_BAR_TIPO_IMPORTAR + "tipodelibro.png", ANCHO, ALTO));
            socio_importar.setGraphic(imagen_menu_bar(App.MENU_BAR_TIPO_IMPORTAR + "socio.png", ANCHO, ALTO));
            autor_importar.setGraphic(imagen_menu_bar(App.MENU_BAR_TIPO_IMPORTAR + "socio.png", ANCHO, ALTO));
        } catch (Exception e) {
        }

//        lineapedido_importar.setGraphic(new ImageView("file:.png"));
//        cambiaridioma.setGraphic(new ImageView("file:.png"));
//        espanyol.setGraphic(new ImageView("file:.png"));
//        ingles.setGraphic(new ImageView("file:.png"));
//        frances.setGraphic(new ImageView("file:.png"));
//        aleman.setGraphic(new ImageView("file:.png"));
//        italiano.setGraphic(new ImageView("file:.png"));
//        ruso.setGraphic(new ImageView("file:.png"));
//        polaco.setGraphic(new ImageView("file:.png"));
//        personalizacion.setGraphic(new ImageView("file:.png"));
        cambiarcolor.setGraphic(imagen_menu_bar(App.MENU_BAR_PERSONALIZACION + "color.png", ANCHO, ALTO));
        cambiarlogo.setGraphic(imagen_menu_bar(App.MENU_BAR_PERSONALIZACION + "logo.png", ANCHO, ALTO));
        ponerimagendefondo.setGraphic(imagen_menu_bar(App.MENU_BAR_PERSONALIZACION + "imagen.png", ANCHO, ALTO));
        importarpreferencias.setGraphic(imagen_menu_bar(App.MENU_BAR_PERSONALIZACION + "abrir.jpg", ANCHO, ALTO));
        cambiarnombre.setGraphic(imagen_menu_bar(App.MENU_BAR_PERSONALIZACION + "color.png", ANCHO, ALTO));
        guardarprferencias.setGraphic(imagen_menu_bar(App.MENU_BAR_PERSONALIZACION + "exportar.jpg", ANCHO, ALTO));

        if (!image.isEmpty()) {
            poner_imagen(image);
        }

        if (!imageFondo.isEmpty()) {
            poner_imagen(image);
        }

        //cambiar_progreso(90);
        //cambiar_progreso(100);
        //cambiar_progreso(0);
        //cerrar_dialog();
        crear_threads();
        cambiar_modo();
        Cargar_datos();
        cambiar_fondo_aplicacion();
        cerrar_dialog();

    }

    /**
     * Pone la imagen en el menubar.
     *
     * @param imagen Nombre de la imagen
     * @param ancho El ancho de la imagen
     * @param alto El alto de la imagen
     */
    public static ImageView imagen_menu_bar(String imagen, double ancho, double alto) {
        ImageView Imagen = new ImageView(new File(imagen).toURI().toString());
        Imagen.setFitHeight(alto);
        Imagen.setFitWidth(ancho);
        return Imagen;
    }

    /**
     * Pone la pagina web.
     */
    public void ir_a_la_pagina_web() {
        new ThreadAbrirPaginaWeb().start();
    }

    /**
     * Sale de la aplicacion.
     */
    @FXML
    protected void salir() {
        Platform.exit();
        System.exit(0);
    }

    /**
     * Realiza la copia de seguridad.
     */
    @FXML
    protected void realizarCopiaDeSeguridad() {
        DirectoryChooser directoryChooser = new DirectoryChooser();
        directoryChooser.setInitialDirectory(CARPETA_PERSONAL);
        File selectedDirectory = directoryChooser.showDialog(App.stage);
        if (selectedDirectory != null && !progreso.isVisible()) {
            String archivo = selectedDirectory.getAbsolutePath();
            copiaSeguridad copia = new copiaSeguridad(archivo, progreso, cancelar);
            copia.start();
        }
    }

    /**
     * Restaura la copia de seguridad.
     */
    @FXML
    protected void restaurarCopiaDeSeguridad() {

        File copia = App.fileChooser(resource.getString(Constantes.ELIGE_UNA_IMAGEN));
        if (copia != null && !progreso.isVisible()) {
            restaurarCopia Copia = new restaurarCopia(copia, progreso, cancelar);
            Copia.start();
        }
    }

    /**
     * Muestra como se hacen las copias de seguridad semanalmente.
     */
    @FXML
    protected void mostrarmenucopias() {
        Parent parent = null;
        try {

            FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("dialogprogramarcopia.fxml"), resource);
            parent = fxmlLoader.load();
            Scene scene = new Scene(parent);
            Stage stage = new Stage();
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setScene(scene);
            stage.showAndWait();
            cambiar_a_cursor_normal();

        } catch (IOException ex) {
            Dialog.Mensaje_de_excepcion(ex);
        } catch (Exception ex) {
            Dialog.Mensaje_de_excepcion(ex);
        }
    }

    /**
     * Inicia la sesion o sale según el booleano sesion iniciada.
     */
    @FXML
    protected void iniciar_salir_sesion() {
        Parent parent = null;
        try {

            sesion_iniciada = (usuario == null);

            if (sesion_iniciada) {

                FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("iniciosesion.fxml"), resource);
                parent = fxmlLoader.load();
                Scene scene = new Scene(parent);
                Stage stage = new Stage();
                stage.initModality(Modality.APPLICATION_MODAL);
                stage.setScene(scene);
                stage.showAndWait();
                sesion_iniciada = (usuario == null);
                if (sesion_iniciada) {
                    boton_iniciar_sesion.setBackground(new Background(new BackgroundFill(COLOR_EXITO, null, null)));
                    boton_iniciar_sesion.setText(resource.getString(Constantes.INICIAR_SESION));
                    App.Cambiar_Pantalla(fondo.getId(), App.obtenerIdioma(preferences.get(Constantes.IDIOMA, Constantes.IDIOMA_POR_DEFECTO)));
                } else {
                    preferences.putInt(Constantes.USUARIO_RECORDADO, 0);
                    boton_iniciar_sesion.setBackground(new Background(new BackgroundFill(COLOR_RED, null, null)));
                    boton_iniciar_sesion.setText(resource.getString(Constantes.CERRAR_SESION));
                    cambiar_a_cursor_normal();
                }

            } else {
                usuario = null;
                preferences.putInt(Constantes.USUARIO_RECORDADO, 0);
                boton_iniciar_sesion.setBackground(new Background(new BackgroundFill(COLOR_RED, null, null)));
                boton_iniciar_sesion.setText(resource.getString(Constantes.INICIAR_SESION));
                App.Cambiar_Pantalla(fondo.getId(), App.obtenerIdioma(preferences.get(Constantes.IDIOMA, Constantes.IDIOMA_POR_DEFECTO)));
            }

            usuario_tipo.setDisable(sesion_iniciada);

        } catch (IOException ex) {
            Dialog.Mensaje_de_excepcion(ex);
        } catch (Exception ex) {
            Dialog.Mensaje_de_excepcion(ex);
        }
    }

    @FXML
    protected void cambiar_a_cursor_mano() {
        //App.stage.getScene().setCursor(Cursor.HAND);
    }

    @FXML
    protected void cambiar_a_cursor_normal() {
        //App.stage.getScene().setCursor(Cursor.DEFAULT);
    }

    @FXML
    protected void cambiar_a_cursor_esperando() {
        //App.stage.getScene().setCursor(Cursor.WAIT);
    }

    /**
     * Comprueba que esta vacio.
     *
     * @param action informacion del textField
     */
    @FXML
    protected void comprobar_si_el_textfield_esta_vacio(ActionEvent action) {
        JFXTextField textField = ((JFXTextField) action.getSource());
        String texto = textField.getText();
        Color color = texto.length() > 0 ? COLOR_EXITO : COLOR_RED;
        textField.focusColorProperty().set(color);
        textField.unFocusColorProperty().set(color);
    }

    /**
     * Comprueba que esta vacio cuando escribe.
     *
     * @param action informacion del textField cuando escribe
     */
    @FXML
    protected void comprobar_escribir_el_textfield_esta_vacio(KeyEvent action) {
        JFXTextField textField = ((JFXTextField) action.getSource());
        String texto = textField.getText();
        Color color = texto.length() > 0 ? COLOR_EXITO : COLOR_RED;
        textField.focusColorProperty().set(color);
        textField.unFocusColorProperty().set(color);
    }

    /**
     * Comprueba que esta vacio cuando pasa por encima.
     *
     * @param action informacion del textField cuando pasa por encima.
     */
    @FXML
    protected void comprobar_pasar_el_textfield_esta_vacio(MouseEvent action) {
        JFXTextField textField = ((JFXTextField) action.getSource());
        String texto = textField.getText();
        Color color = texto.length() > 0 ? COLOR_EXITO : COLOR_RED;
        textField.focusColorProperty().set(color);
        textField.unFocusColorProperty().set(color);

    }

    /**
     * Comprueba que esta vacio cuando pasa por encima.
     *
     * @param action informacion del combobox cuando pasa por encima.
     */
    @FXML
    protected void comprobar_combobox_esta_vacio(MouseEvent action) {
        JFXComboBox comboBox = ((JFXComboBox) action.getSource());
        comboBox.focusColorProperty().set(comboBox.getSelectionModel().isEmpty() ? COLOR_EXITO : COLOR_RED);

        Color color = !(comboBox.getSelectionModel().getSelectedIndex() == -1) ? COLOR_EXITO : COLOR_RED;
        comboBox.focusColorProperty().set(color);
        comboBox.unFocusColorProperty().set(color);

    }

    /**
     * Comprueba que esta vacio cuando pasa por encima y que es un numero.
     *
     * @param action informacion del textfield cuando pasa por encima y que es
     * un numero.
     */
    @FXML
    protected void comprobar_si_el_textfield_esta_vacio_y_numero(ActionEvent action) {
        JFXTextField textField = ((JFXTextField) action.getSource());

        String texto = textField.getText();
        Color color = texto.length() > 0 && es_un_numero(texto) ? COLOR_EXITO : COLOR_RED;
        textField.focusColorProperty().set(color);
        textField.unFocusColorProperty().set(color);

    }

    /**
     * Comprueba que esta vacio cuando pasa por encima y que es un numero.
     *
     * @param action informacion del textfield cuando pasa por encima y que es
     * un numero.
     */
    @FXML
    protected void comprobar_escribir_el_textfield_esta_vacio_y_numero(KeyEvent action) {
        JFXTextField textField = ((JFXTextField) action.getSource());
        String texto = textField.getText();
        Color color = texto.length() > 0 && es_un_numero(texto) ? COLOR_EXITO : COLOR_RED;
        textField.focusColorProperty().set(color);
        textField.unFocusColorProperty().set(color);
    }

    /**
     * Comprueba que es un numero.
     *
     * @param texto textto a comprobar.
     * @return
     */
    public boolean es_un_numero(String texto) {
        boolean numero = true;
        try {
            Integer.parseInt(texto);
        } catch (NumberFormatException e) {
            numero = Constantes.BOOLEAN_POR_DEFECTO;
        }
        return numero;
    }

    @FXML
    protected void comprobar_si_el_combobox_esta_vacio(ActionEvent action) {
        App.stage.getScene().setCursor(Cursor.WAIT);
    }

    /**
     * Cambia a pantalla completa.
     */
    @FXML
    protected void cambiarpantallacompleta() {
        boolean esta_a_pantalla_completa = !App.stage.isFullScreen();
        pantallacompleta.setText(esta_a_pantalla_completa ? resource.getString(Constantes.SALIR_DE_LA_PANTALLA_COMPLETA) : resource.getString(Constantes.PONER_EN_PANTALLA_COMPLETA));
        App.stage.setFullScreen(esta_a_pantalla_completa);
        preferences.putBoolean(Constantes.PANTALLA_COMPLETA, esta_a_pantalla_completa);
    }

    /**
     * Cambia el fondo de la aplicacion.
     */
    @FXML
    protected void cambiar_fondo_aplicacion() {
        boolean modo = color_imagen_fondo.isSelected();
        fondo.setBackground(new Background(new BackgroundFill(Color.WHITE, null, null)));

        preferences.putBoolean(Constantes.MODO_IMAGEN, modo);
        String texto = modo ? resource.getString(Constantes.MODO_FONDO_IMAGEN) : resource.getString(Constantes.MODO_FONDO_COLOR);
        color_imagen_fondo.setText(texto);
        BackgroundFill background;
        if (!modo) {
            background = new BackgroundFill(Color.web(preferences.get(Constantes.COLOR, Constantes.COLOR_POR_DEFECTO)), null, null);
            fondo.setBackground(new Background(background));
        } else {
            String imagen = Constantes.VACIO;
            FileInputStream input = null;
            imagen = preferences.get(Constantes.IMAGEN_DE_FONDO, Constantes.VACIO);

            if (!imagen.isEmpty()) {
                try {

                    input = new FileInputStream(imagen);
                } catch (FileNotFoundException ex) {
                    Dialog.Mensaje_de_excepcion(ex);
                }

                if (input != null) {
                    Image image = new Image(input);

                    BackgroundImage backgroundimage = new BackgroundImage(image,
                            BackgroundRepeat.NO_REPEAT,
                            BackgroundRepeat.NO_REPEAT,
                            BackgroundPosition.DEFAULT,
                            BackgroundSize.DEFAULT);

                    Background background_modo = new Background(backgroundimage);

                    fondo.setBackground(background_modo);
                }

            }

        }
    }

    /**
     *
     */
    @FXML
    public abstract void insertar();

    /**
     *
     */
    @FXML
    public abstract void modificar();

    /**
     *
     */
    @FXML
    public abstract void eliminar();

    /**
     * Cambia el idioma.
     */
    @FXML
    protected void cambiar_idioma(ActionEvent action) throws IOException {
        Locale idioma = App.obtenerIdioma(((MenuItem) (action.getSource())).getId());
        MenuItem seleccionado = ((MenuItem) (action.getSource()));
        idioma_seleciconado = seleccionado.getId();
        preferences.put(Constantes.IDIOMA, idioma_seleciconado);
        fondo.getScene().getWindow().hide();
        App.Cambiar_Pantalla(fondo.getId(), idioma);
    }

    public void cambiar_prueba(ActionEvent action) throws IOException {
        Parent parent;
        try {
            parent = FXMLLoader.load(App.class.getResource("dialogprueba.fxml"));
            JFXDialogLayout dialogLayout = new JFXDialogLayout();
            dialogLayout.setBody(parent);
            JFXDialog dialog = new JFXDialog(stackPane, dialogLayout, JFXDialog.DialogTransition.BOTTOM);
            dialog.show();
        } catch (IOException ex) {
            Dialog.Mensaje_de_excepcion(ex);
        }
    }

    /**
     * Cambia de pantalla.
     */
    @FXML
    protected void cambiar_de_tipo(ActionEvent action) {
        try {
            String pantalla_a_cambiar = ((MenuItem) action.getSource()).getId();
            Locale idioma = App.obtenerIdioma(idioma_seleciconado);
            fondo.getScene().getWindow().hide();
            App.Cambiar_Pantalla(pantalla_a_cambiar, idioma);
        } catch (IOException ex) {
            ex.printStackTrace();
            //Dialog.Mensaje_de_excepcion(ex);
        }
    }

    /**
     * Cambia de modo dia a modo noche o al reves.
     */
    @FXML
    protected void cambiar_modo() {
        boolean modo = modo_dia_noche.isSelected();
        preferences.putBoolean(Constantes.MODO_DIA, modo);
        modo_dia_noche.setText(resource.getString(modo ? Constantes.MODO_DIA : Constantes.MODO_NOCHE));
        String css_a_poner = App.class.getResource("css/" + (modo ? "estilo" : "estilo_noche") + ".css").toExternalForm();
        fondo.getStylesheets().set(0, css_a_poner);
        conjunto.getStylesheets().set(0, css_a_poner);
        exportar.getStylesheets().set(0, css_a_poner);

        opciones_abajo.getStylesheets().set(0, css_a_poner);
        actualizar_contenido.getStylesheets().set(0, css_a_poner);
    }

    /**
     * Importa los registros.
     *
     * @param action Informacion de la pantalla.
     */
    @FXML
    protected void importar_registros(ActionEvent action) {
        String id = ((MenuItem) action.getSource()).getId();
        ObservableList<FileChooser.ExtensionFilter> filtros = FXCollections.observableArrayList();
        filtros.add(new FileChooser.ExtensionFilter(resource.getString(Constantes.ELIGE_UNA_IMAGEN) + Constantes.CSV, ".csv"));
        File archivo = App.fileChooser(Constantes.ELIGE_UNA_IMAGEN);

        try {

            if (archivo != null) {
                String[] valores = Leer_archivo(archivo).split(System.lineSeparator());

                for (String texto_linea : valores) {
                    String[] linea = texto_linea.split(Constantes.SEPARADOR_EXPORTACION);
                    switch (id) {
                        case "libro_importar":

                            Modelo.Libro libro = new Modelo.Libro();
                            libro.setTitulo(linea[0]);
                            libro.setAutor(autorDAO.buscarPorClavePrimaria(Integer.parseInt(linea[1])));
                            libro.setCantidad(Integer.parseInt(linea[2]));
                            libro.setEditorial(editorialDAO.buscarPorClavePrimaria(Integer.parseInt(linea[3])));
                            libro.setEstado_del_libro(linea[4]);
                            libro.setISBN(linea[5]);
                            libro.setLocalizacion(localizacionDAO.buscarPorClavePrimaria(Integer.parseInt(linea[6])));
                            libro.setTipo_del_libro(tipoDBDAO.buscarPorClavePrimaria(Integer.parseInt(linea[7])));
                            libroDAO.insertarRegistro(libro);
                            break;
                        case "editorial_importar":
                            Modelo.Editorial editorial = new Modelo.Editorial();
                            editorial.setNombre(linea[0]);
                            editorial.setFacebook(linea[1]);
                            editorial.setCIF(linea[2]);
                            editorial.setDireccion(linea[3]);
                            editorial.setLocalidad(linea[4]);
                            editorial.setInstagram(linea[5]);
                            editorial.setPagina_web(linea[6]);
                            editorial.setTelefono(Integer.parseInt(linea[7]));
                            editorial.setTwitter(linea[8]);
                            if (linea.length > 8) {
                                editorial.setTwitter(linea[8]);
                            }
                            editorialDAO.insertarRegistro(editorial);
                            break;
                        case "prestamo_importar":

                            Modelo.Prestamo prestamo = new Modelo.Prestamo();
                            prestamo.setFecha_de_prestamo(DAOMetodos.obtener_fecha(linea[0]));
                            prestamo.setFecha_que_se_ha_devuelto(DAOMetodos.obtener_fecha(linea[1]));
                            prestamo.setFecha_que_se_tiene_que_devolver(DAOMetodos.obtener_fecha(linea[2]));
                            prestamo.setLibro_prestado(libroDAO.buscarPorClavePrimaria(Integer.parseInt(linea[3])));
                            prestamo.setSocio(socioDBDAO.buscarPorClavePrimaria(Integer.parseInt(linea[4])));
                            prestamoDBDAO.insertarRegistro(prestamo);
                            break;
                        case "localizacion_importar":
                            Modelo.Localizacion localizacion = new Modelo.Localizacion();
                            localizacion.setNombre(linea[0]);
                            new DAO.LocalizacionDAO().insertarRegistro(localizacion);
                            break;
                        case "tipo_importar":
                            Modelo.Tipo tipo = new Modelo.Tipo();
                            tipo.setNombre(linea[0]);
                            tipo.setDescripcion(linea[1]);
                            tipoDBDAO.insertarRegistro(tipo);
                            break;
                        case "socio_importar":
                            Modelo.Socio socio = new Modelo.Socio();
                            socio.setNombre(linea[0]);
                            socio.setDNI(linea[1]);
                            socio.setFecha_de_nacimiento(DAOMetodos.obtener_fecha(linea[2]));
                            socio.setDireccion(linea[3]);
                            socio.setLocalidad(linea[4]);
                            socioDBDAO.insertarRegistro(socio);
                            break;
                        case "autor_importar":

                            Modelo.Autor autor = new Modelo.Autor();
                            autor.setNombre(linea[0]);
                            autor.setFacebook(linea[1]);
                            autor.setFecha_de_nacimiento(DAOMetodos.obtener_fecha(linea[2]));
                            autor.setInstagram(linea[3]);
                            autor.setPagina_web(linea[4]);
                            autor.setTelefono(Integer.parseInt(linea[5]));
                            if (linea.length > 6) {
                                autor.setTwitter(linea[6]);
                            }
                            autorDAO.insertarRegistro(autor);
                            break;
                        default: ;
                            break;
                    }
                }

            }

            Dialog.Dialogo_de_confirmacion(resource.getString(Constantes.CONFIRMACION_EXPORTACION));

        } catch (XmlRpcException ex) {
            Dialog.Mensaje_de_excepcion(ex);
        } catch (Exception ex) {
            Dialog.Mensaje_de_excepcion(ex);
        }

    }

    /**
     * Lee el archivo.
     *
     * @param archivo que lee
     * @return un string que es el contenido del archivo.
     */
    protected String Leer_archivo(File archivo) {
        StringBuilder texto = new StringBuilder();
        try ( BufferedReader lectura = new BufferedReader(new FileReader(archivo))) {
            String linea = Constantes.VACIO;
            while ((linea = lectura.readLine()) != null) {
                texto.append(linea).append(System.lineSeparator());
            }
        } catch (Exception e) {
        }
        return texto.toString();
    }

    /**
     * Cambia el color
     */
    @FXML
    protected void cambiar_color() {

        Color color = Dialog.Dialogo_de_color();
        fondo.setBackground(new Background(new BackgroundFill(color, null, null)));
        preferences.put(Constantes.COLOR, color.toString());
    }

    /**
     * Cambia el logo de la aplicacion.
     */
    @FXML
    protected void cambiar_logo() {
        FileChooser.ExtensionFilter filtro1 = new FileChooser.ExtensionFilter(resource.getString(Constantes.ARCHIVOS) + Constantes.JPEG, Constantes.JPEG_TIPO);
        FileChooser.ExtensionFilter filtro2 = new FileChooser.ExtensionFilter(resource.getString(Constantes.ARCHIVOS) + Constantes.PNG, Constantes.PNG_TIPO);
        FileChooser.ExtensionFilter filtro3 = new FileChooser.ExtensionFilter(resource.getString(Constantes.ARCHIVOS) + Constantes.JPG, "*.jpg");
        ObservableList<FileChooser.ExtensionFilter> filtros = FXCollections.observableArrayList();
        filtros.add(filtro1);
        filtros.add(filtro2);
        filtros.add(filtro3);
        String imagen = App.fileChooser(resource.getString(Constantes.ELIGE_UNA_IMAGEN), filtros).getAbsoluteFile().toString();
        poner_imagen(imagen);

        preferences.put(Constantes.IMAGEN_DE_LA_APLICACION, imagen);
    }

    /**
     * Pone la imagen.
     */
    private void poner_imagen(String imagen) {
        if ((new File(imagen)).exists()) {
            FileInputStream input = null;
            try {
                input = new FileInputStream(imagen);
            } catch (FileNotFoundException ex) {

            }
            Image image = new Image(input);
            imagen_aplicacion.setImage(image);
        }

    }

    /**
     * Cambia la imagen de fondo.
     */
    @FXML
    protected void poner_imagen_de_fondo() {
        FileChooser.ExtensionFilter filtro1 = new FileChooser.ExtensionFilter(resource.getString(Constantes.ARCHIVOS) + Constantes.JPEG, Constantes.JPEG_TIPO);
        FileChooser.ExtensionFilter filtro2 = new FileChooser.ExtensionFilter(resource.getString(Constantes.ARCHIVOS) + Constantes.PNG, Constantes.PNG_TIPO);
        FileChooser.ExtensionFilter filtro3 = new FileChooser.ExtensionFilter(resource.getString(Constantes.ARCHIVOS) + Constantes.JPG, "*.jpg");
        ObservableList<FileChooser.ExtensionFilter> filtros = FXCollections.observableArrayList();
        filtros.add(filtro1);
        filtros.add(filtro2);
        filtros.add(filtro3);
        String imagen = App.fileChooser(resource.getString(Constantes.ELIGE_UNA_IMAGEN), filtros).getAbsoluteFile().toString();
        poner_imagen(imagen);
        preferences.put(Constantes.IMAGEN_DE_FONDO, imagen);
    }

    /**
     * Importa las preferencias.
     */
    @FXML
    protected void importar_preferencias() {
        FileChooser.ExtensionFilter filtro1 = new FileChooser.ExtensionFilter(resource.getString(Constantes.ARCHIVOS) + Constantes.XML, Constantes.XML_TIPO);
        ObservableList<FileChooser.ExtensionFilter> filtros = FXCollections.observableArrayList();
        filtros.add(filtro1);
        try {
            File import_preferences = App.fileChooser(resource.getString(Constantes.ELIGE_PREFERENCIAS), filtros);
            Preferences preferences_import = Preferences.userRoot();

            InputStream is = new BufferedInputStream(new FileInputStream(import_preferences));

            if (es_preferences_valido(preferences, preferences_import)) {
                Preferences.importPreferences(is);
                String[] keys = preferences_import.keys();
                preferences.put(Constantes.IDIOMA, preferences_import.get(Constantes.IDIOMA, Constantes.IDIOMA_POR_DEFECTO));
                preferences.put(Constantes.IMAGEN_DE_FONDO, preferences_import.get(Constantes.IMAGEN_DE_FONDO, Constantes.VACIO));
                preferences.putBoolean(Constantes.MODO_DIA, preferences_import.getBoolean(Constantes.MODO_DIA, Constantes.BOOLEAN_POR_DEFECTO));
                preferences.putBoolean(Constantes.PANTALLA_COMPLETA, preferences_import.getBoolean(Constantes.PANTALLA_COMPLETA, Constantes.BOOLEAN_POR_DEFECTO));
                preferences.putBoolean(Constantes.MAXIMIZADO, preferences_import.getBoolean(Constantes.MAXIMIZADO, Constantes.BOOLEAN_POR_DEFECTO));
                preferences.put(Constantes.COLOR, preferences_import.get(Constantes.COLOR, Constantes.COLOR_POR_DEFECTO));
                preferences.put(Constantes.IMAGEN_DE_FONDO, preferences_import.get(Constantes.IMAGEN_DE_FONDO, Constantes.VACIO));
                preferences.putBoolean(Constantes.MODO_IMAGEN, preferences_import.getBoolean(Constantes.MODO_IMAGEN, Constantes.BOOLEAN_POR_DEFECTO));
                preferences.put(Constantes.TITULO_APLICACION, preferences_import.get(Constantes.TITULO_APLICACION, Constantes.VACIO));
                is.close();
                preferences_import.flush();
                Toast.makeText(App.stage, resource.getString(Constantes.PREFERENCIAS_EXITO), Constantes.TOAST_DELAY, Constantes.FADE_IN_DELAY0, Constantes.FADE_IN_DELAY0, COLOR_EXITO);
                App.Cambiar_Pantalla(fondo.getId(), App.obtenerIdioma(preferences_import.get(keys[0], Constantes.IDIOMA_POR_DEFECTO)));
            } else {
                Toast.makeText(App.stage, resource.getString(Constantes.PREFERENCIAS_NO_EXITO), Constantes.TOAST_DELAY, Constantes.FADE_IN_DELAY0, Constantes.FADE_IN_DELAY0, COLOR_RED);
            }
            preferences_import.removeNode();
            preferences_import.flush();

        } catch (FileNotFoundException ex) {
            Dialog.Mensaje_de_excepcion(ex);
        } catch (IOException | InvalidPreferencesFormatException | BackingStoreException ex) {
            Dialog.Mensaje_de_excepcion(ex);
        }
    }

    /**
     * Cambia el nombre de la aplicacion
     */
    @FXML
    protected void cambiar_nombre() {
        String titulo = Dialog.Dialogo_de_textField();
        if (titulo != null) {
            preferences.put(Constantes.TITULO_APLICACION, titulo);
            App.stage.setTitle(preferences.get(Constantes.TITULO_APLICACION, Constantes.TITULO_APLICACION_POR_DEFECTO));
        }
    }

    @FXML
    public abstract void Exportar();

    @FXML
    public abstract void Cargar_datos();

    @FXML
    public abstract void actualizar();

    /**
     * Guarda las prefencias.
     */
    @FXML
    protected void guardar_prferencias() {
        try {
            Preferences pref_temporal = Preferences.userRoot();
            String[] keys = this.preferences.keys();

            pref_temporal.put(Constantes.IDIOMA, this.preferences.get(Constantes.IDIOMA, Constantes.IDIOMA_POR_DEFECTO));
            pref_temporal.put(Constantes.IMAGEN_DE_LA_APLICACION, this.preferences.get(Constantes.IMAGEN_DE_LA_APLICACION, Constantes.VACIO));
            pref_temporal.putBoolean(Constantes.MODO_DIA, this.preferences.getBoolean(Constantes.MODO_DIA, Constantes.BOOLEAN_POR_DEFECTO));
            pref_temporal.putBoolean(Constantes.PANTALLA_COMPLETA, this.preferences.getBoolean(Constantes.PANTALLA_COMPLETA, Constantes.BOOLEAN_POR_DEFECTO));
            pref_temporal.putBoolean(Constantes.MAXIMIZADO, this.preferences.getBoolean(Constantes.MAXIMIZADO, Constantes.BOOLEAN_POR_DEFECTO));
            pref_temporal.putBoolean(Constantes.MODO_IMAGEN, preferences.getBoolean(Constantes.MODO_IMAGEN, Constantes.BOOLEAN_POR_DEFECTO));
            pref_temporal.put(Constantes.COLOR, preferences.get(Constantes.COLOR, Constantes.COLOR_POR_DEFECTO));
            pref_temporal.put(Constantes.IMAGEN_DE_FONDO, preferences.get(Constantes.IMAGEN_DE_FONDO, Constantes.VACIO));
            pref_temporal.put(Constantes.TITULO_APLICACION, preferences.get(Constantes.TITULO_APLICACION, Constantes.VACIO));
            pref_temporal.exportNode(new FileOutputStream(App.guardar_fileChooser(resource.getString(Constantes.GUARDAR_PREFERENCIAS))));
            Toast.makeText(App.stage, resource.getString(Constantes.PREFERENCIAS_GUARDADAS_CON_EXITO), Constantes.TOAST_DELAY, Constantes.FADE_IN_DELAY0, Constantes.FADE_IN_DELAY0, COLOR_EXITO);
        } catch (FileNotFoundException ex) {
            Dialog.Mensaje_de_excepcion(ex);
        } catch (IOException | BackingStoreException ex) {
            Dialog.Mensaje_de_excepcion(ex);
        }
    }

    /**
     * Comprueba que un archivo de preferencias válido.
     */
    private boolean es_preferences_valido(Preferences preferences, Preferences preferences0) throws BackingStoreException {
        String[] keys1 = preferences.keys();
        String[] keys2 = preferences0.keys();
        boolean valido = true;
        int tamanyo = keys1.length;
        valido = tamanyo == keys2.length;

        for (int contador = 0; contador < tamanyo && valido; contador++) {
            valido = keys1[contador].equals(keys2[contador]);
        }
        return valido;
    }

}
