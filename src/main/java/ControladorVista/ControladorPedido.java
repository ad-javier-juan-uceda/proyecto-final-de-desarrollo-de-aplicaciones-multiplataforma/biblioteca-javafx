/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ControladorVista;

import static ControladorVista.ControladorGlobal.COLOR_EXITO;
import static ControladorVista.DialogModificarPedido.pedido;
import DAO.PedidoDAO;
import DAO.EditorialDAO;
import DAO.LibroDAO;
import DAO.LocalizacionDAO;
import DAO.SocioDAO;
import DAO.TipoDAO;
import Modelo.Pedido;
import Modelo.Editorial;
import Modelo.Libro;
import Modelo.Linea_pedido;
import Modelo.Localizacion;
import Modelo.Tipo;
import Modelo.Usuario;
import Thread.ThreadPaginaWebEscrito;
import Utilidades.Constantes;
import static Utilidades.Constantes.CARPETA_PERSONAL;
import Utilidades.Dialog;
import Utilidades.Toast;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.controls.JFXToggleButton;
import com.mycompany.proyectobiblioteca.App;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Date;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Cursor;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.SingleSelectionModel;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
import org.apache.xmlrpc.XmlRpcException;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import javafx.stage.DirectoryChooser;

/**
 *
 * @author batoi
 */
public class ControladorPedido extends ControladorGlobal<Pedido> {

    private PedidoDAO librosDAO;
    private ObservableList<Pedido> lista_libros;

    @FXML
    private Label lblUsuario;
    @FXML
    private Label lblFechaDeRealizacion;
    @FXML
    private Label lblFechaDeEntrega;
    @FXML
    private Label lblArticulo;

    @FXML
    private JFXComboBox<Usuario> comboBoxUsuario;
    @FXML
    private DatePicker datePickerFechaRealizacion;
    @FXML
    private DatePicker datePickerFechaDeEntrega;
    @FXML
    private JFXComboBox<Libro> comboBoxArticulo;
    @FXML
    private JFXButton limpiarBusqueda;

    @FXML
    private Label label_titulo;
    @FXML
    private Label label_isbn;
    @FXML
    private Label elegir_editorial;
    @FXML
    private Label prestado;
    @FXML
    private JFXToggleButton button_prestado;
    @FXML
    private Label elegir_autor;
    @FXML
    private Label elegir_tipo;
    @FXML
    private Label elegir_localizaicion;
    @FXML
    private Label elegir_estado;
    @FXML
    private GridPane gridpane;
    @FXML
    private GridPane opciones_modificar;
    @FXML
    private HBox opciones_buscar;
    @FXML
    private HBox opciones_buscar_2;
    @FXML
    private HBox opciones_buscar_3;
    @FXML
    private VBox fondo;
    @FXML
    private JFXTextField textfield_buscar_2;
    @FXML
    private JFXTextField textfield_cantidad;
    @FXML
    private JFXTextField textfield_paginas;
    @FXML
    private Tab tabinsertar;
    @FXML
    private Tab tabmodificar;

    @FXML
    private Label label_contenido;
    @FXML
    private Label label_autor;
    @FXML
    private Label label_editorial;
    @FXML
    private Label label_tipo;
    @FXML
    private Label label_estado;
    @FXML
    private Label label_localizacion;
    @FXML
    private Label label_cantidad;
    @FXML
    private Label label_paginas;

    @FXML
    private Label label_nombre;
    @FXML
    private Label label_telefono;
    @FXML
    private Label label_fecha;
    @FXML
    private Label label_pagina_web;
    @FXML
    private Label label_facebook;
    @FXML
    private Label label_twitter;
    @FXML
    private Label label_instagram;

    @FXML
    private JFXTextField texto_nombre;
    @FXML
    private JFXTextField texto_telefono;
    @FXML
    private JFXTextField texto_pagina_web;
    @FXML
    private JFXTextField texto_facebook;
    @FXML
    private JFXTextField texto_twitter;
    @FXML
    private JFXTextField texto_instagram;
    @FXML
    private DatePicker texto_fecha;

    @FXML
    private JFXButton button_limpiar;
    @FXML
    private JFXButton cancelar;
    @FXML
    private JFXButton actualizar;
    @FXML
    private Label cantidad;
    @FXML
    private JFXTextField texto_cantidad;
    @FXML
    private JFXTextField texto_titulo;
    @FXML
    private JFXTextField texto_isbn;
    @FXML
    private Label paginas;
    @FXML
    private JFXTextField texto_paginas;

    @FXML
    private TableColumn<Pedido, String> Titulo;
    @FXML
    private TableColumn<Pedido, String> ISBN;
    @FXML
    private TableColumn<Pedido, String> estado_del_libro;
    @FXML
    private TableColumn<Pedido, String> cantidad_scenebuilder;
    @FXML
    private TableColumn<Pedido, String> paginas_scenebuilder;
    @FXML
    private TableColumn<Pedido, Void> editorial;
    @FXML
    private TableColumn<Pedido, Void> localizacion;
    @FXML
    private TableColumn<Pedido, Void> tipo_del_libro;
    @FXML
    private TableColumn<Pedido, Void> autor;

    @FXML
    private TableView<Pedido> tabla_modificar_libros;
    public static Pedido libro;
    private ContextMenu menu;
    private JFXComboBox<String> combobox_tipos_atributo;

    @FXML
    private DatePicker texto_fecha_pedido;
    @FXML
    private DatePicker texto_entrega;
    @FXML
    private TableColumn<Linea_pedido, String> columnalinro;
    @FXML
    private TableColumn<Linea_pedido, String> columnacantidad;
    @FXML
    private TableColumn<Linea_pedido, String> columnaprecio;

    @FXML
    private TableView<Linea_pedido> tabla;

    @Override
    public void insertar() {
        cambiar_a_cursor_esperando();
        Pedido autor_actualizacion = new Pedido();
        cambiar_a_cursor_esperando();

        autor_actualizacion.setFecha_de_entrega(Date.valueOf(texto_entrega.getValue()));
        autor_actualizacion.setFecha_de_realizacion(Date.valueOf(texto_fecha_pedido.getValue()));
        autor_actualizacion.setUsuario(usuario);

        try {
            int id = pedidoDBDAO.insertarRegistro(autor_actualizacion);
            autor_actualizacion.setId(id);

            libro = autor_actualizacion;

            dialogo();

            Toast.makeText(App.stage, resource.getString(Constantes.INSERCION_AUTOR_CORRECTA), Constantes.TOAST_DELAY, Constantes.FADE_IN_DELAY0, Constantes.FADE_IN_DELAY0, COLOR_EXITO);

        } catch (XmlRpcException ex) {
            Toast.makeText(App.stage, resource.getString(Constantes.INSERCION_AUTOR_NO_CORRECTA), Constantes.TOAST_DELAY, Constantes.FADE_IN_DELAY0, Constantes.FADE_IN_DELAY0, COLOR_RED);
        } catch (Exception ex) {
            Toast.makeText(App.stage, resource.getString(Constantes.INSERCION_AUTOR_NO_CORRECTA), Constantes.TOAST_DELAY, Constantes.FADE_IN_DELAY0, Constantes.FADE_IN_DELAY0, COLOR_RED);
        }

        cambiar_a_cursor_normal();
        actualizar_datos_tabla();
        panel.getSelectionModel().select(tabmodificar);
        tabla_modificar_libros.scrollTo(tabla_modificar_libros.getItems().size() - 1);

        cambiar_a_cursor_normal();
    }

    @FXML
    public void anyadirlinea(ActionEvent event) {
        try {
            pedido = null;
            Parent parent = null;
            FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("dialogmodificarlineapedido.fxml"));
            parent = fxmlLoader.load();
            Scene scene = new Scene(parent);
            Stage stage = new Stage();
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setScene(scene);
            stage.showAndWait();
            tabla.getItems().add(DialogModificarLineaPedido.lineapedido);
        } catch (IOException ex) {
            Dialog.Mensaje_de_excepcion(ex);
        }
    }

    @Override
    public void modificar() {

    }

    @Override
    public void eliminar() {

    }

    @Override
    public void Exportar() {
        File archivo_a_guardar = App.guardar_fileChooser(resource.getString(Constantes.EXPORTAR_FILE_CHOOSER), Constantes.CSV);

        if (archivo_a_guardar != null) {

            try (BufferedWriter escribir = new BufferedWriter(new FileWriter(archivo_a_guardar))) {
                ObservableList<Pedido> libros_a_eliminar = tabla_modificar_libros.getSelectionModel().getSelectedItems();
                StringBuilder texto = new StringBuilder();
                for (Pedido libro_a_eliminar : libros_a_eliminar) {
                    texto.append(libro_a_eliminar.getUsuario().getId()).append(Constantes.SEPARADOR_EXPORTACION);
                    texto.append(libro_a_eliminar.getFecha_de_entrega()).append(Constantes.SEPARADOR_EXPORTACION);
                    texto.append(libro_a_eliminar.getFecha_de_realizacion()).append(Constantes.SEPARADOR_EXPORTACION);

                    texto.append(System.lineSeparator());
                }
                escribir.write(texto.toString());

            } catch (IOException ex) {
                Dialog.Mensaje_de_excepcion(ex);
            }
            Dialog.Dialogo_de_confirmacion(resource.getString(Constantes.CONFIRMACION_EXPORTACION), archivo_a_guardar);
        }

    }

    @Override
    public void Cargar_datos() {
        try {
            librosDAO = new PedidoDAO();
            lista_libros = FXCollections.observableArrayList();
            menu = new ContextMenu();
            Anular_exportar();
            actualizar_datos_tabla();
        } catch (Exception ex) {
            Dialog.Mensaje_de_excepcion(ex);
        }
    }

    @FXML
    public void BuscarPorUsuario() {
        try {
            Usuario usuario = comboBoxUsuario.getSelectionModel().getSelectedItem();
            if (usuario != null) {
                ObservableList<Pedido> objetosABuscar = tabla_modificar_libros.getItems();
                ObservableList<Pedido> objetosEncontrados = FXCollections.observableArrayList();
                for (Pedido pedido : objetosABuscar) {
                    if (pedido.getUsuario().getId() == usuario.getId()) {
                        objetosEncontrados.add(pedido);
                    }
                }
                tabla_modificar_libros.getItems().setAll(objetosEncontrados);
            }

        } catch (Exception ex) {
            Dialog.Mensaje_de_excepcion(ex);
        }
    }

    private boolean estaElLibro(ArrayList<Linea_pedido> lineas_pedido, Libro libro) {
        boolean estaElLibro = false;
        for (int contador = 0; contador < lineas_pedido.size(); contador++) {
            if (lineas_pedido.get(contador).getLibro().getId() == libro.getId()) {
                estaElLibro = true;
                contador = lineas_pedido.size();
            }
        }
        return estaElLibro;
    }

    @FXML
    public void BuscarPorLibros() {
        try {
            Libro libro = comboBoxArticulo.getSelectionModel().getSelectedItem();
            if (libro != null) {
                ObservableList<Pedido> objetosABuscar = tabla_modificar_libros.getItems();
                ObservableList<Pedido> objetosEncontrados = FXCollections.observableArrayList();
                for (Pedido pedido : objetosABuscar) {
                    if (estaElLibro(pedido.getLineas_pedido(), libro)) {
                        objetosEncontrados.add(pedido);
                    }

                }
                tabla_modificar_libros.getItems().setAll(objetosEncontrados);
            }

        } catch (Exception ex) {
            Dialog.Mensaje_de_excepcion(ex);
        }
    }

    private boolean dosFechasIguales(java.util.Date fecha_de_entrega, Date fechaDeEntrega) {
        return fecha_de_entrega.getDate() == fechaDeEntrega.getDate()
                && fecha_de_entrega.getMonth() == fechaDeEntrega.getMonth()
                && fecha_de_entrega.getYear() == fechaDeEntrega.getYear();
    }

    @FXML
    public void BuscarFechaDeEntrega() {
        try {
            Date fechaDeEntrega = Date.valueOf(datePickerFechaDeEntrega.getValue());
            if (fechaDeEntrega != null) {
                ObservableList<Pedido> objetosABuscar = tabla_modificar_libros.getItems();
                ObservableList<Pedido> objetosEncontrados = FXCollections.observableArrayList();
                for (Pedido pedido : objetosABuscar) {
                    System.out.println(pedido);
                    if (dosFechasIguales(pedido.getFecha_de_realizacion(), fechaDeEntrega)) {
                        System.out.println(pedido.getId());
                        objetosEncontrados.add(pedido);
                    }

                }
                tabla_modificar_libros.getItems().setAll(objetosEncontrados);
            }

        } catch (Exception ex) {
            //Dialog.Mensaje_de_excepcion(ex);
        }
    }

    @FXML
    public void BuscarFechaDeRealizacion() {
        try {
            Date fechaDeRealizacion = Date.valueOf(datePickerFechaRealizacion.getValue());
            if (fechaDeRealizacion != null) {
                ObservableList<Pedido> objetosABuscar = tabla_modificar_libros.getItems();
                ObservableList<Pedido> objetosEncontrados = FXCollections.observableArrayList();
                for (Pedido pedido : objetosABuscar) {
                    if (dosFechasIguales(pedido.getFecha_de_entrega(), fechaDeRealizacion)) {
                        objetosEncontrados.add(pedido);
                    }

                }
                tabla_modificar_libros.getItems().setAll(objetosEncontrados);
            }
        } catch (Exception ex) {
            //Dialog.Mensaje_de_excepcion(ex);
        }
    }

    @FXML
    public void LimpiarBusqueda() {
        try {
            tabla_modificar_libros.getItems().setAll(pedidoDBDAO.buscarTodos());
            comboBoxUsuario.getSelectionModel().clearSelection();
            comboBoxArticulo.getSelectionModel().clearSelection();

            datePickerFechaDeEntrega.setPromptText("");
            datePickerFechaRealizacion.setPromptText("");
        } catch (XmlRpcException ex) {
            Dialog.Mensaje_de_excepcion(ex);
        } catch (Exception ex) {
            Dialog.Mensaje_de_excepcion(ex);
        }
    }

    public void actualizar_datos_tabla() {

        try {
            lista_libros.setAll(librosDAO.buscarTodos());
            tabla_modificar_libros.setItems(lista_libros);

            Titulo.setCellValueFactory(o -> new SimpleStringProperty(o.getValue().getUsuario().toString()));
            ISBN.setCellValueFactory(o -> new SimpleStringProperty(String.valueOf(o.getValue().getFecha_de_entrega())));
            estado_del_libro.setCellValueFactory(o -> new SimpleStringProperty(o.getValue().getFecha_de_realizacion().toString()));
            //editorial = addButtonToTable("pagina_web");
            //localizacion = addButtonToTable("facebook");
            //tipo_del_libro = addButtonToTable("twitter");
            //autor = addButtonToTable("instagram");
            Titulo.setSortable(true);
            ISBN.setSortable(true);
            //estado_del_libro.setSortable(true);

            //editorial.setSortable(true);
            //localizacion.setSortable(true);
            //tipo_del_libro.setSortable(true);
            //autor.setSortable(true);
            comboBoxUsuario.getItems().setAll(usuarioDBDAO.buscarTodos());
            comboBoxArticulo.getItems().setAll(libroDAO.buscarTodos());

            MenuItem menuItem1 = new MenuItem(resource.getString(Constantes.ELIMINAR));
            MenuItem menuItem2 = new MenuItem(resource.getString(Constantes.ELIMINAR_TODOS_LOS_SELECCIONADOS));
            MenuItem menuItem3 = new MenuItem(resource.getString("exportarPDF"));

            menuItem1.setOnAction((event) -> {
                try {

                    if (Dialog.Dialogo_de_confirmacion(resource.getString(Constantes.PREGUNTA_ELIMINAR))) {
                        librosDAO.eliminarRegistro(tabla_modificar_libros.getSelectionModel().getSelectedItem());
                        actualizar_datos_tabla();
                    }

                } catch (XmlRpcException ex) {
                    Dialog.Mensaje_de_excepcion(ex);
                } catch (Exception ex) {
                    Dialog.Mensaje_de_excepcion(ex);
                }
            });

            menuItem2.setOnAction((event) -> {
                try {

                    if (Dialog.Dialogo_de_confirmacion(resource.getString(Constantes.PREGUNTA_ELIMINAR))) {
                        ObservableList<Pedido> libros_a_eliminar = tabla_modificar_libros.getSelectionModel().getSelectedItems();
                        for (Pedido libro_a_eliminar : libros_a_eliminar) {
                            librosDAO.eliminarRegistro(libro_a_eliminar);
                        }
                        actualizar_datos_tabla();
                    }

                } catch (XmlRpcException ex) {
                    Dialog.Mensaje_de_excepcion(ex);
                } catch (Exception ex) {
                    Dialog.Mensaje_de_excepcion(ex);
                }
            });

            menuItem3.setOnAction((event) -> {
                try {

                    if (Dialog.Dialogo_de_confirmacion(resource.getString(Constantes.PREGUNTA_ELIMINAR))) {
                        ObservableList<Pedido> libros_a_eliminar = tabla_modificar_libros.getSelectionModel().getSelectedItems();
                        for (Pedido pedidoAExportar : libros_a_eliminar) {
                            Exportar_factura_a_PDF(pedidoAExportar);
                        }
                    }

                } catch (Exception ex) {
                    Dialog.Mensaje_de_excepcion(ex);
                }
            });

            menu.getItems().setAll(menuItem1, menuItem2, menuItem3);
            tabla_modificar_libros.setContextMenu(menu);
            tabla_modificar_libros.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        } catch (XmlRpcException ex) {
            Dialog.Mensaje_de_excepcion(ex);
        } catch (Exception ex) {
            Dialog.Mensaje_de_excepcion(ex);
        }
    }

    @Override
    public void actualizar() {
        actualizar_datos_tabla();
    }

    public void pantalla_modificar_libro(MouseEvent mouse) {

        if (Dialog.es_doble_click(mouse) && Dialog.es_boton_izquierdo(mouse)) {

            try {
                libro = tabla_modificar_libros.getSelectionModel().getSelectedItem();
                System.out.println(libro.getId());
                dialogo();
            } catch (IOException ex) {
                Dialog.Mensaje_de_excepcion(ex);
            } catch (Exception ex) {
                Dialog.Mensaje_de_excepcion(ex);
            }
        }
    }

    public void dialogo() throws IOException, Exception {
        if (libro != null) {
            Parent parent = null;
            FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("dialogmodificarpedido.fxml"), resource);
            parent = fxmlLoader.load();
            Scene scene = new Scene(parent);
            Stage stage = new Stage();
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setScene(scene);
            stage.showAndWait();
            actualizar_datos_tabla();
            cambiar_a_cursor_normal();
        }

    }

    public void Limpiar_busqueda() {
        textfield_buscar_2.setText(Constantes.VACIO);

        try {
            tabla_modificar_libros.getItems().setAll(librosDAO.buscarTodos());
        } catch (XmlRpcException ex) {
            Dialog.Mensaje_de_excepcion(ex);
        } catch (Exception ex) {
            Dialog.Mensaje_de_excepcion(ex);
        }

    }

    public void Anular_exportar() {
        try {
            exportar.setDisable(!tabmodificar.isSelected());
        } catch (NullPointerException n) {

        }
    }

    public void Exportar_factura_a_PDF(Pedido pedido) {

        DirectoryChooser directoryChooser = new DirectoryChooser();
        directoryChooser.setInitialDirectory(CARPETA_PERSONAL);
        File file = directoryChooser.showDialog(App.stage);
        pedido.calcular_total();

        if (file != null) {

            file = new File(file.getAbsolutePath() + File.separator + pedido.getId() + ".pdf");

            // Se crea el documento
            Document documento = new Document();

            // Se crea el OutputStream para el fichero donde queremos dejar el pdf.
            FileOutputStream ficheroPdf;
            try {
                ficheroPdf = new FileOutputStream(file);
                // Se asocia el documento al OutputStream y se indica que el espaciado entre
                // lineas sera de 20. Esta llamada debe hacerse antes de abrir el documento
                PdfWriter.getInstance(documento, ficheroPdf).setInitialLeading(20);

                // Se abre el documento.
                documento.open();

                documento.add(new Paragraph("Pedido de libro", FontFactory.getFont("arial", // fuente
                        22, // tamaño
                        Font.ITALIC, // estilo
                        BaseColor.CYAN))); // color

                documento.add(new Paragraph("Fecha de realizacion  " + pedido.getFecha_de_realizacion().toString() + System.lineSeparator()));
                documento.add(new Paragraph("Fecha de entrega  " + pedido.getFecha_de_entrega().toString() + System.lineSeparator()));
                documento.add(new Paragraph("Total pedido " + pedido.getTotal_pedido() + "€" + System.lineSeparator()));
                PdfPTable tabla = new PdfPTable(4);

                tabla.addCell("Libro");
                tabla.addCell("Cantidad");
                tabla.addCell("Precio");
                tabla.addCell("Total linea");

                for (Linea_pedido lineaPedido : pedido.getLineas_pedido()) {
                    tabla.addCell(lineaPedido.getLibro().toString());
                    tabla.addCell(String.valueOf(lineaPedido.getCantidad()));
                    tabla.addCell(String.valueOf(lineaPedido.getPrecio()));
                    tabla.addCell(String.valueOf(lineaPedido.getTotal_linea()));

                }

                documento.add(tabla);

            } catch (FileNotFoundException e) {

            } catch (DocumentException e) {

            }

            documento.close();
        }
    }

}
