/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ControladorVista;

import static ControladorVista.ControladorGlobal.COLOR_EXITO;
import static ControladorVista.ControladorGlobal.COLOR_RED;
import static ControladorVista.ControladorGlobal.*;
import DAO.Linea_pedidoDAO;
import DAO.LibroDAO;
import Modelo.Linea_pedido;
import Modelo.Libro;
import Utilidades.Constantes;
import Utilidades.Dialog;
import Utilidades.Toast;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import com.mycompany.proyectobiblioteca.App;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import org.apache.xmlrpc.XmlRpcException;

/**
 *
 * @author batoi
 */
public class DialogModificarLineaPedido implements Initializable {
    
    private ResourceBundle resource;

    @FXML
    private GridPane gridpane;
    public static Linea_pedido lineapedido;
    @FXML
    private Label label_nombre;
    @FXML
    private Label label_telefono;
    @FXML
    private Label label_fecha;
    @FXML
    private Label label_pagina_web;
    @FXML
    private Label label_facebook;
    @FXML
    private Label label_twitter;
    @FXML
    private Label label_instagram;

    @FXML
    private JFXTextField texto_telefono;
    @FXML
    private JFXTextField texto_pagina_web;
    @FXML
    private JFXComboBox<Libro> texto_nombre;

    @Override
    public void initialize(URL arg0, ResourceBundle pResource) {
        resource = pResource;
        Linea_pedido autor_seleccionado = DialogModificarPedido.pedido == null ? new Linea_pedido() : DialogModificarPedido.pedido;
        try {
            texto_nombre.getItems().setAll(libroDAO.buscarTodos());
            texto_nombre.getSelectionModel().select(autor_seleccionado.getLibro());
            texto_pagina_web.setText(String.valueOf(autor_seleccionado.getCantidad()));
            texto_telefono.setText(String.valueOf(autor_seleccionado.getPrecio()));

        } catch (XmlRpcException ex) {
            Dialog.Mensaje_de_excepcion(ex);
        } catch (Exception ex) {
            Dialog.Mensaje_de_excepcion(ex);
        }

    }

    protected void cambiar_a_cursor_esperando() {
        App.stage.getScene().setCursor(Cursor.WAIT);
    }

    @FXML
    public void actualizar(ActionEvent event) {
        cambiar_a_cursor_esperando();
        Linea_pedido autor_actualizacion = DialogModificarPedido.pedido;
        int cantidad = 0;

        try {
            cantidad = Integer.parseInt(texto_pagina_web.getText());
        } catch (Exception e) {
            cantidad = 0;
        }

        double precio = 0.0;

        try {
            precio = Double.parseDouble(texto_telefono.getText());
        } catch (Exception e) {
            precio = 0.0;
        }

        try {

            if (DialogModificarPedido.pedido != null) {
                autor_actualizacion.setLibro(texto_nombre.getSelectionModel().getSelectedItem());
                autor_actualizacion.setCantidad(cantidad);
                autor_actualizacion.setPrecio(precio);
                
                
                autor_actualizacion.setPedido(ControladorPedido.libro);
                Linea_pedidoDBDAO.actualizarRegistro(autor_actualizacion);
                Toast.makeText(App.stage, resource.getString(Constantes.ACTUALIZACION_AUTOR_CORRECTA), Constantes.TOAST_DELAY, Constantes.FADE_IN_DELAY0, Constantes.FADE_IN_DELAY0, COLOR_EXITO);
                lineapedido = autor_actualizacion;
            } else {
                autor_actualizacion = new Linea_pedido();
                autor_actualizacion.setLibro(texto_nombre.getSelectionModel().getSelectedItem());
                autor_actualizacion.setCantidad(cantidad);
                autor_actualizacion.setPrecio(precio);
                
                autor_actualizacion.setPedido(ControladorPedido.libro);
                int id = Linea_pedidoDBDAO.insertarRegistro(autor_actualizacion);
                lineapedido = autor_actualizacion;
                lineapedido.setId(id);
                System.out.println("Linea " + autor_actualizacion.getId() + " pedido " + ControladorPedido.libro.getId());
                
                Toast.makeText(App.stage, resource.getString(Constantes.INSERCION_AUTOR_CORRECTA), Constantes.TOAST_DELAY, Constantes.FADE_IN_DELAY0, Constantes.FADE_IN_DELAY0, COLOR_EXITO);
            }

        } catch (XmlRpcException ex) {
            ex.printStackTrace();
            Toast.makeText(App.stage, resource.getString(Constantes.INSERCION_AUTOR_NO_CORRECTA), Constantes.TOAST_DELAY, Constantes.FADE_IN_DELAY0, Constantes.FADE_IN_DELAY0, COLOR_RED);
        } catch (Exception ex) {
            ex.printStackTrace();
            Toast.makeText(App.stage, resource.getString(Constantes.INSERCION_AUTOR_NO_CORRECTA), Constantes.TOAST_DELAY, Constantes.FADE_IN_DELAY0, Constantes.FADE_IN_DELAY0, COLOR_RED);
        }

        cambiar_a_cursor_normal();
        closeStage(event);
    }

    protected void cambiar_a_cursor_normal() {
        App.stage.getScene().setCursor(Cursor.DEFAULT);
    }

    public void closeStage(ActionEvent event) {
        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        stage.close();
    }

}
