/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ControladorVista;

import static ControladorVista.ControladorGlobal.COLOR_EXITO;
import static ControladorVista.ControladorPrestamo.libro;
import DAO.AutorDAO;
import DAO.EditorialDAO;
import DAO.LibroDAO;
import DAO.LocalizacionDAO;
import DAO.PrestamoDAO;
import DAO.TipoDAO;
import Modelo.Autor;
import Modelo.Editorial;
import Modelo.Libro;
import Modelo.Localizacion;
import Modelo.Prestamo;
import Modelo.Tipo;
import Utilidades.Constantes;
import Utilidades.Dialog;
import Utilidades.Toast;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.controls.JFXToggleButton;
import com.mycompany.proyectobiblioteca.App;
import static com.mycompany.proyectobiblioteca.App.crear_threads;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.apache.xmlrpc.XmlRpcException;

/**
 *
 * @author batoi
 */
public class ControladorLibros extends ControladorGlobal<Libro> {

    private LibroDAO librosDAO;
    private ObservableList<Libro> lista_libros;

    @FXML
    private Label label_titulo;
    @FXML
    private JFXCheckBox buscarNoDevueltos;
    @FXML
    private Label label_isbn;
    @FXML
    private Label elegir_editorial;
    @FXML
    private Label prestado;
    @FXML
    private JFXToggleButton button_prestado;
    @FXML
    private Label elegir_autor;
    @FXML
    private Label elegir_tipo;
    @FXML
    private Label elegir_localizaicion;
    @FXML
    private Label elegir_estado;
    @FXML
    private GridPane gridpane;
    @FXML
    private GridPane opciones_modificar;
    @FXML
    private HBox opciones_buscar;
    @FXML
    private HBox opciones_buscar_2;
    @FXML
    private HBox opciones_buscar_3;
    @FXML
    private VBox fondo;
    @FXML
    private JFXTextField textfield_buscar_2;
    @FXML
    private JFXTextField textfield_cantidad;
    @FXML
    private JFXTextField textfield_paginas;
    @FXML
    private Tab tabinsertar;
    @FXML
    private Tab tabmodificar;

    @FXML
    private Label label_contenido;
    @FXML
    private Label label_autor;
    @FXML
    private Label label_editorial;
    @FXML
    private Label label_tipo;
    @FXML
    private Label label_estado;
    @FXML
    private Label label_localizacion;
    @FXML
    private Label label_cantidad;
    @FXML
    private Label label_paginas;

    @FXML
    private JFXComboBox<String> combobox_estado;
    @FXML
    private JFXComboBox<Editorial> combobox_editorial;
    @FXML
    private JFXComboBox<Localizacion> combobox_localizacion;
    @FXML
    private JFXComboBox<Tipo> combobox_tipo;
    @FXML
    private JFXComboBox<Autor> combobox_autor;
    @FXML
    private JFXComboBox<String> combobox_tipos_atributo;
    @FXML
    private JFXToggleButton combobox_prestado;

    @FXML
    private JFXButton button_limpiar;
    @FXML
    private JFXButton cancelar;
    @FXML
    private JFXButton actualizar;
    @FXML
    private Label cantidad;
    @FXML
    private JFXTextField texto_cantidad;
    @FXML
    private JFXTextField texto_titulo;
    @FXML
    private JFXTextField texto_isbn;
    @FXML
    private Label paginas;
    @FXML
    private JFXTextField texto_paginas;
    @FXML
    private JFXComboBox<String> texto_estado;
    @FXML
    private JFXComboBox<Editorial> texto_editorial;
    @FXML
    private JFXComboBox<Localizacion> texto_localizacion;
    @FXML
    private JFXComboBox<Tipo> texto_tipo;
    @FXML
    private JFXComboBox<Autor> texto_autor;

    @FXML
    private TableColumn<Libro, String> Titulo;
    @FXML
    private TableColumn<Libro, String> ISBN;
    @FXML
    private TableColumn<Libro, String> estado_del_libro;
    @FXML
    private TableColumn<Libro, String> cantidad_scenebuilder;
    @FXML
    private TableColumn<Libro, String> paginas_scenebuilder;
    @FXML
    private TableColumn<Libro, String> editorial;
    @FXML
    private TableColumn<Libro, String> localizacion;
    @FXML
    private TableColumn<Libro, String> tipo_del_libro;
    @FXML
    private TableColumn<Libro, String> autor;

    @FXML
    private TableView<Libro> tabla_modificar_libros;
    public static Libro libro;
    private ContextMenu menu;

    @Override
    public void insertar() {
        cambiar_a_cursor_esperando();
        Libro libro_actualizacion = new Libro();

        libro_actualizacion.setTitulo(texto_titulo.getText());
        libro_actualizacion.setISBN(texto_isbn.getText());
        libro_actualizacion.setEditorial(texto_editorial.getSelectionModel().getSelectedItem());
        libro_actualizacion.setEsta_prestado(button_prestado.selectedProperty().getValue());
        libro_actualizacion.setEstado_del_libro(String.valueOf(texto_estado.getSelectionModel().getSelectedIndex()));
        libro_actualizacion.setLocalizacion(texto_localizacion.getSelectionModel().getSelectedItem());
        int paginas = 0;
        try {
            paginas = Integer.parseInt(texto_paginas.getText());
        } catch (Exception e) {
            paginas = 0;
        }
        int cantidad = 0;
        try {
            cantidad = Integer.parseInt(texto_cantidad.getText());
        } catch (Exception e) {
            cantidad = 0;
        }
        libro_actualizacion.setPaginas(paginas);
        libro_actualizacion.setCantidad(cantidad);
        libro_actualizacion.setTipo_del_libro(texto_tipo.getSelectionModel().getSelectedItem());
        libro_actualizacion.setAutor(texto_autor.getSelectionModel().getSelectedItem());

        try {
            libroDAO.insertarRegistro(libro_actualizacion);
            Toast.makeText(App.stage, resource.getString(Constantes.ACTUALIZACION_AUTOR_CORRECTA), Constantes.TOAST_DELAY, Constantes.FADE_IN_DELAY0, Constantes.FADE_IN_DELAY0, COLOR_EXITO);
            crear_threads();
        } catch (XmlRpcException ex) {
            Toast.makeText(App.stage, resource.getString(Constantes.INSERCION_AUTOR_NO_CORRECTA), Constantes.TOAST_DELAY, Constantes.FADE_IN_DELAY0, Constantes.FADE_IN_DELAY0, COLOR_RED);
        } catch (Exception ex) {
            Toast.makeText(App.stage, resource.getString(Constantes.INSERCION_AUTOR_NO_CORRECTA), Constantes.TOAST_DELAY, Constantes.FADE_IN_DELAY0, Constantes.FADE_IN_DELAY0, COLOR_RED);
        }

        actualizar_datos_tabla();
        panel.getSelectionModel().select(tabmodificar);
        tabla_modificar_libros.scrollTo(tabla_modificar_libros.getItems().size() - 1);
        tabla_modificar_libros.getSelectionModel().select(tabla_modificar_libros.getItems().size() - 1);
        cambiar_a_cursor_normal();
    }

    @Override
    public void modificar() {

    }

    @Override
    public void eliminar() {

    }

    @FXML
    public void buscarLibrosNoDevueltos() {
        Thread thread = new Thread(() -> {
            try {
                buscarNoDevueltos.setDisable(true);
                tabla_modificar_libros.getItems().clear();
                if (buscarNoDevueltos.isSelected()) {
                    tabla_modificar_libros.getItems().setAll(prestamoDBDAO.librosNoHayPrestamo());
                } else {
                    tabla_modificar_libros.getItems().addAll(librosDAO.buscarTodos());
                }
                buscarNoDevueltos.setDisable(false);
            } catch (Exception ex) {
                Dialog.Mensaje_de_excepcion(ex);
            }
        });
        thread.start();
    }

    @Override
    public void Exportar() {
        File archivo_a_guardar = App.guardar_fileChooser(resource.getString(Constantes.EXPORTAR_FILE_CHOOSER), Constantes.CSV);

        if (archivo_a_guardar != null) {

            try ( BufferedWriter escribir = new BufferedWriter(new FileWriter(archivo_a_guardar))) {
                ObservableList<Libro> libros_a_eliminar = tabla_modificar_libros.getSelectionModel().getSelectedItems();
                StringBuilder texto = new StringBuilder();
                for (Libro libro_a_eliminar : libros_a_eliminar) {
                    texto.append(libro_a_eliminar.getTitulo()).append(Constantes.SEPARADOR_EXPORTACION);
                    texto.append(libro_a_eliminar.getAutor().getId()).append(Constantes.SEPARADOR_EXPORTACION);
                    texto.append(libro_a_eliminar.getCantidad()).append(Constantes.SEPARADOR_EXPORTACION);
                    texto.append(libro_a_eliminar.getEditorial().getId()).append(Constantes.SEPARADOR_EXPORTACION);
                    texto.append(libro_a_eliminar.getEstado_del_libro()).append(Constantes.SEPARADOR_EXPORTACION);
                    texto.append(libro_a_eliminar.getISBN()).append(Constantes.SEPARADOR_EXPORTACION);
                    texto.append(libro_a_eliminar.getLocalizacion().getId()).append(Constantes.SEPARADOR_EXPORTACION);
                    texto.append(libro_a_eliminar.getTipo_del_libro().getId()).append(Constantes.SEPARADOR_EXPORTACION);
                    texto.append(System.lineSeparator());
                }
                escribir.write(texto.toString());

            } catch (IOException ex) {
                Dialog.Mensaje_de_excepcion(ex);
            }
            Dialog.Dialogo_de_confirmacion(resource.getString(Constantes.CONFIRMACION_EXPORTACION), archivo_a_guardar);
        }

    }

    @Override
    public void Cargar_datos() {
        try {
            librosDAO = new LibroDAO();
            lista_libros = FXCollections.observableArrayList();
            menu = new ContextMenu();
            Anular_exportar();
            actualizar_datos_tabla();
        } catch (XmlRpcException ex) {
            Dialog.Mensaje_de_excepcion(ex);
        } catch (Exception ex) {
            Dialog.Mensaje_de_excepcion(ex);
        }
    }

    public void actualizar_datos_tabla() {

        try {

            String[] estados_posibles = {
                resource.getString(Constantes.BUEN_ESTADO),
                resource.getString(Constantes.ESTROPEADO),
                resource.getString(Constantes.INUTILIZABLE),
                resource.getString(Constantes.PAGINAS_ROTAS)

            };

            lista_libros.setAll(librosDAO.buscarTodos());
            tabla_modificar_libros.setItems(lista_libros);
            Titulo.setCellValueFactory(o -> new SimpleStringProperty(o.getValue().getTitulo()));
            ISBN.setCellValueFactory(o -> new SimpleStringProperty(o.getValue().getISBN()));
            estado_del_libro.setCellValueFactory(o -> new SimpleStringProperty(estados_posibles[Integer.parseInt(o.getValue().getEstado_del_libro())]));
            cantidad_scenebuilder.setCellValueFactory(o -> new SimpleStringProperty(String.valueOf(o.getValue().getCantidad())));

            paginas_scenebuilder.setCellValueFactory(o -> new SimpleStringProperty(String.valueOf(o.getValue().getPaginas())));
            editorial.setCellValueFactory(o -> new SimpleStringProperty(o.getValue().getEditorial().toString()));
            localizacion.setCellValueFactory(o -> new SimpleStringProperty(o.getValue().getLocalizacion().toString()));
            tipo_del_libro.setCellValueFactory(o -> new SimpleStringProperty(o.getValue().getTipo_del_libro().toString()));
            autor.setCellValueFactory(o -> new SimpleStringProperty(o.getValue().getAutor().getNombre()));

            Titulo.setSortable(true);
            ISBN.setSortable(true);
            estado_del_libro.setSortable(true);
            cantidad_scenebuilder.setSortable(true);
            paginas_scenebuilder.setSortable(true);
            editorial.setSortable(true);
            localizacion.setSortable(true);
            tipo_del_libro.setSortable(true);
            autor.setSortable(true);

            texto_editorial.getItems().setAll(editorialDAO.buscarTodos());
            texto_localizacion.getItems().setAll(localizacionDAO.buscarTodos());
            texto_tipo.getItems().setAll(tipoDBDAO.buscarTodos());
            texto_autor.getItems().setAll(autorDAO.buscarTodos());
            texto_estado.getItems().clear();
            for (String estado_a_poner : estados_posibles) {
                texto_estado.getItems().add(estado_a_poner);
            }

            combobox_editorial.getItems().setAll(editorialDAO.buscarTodos());
            combobox_localizacion.getItems().setAll(localizacionDAO.buscarTodos());
            combobox_tipo.getItems().setAll(tipoDBDAO.buscarTodos());
            combobox_autor.getItems().setAll(autorDAO.buscarTodos());
            combobox_estado.getItems().clear();
            for (String estado_a_poner : estados_posibles) {
                combobox_estado.getItems().add(estado_a_poner);
            }

            combobox_tipos_atributo.getItems().setAll(
                    Constantes.TITULO_LIBRO, Constantes.ISBN
            );

            MenuItem menuItem1 = new MenuItem(resource.getString(Constantes.ELIMINAR));
            MenuItem menuItem2 = new MenuItem(resource.getString(Constantes.ELIMINAR_TODOS_LOS_SELECCIONADOS));

            menuItem1.setOnAction((event) -> {
                try {

                    if (Dialog.Dialogo_de_confirmacion(resource.getString(Constantes.PREGUNTA_ELIMINAR))) {
                        librosDAO.eliminarRegistro(tabla_modificar_libros.getSelectionModel().getSelectedItem());
                        actualizar_datos_tabla();
                    }
                    crear_threads();
                } catch (XmlRpcException ex) {
                    Dialog.Mensaje_de_excepcion(ex);
                } catch (Exception ex) {
                    Dialog.Mensaje_de_excepcion(ex);
                }
            });

            menuItem2.setOnAction((event) -> {
                try {

                    if (Dialog.Dialogo_de_confirmacion(resource.getString(Constantes.PREGUNTA_ELIMINAR))) {
                        ObservableList<Libro> libros_a_eliminar = tabla_modificar_libros.getSelectionModel().getSelectedItems();
                        for (Libro libro_a_eliminar : libros_a_eliminar) {
                            librosDAO.eliminarRegistro(libro_a_eliminar);
                        }
                        crear_threads();
                        actualizar_datos_tabla();
                    }

                } catch (XmlRpcException ex) {
                    Dialog.Mensaje_de_excepcion(ex);
                } catch (Exception ex) {
                    Dialog.Mensaje_de_excepcion(ex);
                }
            });

            MenuItem menuItem3 = new MenuItem("Prestar libro");
            MenuItem menuItem4 = new MenuItem("Devolver libro");
            menuItem3.setOnAction((event) -> {
                try {
                    if (prestamoDBDAO.noHayPrestamo(libro)) {
                        libro = tabla_modificar_libros.getSelectionModel().getSelectedItem();
                        ControladorPrestamo.libro = new Prestamo();
                        ControladorPrestamo.libro.setLibro_prestado(libro);
                        dialogoPrestamo();

                    } else {
                        throw new Exception("El libro esta prestado");
                    }

                } catch (IOException ex) {
                    Dialog.Mensaje_de_excepcion(ex);
                } catch (Exception ex) {
                    Dialog.Mensaje_de_excepcion(ex);
                }

            });
            menuItem4.setOnAction((event) -> {
                try {
                    if (!prestamoDBDAO.noHayPrestamo(libro)) {

                        libro = tabla_modificar_libros.getSelectionModel().getSelectedItem();
                        ArrayList<Prestamo> prestamos = prestamoDBDAO.buscarPorLibro(libro);

                        if (!prestamos.isEmpty()) {
                            Prestamo prestamo = prestamos.get(prestamos.size() - 1);
                            Date fechaActual = new Date();
                            if (prestamoDBDAO.despuesDeDia(prestamo.getFecha_que_se_ha_devuelto(), fechaActual)) {
                                prestamo.setFecha_que_se_ha_devuelto(PrestamoDAO.variarDiasAFecha(prestamo.getFecha_que_se_ha_devuelto(), -1));
                            }
                            if (prestamoDBDAO.despuesDeDia(prestamo.getFecha_que_se_tiene_que_devolver(), fechaActual)) {
                                prestamo.setFecha_que_se_tiene_que_devolver(PrestamoDAO.variarDiasAFecha(prestamo.getFecha_que_se_tiene_que_devolver(), -1));
                            }
                            prestamoDBDAO.actualizarRegistro(prestamo);
                        } else {
                            throw new Exception("El libro no esta prestado");
                        }

                    } else {
                        throw new Exception("El libro no esta prestado");
                    }

                } catch (Exception ex) {
                    Dialog.Mensaje_de_excepcion(ex);
                }
            });
            menu.getItems().setAll(menuItem1, menuItem2, menuItem3, menuItem4);
            tabla_modificar_libros.setContextMenu(menu);
            tabla_modificar_libros.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
            texto_titulo.setText(Constantes.VACIO);
            texto_isbn.setText(Constantes.VACIO);
            texto_editorial.getSelectionModel().clearSelection();
            texto_estado.getSelectionModel().clearSelection();
            texto_localizacion.getSelectionModel().clearSelection();
            texto_autor.getSelectionModel().clearSelection();
            texto_tipo.getSelectionModel().clearSelection();
            button_prestado.selectedProperty().setValue(Constantes.BOOLEAN_POR_DEFECTO);
            texto_cantidad.setText(Constantes.VACIO);
            texto_paginas.setText(Constantes.VACIO);

        } catch (XmlRpcException ex) {
            Dialog.Mensaje_de_excepcion(ex);
        } catch (Exception ex) {
            Dialog.Mensaje_de_excepcion(ex);
        }
    }

    @Override
    public void actualizar() {
        actualizar_datos_tabla();
    }

    public void pantalla_modificar_libro(MouseEvent mouse) {

        if (Dialog.es_doble_click(mouse) && Dialog.es_boton_izquierdo(mouse)) {

            try {
                dialogo();
            } catch (IOException ex) {
                Dialog.Mensaje_de_excepcion(ex);
            } catch (Exception ex) {
                Dialog.Mensaje_de_excepcion(ex);
            }
        }
    }

    public void dialogo() throws IOException, Exception {
        Parent parent = null;
        libro = tabla_modificar_libros.getSelectionModel().getSelectedItem();

        if (libro != null) {
            FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("dialogmodificarlibro.fxml"), resource);
            parent = fxmlLoader.load();
            Scene scene = new Scene(parent);
            Stage stage = new Stage();
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setScene(scene);
            stage.showAndWait();
            actualizar_datos_tabla();
            cambiar_a_cursor_normal();
        }
    }

    public void dialogoPrestamo() throws IOException, Exception {
        Parent parent = null;
        libro = tabla_modificar_libros.getSelectionModel().getSelectedItem();
        if (libro != null) {
            FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("dialogmodificarprestamo.fxml"), resource);
            parent = fxmlLoader.load();
            Scene scene = new Scene(parent);
            Stage stage = new Stage();
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setScene(scene);
            stage.showAndWait();
            actualizar_datos_tabla();
            cambiar_a_cursor_normal();
        }
    }

    public void Limpiar_busqueda() {
        combobox_estado.getSelectionModel().clearSelection();
        combobox_editorial.getSelectionModel().clearSelection();
        combobox_localizacion.getSelectionModel().clearSelection();
        combobox_tipo.getSelectionModel().clearSelection();
        combobox_autor.getSelectionModel().clearSelection();
        combobox_tipos_atributo.getSelectionModel().clearSelection();
        combobox_prestado.selectedProperty().setValue(Constantes.BOOLEAN_POR_DEFECTO);
        textfield_buscar_2.setText(Constantes.VACIO);
        textfield_cantidad.setText(Constantes.VACIO);
        textfield_paginas.setText(Constantes.VACIO);

        try {
            tabla_modificar_libros.getItems().setAll(librosDAO.buscarTodos());
        } catch (XmlRpcException ex) {
            Dialog.Mensaje_de_excepcion(ex);
        } catch (Exception ex) {
            Dialog.Mensaje_de_excepcion(ex);
        }

    }

    public void Buscar_autor() {
        Autor autor = combobox_autor.getSelectionModel().getSelectedItem();
        ObservableList<Libro> libros_a_buscar = tabla_modificar_libros.getItems();
        if (autor != null) {
            ObservableList<Libro> libros_encontrados = FXCollections.observableArrayList();

            for (Libro libro_a_buscar : libros_a_buscar) {
                if (libro_a_buscar.getAutor().equals(autor)) {
                    libros_encontrados.add(libro_a_buscar);
                }
            }
            tabla_modificar_libros.getItems().setAll(libros_encontrados);
        } else {
            tabla_modificar_libros.getItems().setAll(libros_a_buscar);
        }
    }

    public void Buscar_editorial() {
        Editorial editorial = combobox_editorial.getSelectionModel().getSelectedItem();
        ObservableList<Libro> libros_a_buscar = tabla_modificar_libros.getItems();
        if (editorial != null) {
            ObservableList<Libro> libros_encontrados = FXCollections.observableArrayList();

            for (Libro libro_a_buscar : libros_a_buscar) {
                if (libro_a_buscar.getEditorial().equals(editorial)) {
                    libros_encontrados.add(libro_a_buscar);
                }
            }
            tabla_modificar_libros.getItems().setAll(libros_encontrados);
        } else {
            tabla_modificar_libros.getItems().setAll(libros_a_buscar);
        }
    }

    public void Buscar_tipo() {
        Tipo tipo_de_libro = combobox_tipo.getSelectionModel().getSelectedItem();
        ObservableList<Libro> libros_a_buscar = tabla_modificar_libros.getItems();
        if (tipo_de_libro != null) {
            ObservableList<Libro> libros_encontrados = FXCollections.observableArrayList();

            for (Libro libro_a_buscar : libros_a_buscar) {
                if (libro_a_buscar.getTipo_del_libro().equals(tipo_de_libro)) {
                    libros_encontrados.add(libro_a_buscar);
                }
            }
            tabla_modificar_libros.getItems().setAll(libros_encontrados);
        } else {
            tabla_modificar_libros.getItems().setAll(libros_a_buscar);
        }
    }

    public void Buscar_localizacion() {
        Localizacion localizacion = combobox_localizacion.getSelectionModel().getSelectedItem();
        ObservableList<Libro> libros_a_buscar = tabla_modificar_libros.getItems();
        if (localizacion != null) {
            ObservableList<Libro> libros_encontrados = FXCollections.observableArrayList();

            for (Libro libro_a_buscar : libros_a_buscar) {
                if (libro_a_buscar.getLocalizacion().equals(localizacion)) {
                    libros_encontrados.add(libro_a_buscar);
                }
            }
            tabla_modificar_libros.getItems().setAll(libros_encontrados);
        } else {
            tabla_modificar_libros.getItems().setAll(libros_a_buscar);
        }
    }

    public void Buscar_cantidad() {
        String texto = textfield_cantidad.getText().toLowerCase();
        ObservableList<Libro> libros_a_buscar = tabla_modificar_libros.getItems();
        if (texto.length() > 0) {
            if (es_un_numero(texto)) {
                textfield_cantidad.focusColorProperty().setValue(COLOR_EXITO);
                int numero = Integer.parseInt(texto);
                ObservableList<Libro> libros_encontrados = FXCollections.observableArrayList();

                for (Libro libro_a_buscar : libros_a_buscar) {
                    if (libro_a_buscar.getCantidad() >= numero) {
                        libros_encontrados.add(libro_a_buscar);
                    }
                }
                tabla_modificar_libros.getItems().setAll(libros_encontrados);
            } else {
                textfield_cantidad.focusColorProperty().setValue(COLOR_RED);
                tabla_modificar_libros.getItems().setAll(libros_a_buscar);
            }
        }

    }

    public void Buscar_paginas() {
        String texto = textfield_paginas.getText();
        ObservableList<Libro> libros_a_buscar = tabla_modificar_libros.getItems();
        if (texto.length() > 0) {
            if (es_un_numero(texto)) {
                textfield_paginas.focusColorProperty().setValue(COLOR_EXITO);
                int numero = Integer.parseInt(texto);
                ObservableList<Libro> libros_encontrados = FXCollections.observableArrayList();

                for (Libro libro_a_buscar : libros_a_buscar) {
                    if (libro_a_buscar.getPaginas() >= numero) {
                        libros_encontrados.add(libro_a_buscar);
                    }
                }
                tabla_modificar_libros.getItems().setAll(libros_encontrados);
            } else {
                textfield_paginas.focusColorProperty().setValue(COLOR_RED);
                tabla_modificar_libros.getItems().setAll(libros_a_buscar);
            }
        }

    }

    public void Anular_exportar() {
        try {
            exportar.setDisable(!tabmodificar.isSelected());
        } catch (NullPointerException n) {

        }
    }

    public void Buscar_pretado() {
        boolean prestado = combobox_prestado.selectedProperty().getValue();
        ObservableList<Libro> libros_a_buscar = tabla_modificar_libros.getItems();
        ObservableList<Libro> libros_encontrados = FXCollections.observableArrayList();

        for (Libro libro_a_buscar : libros_a_buscar) {
            if (libro_a_buscar.isEsta_prestado() == prestado) {
                libros_encontrados.add(libro_a_buscar);
            }
        }
        tabla_modificar_libros.getItems().setAll(libros_encontrados);
    }

    public void Buscar_estado() {
        String estado_del_libro = String.valueOf(combobox_estado.getSelectionModel().getSelectedIndex());
        ObservableList<Libro> libros_a_buscar = tabla_modificar_libros.getItems();
        if (estado_del_libro != null) {
            ObservableList<Libro> libros_encontrados = FXCollections.observableArrayList();

            for (Libro libro_a_buscar : libros_a_buscar) {
                if (libro_a_buscar.getEstado_del_libro().equals(estado_del_libro)) {
                    libros_encontrados.add(libro_a_buscar);
                }
            }
            tabla_modificar_libros.getItems().setAll(libros_encontrados);
        } else {
            tabla_modificar_libros.getItems().setAll(libros_a_buscar);
        }
    }

    public void Buscar_contenido() {
        String contenido = combobox_tipos_atributo.getSelectionModel().getSelectedItem();
        String texto_a_buscar = textfield_buscar_2.getText().toLowerCase();
        ObservableList<Libro> libros_a_buscar = tabla_modificar_libros.getItems();
        if ((texto_a_buscar.length() > 0) && (combobox_tipos_atributo.getSelectionModel().getSelectedIndex() != -1)) {

            ObservableList<Libro> libros_encontrados = FXCollections.observableArrayList();
            switch (contenido) {
                case Constantes.TITULO_LIBRO:
                    for (Libro libro_a_buscar : libros_a_buscar) {
                        if (libro_a_buscar.getTitulo().contains(texto_a_buscar)) {
                            libros_encontrados.add(libro_a_buscar);
                        }
                    }
                    break;
                case Constantes.ISBN:
                    for (Libro libro_a_buscar : libros_a_buscar) {
                        if (libro_a_buscar.getISBN().contains(texto_a_buscar)) {
                            libros_encontrados.add(libro_a_buscar);
                        }
                    }
                    break;
                default:
                    break;
            }

            tabla_modificar_libros.getItems().setAll(libros_encontrados);
        }

    }

}
