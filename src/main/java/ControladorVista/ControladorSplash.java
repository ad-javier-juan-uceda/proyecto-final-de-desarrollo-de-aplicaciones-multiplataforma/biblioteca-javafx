/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ControladorVista;

import Utilidades.Constantes;
import com.mycompany.proyectobiblioteca.App;
import java.io.IOException;
import java.net.URL;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.prefs.Preferences;
import javafx.animation.FadeTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import javafx.util.Duration;

/**
 *
 * @author batoi
 */
public class ControladorSplash implements Initializable {
    
    @FXML
    ImageView fondo;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        FadeTransition transition = new FadeTransition(Duration.millis(3000),fondo);
        transition.setFromValue(1.0);
        transition.setToValue(1.0);
        transition.play();
 
        transition.setOnFinished((ActionEvent event) -> {
            Stage ventana = (Stage) fondo.getScene().getWindow();
            ventana.hide();
            Stage ventanaApp = new Stage();
            Parent root = null;
            try {
                Locale idioma = App.obtenerIdioma(Preferences.userRoot().node(Constantes.ARCHIVO_PREFERENCIAS).get(Constantes.IDIOMA, Constantes.IDIOMA_POR_DEFECTO));
                root = FXMLLoader.load(App.class.getResource("libros" + ".fxml"), ResourceBundle.getBundle("com.mycompany.proyectobiblioteca.traduccion", idioma));
            } catch (IOException e) {
                e.printStackTrace();
            }
            Scene scene = new Scene(root);
            ventanaApp.setScene(scene);
            ventanaApp.setTitle("Pantalla principal");
            ventanaApp.show();
        });
 
    }
    
}
