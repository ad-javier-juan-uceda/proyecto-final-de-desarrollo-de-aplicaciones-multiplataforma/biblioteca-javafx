/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ControladorVista;

import static ControladorVista.ControladorGlobal.COLOR_EXITO;
import static ControladorVista.ControladorGlobal.COLOR_RED;
import static ControladorVista.ControladorGlobal.autorDAO;
import DAO.AutorDAO;
import Modelo.Autor;
import Utilidades.Constantes;
import Utilidades.Toast;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import com.mycompany.proyectobiblioteca.App;
import java.net.URL;
import java.time.ZoneId;
import java.sql.Date;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import org.apache.xmlrpc.XmlRpcException;

/**
 *
 * @author batoi
 */
public class DialogModificarAutor implements Initializable {

    @FXML
    private GridPane gridpane;

    @FXML
    private Label label_nombre;
    @FXML
    private Label label_telefono;
    @FXML
    private Label label_fecha;
    @FXML
    private Label label_pagina_web;
    @FXML
    private Label label_facebook;
    @FXML
    private Label label_twitter;
    @FXML
    private Label label_instagram;

    @FXML
    private JFXTextField texto_nombre;
    @FXML
    private JFXTextField texto_telefono;
    @FXML
    private JFXTextField texto_pagina_web;
    @FXML
    private JFXTextField texto_facebook;
    @FXML
    private JFXTextField texto_twitter;
    @FXML
    private JFXTextField texto_instagram;
    @FXML
    private DatePicker texto_fecha;
    @FXML
    private JFXButton actualizar;
    @FXML
    private JFXButton cancelar;
    private ResourceBundle resource;

    @Override
    public void initialize(URL arg0, ResourceBundle pResource) {
        resource = pResource;
        Autor autor_seleccionado = ControladorAutor.libro;

        texto_nombre.setText(autor_seleccionado.getNombre());
        texto_telefono.setText(String.valueOf(autor_seleccionado.getTelefono()));
        texto_pagina_web.setText(autor_seleccionado.getPagina_web());
        texto_facebook.setText(autor_seleccionado.getFacebook());
        texto_twitter.setText(autor_seleccionado.getTwitter());
        texto_instagram.setText(autor_seleccionado.getInstagram());
        try{
            Instant instant = Instant.ofEpochMilli(autor_seleccionado.getFecha_de_nacimiento().getTime()); 
            LocalDateTime localDateTime = LocalDateTime.ofInstant(instant, ZoneId.systemDefault()); 
            LocalDate localDate = localDateTime.toLocalDate();

            texto_fecha.setValue(localDate);
        }
        catch(Exception e)
        {
            texto_fecha.setValue((new java.util.Date()).toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
            
        }        
        
        

    }
  
    protected void cambiar_a_cursor_esperando() {
        App.stage.getScene().setCursor(Cursor.WAIT);
    }

    @FXML
    public void actualizar(ActionEvent event) {
        cambiar_a_cursor_esperando();
        Autor autor_actualizacion = ControladorAutor.libro;
        int telefono = 0;

        try {
            telefono = Integer.parseInt(texto_telefono.getText());
        } catch (Exception e) {
            telefono = 0;
        }

        autor_actualizacion.setNombre(texto_nombre.getText());
        autor_actualizacion.setTelefono(telefono);
        autor_actualizacion.setPagina_web(texto_pagina_web.getText());
        autor_actualizacion.setFacebook(texto_facebook.getText());
        autor_actualizacion.setTwitter(texto_twitter.getText());
        autor_actualizacion.setInstagram(texto_instagram.getText());
        autor_actualizacion.setFecha_de_nacimiento(Date.valueOf(texto_fecha.getValue()));
        try {
            autorDAO.actualizarRegistro(autor_actualizacion);
            Toast.makeText(App.stage, resource.getString(Constantes.ACTUALIZACION_AUTOR_CORRECTA), Constantes.TOAST_DELAY, Constantes.FADE_IN_DELAY0, Constantes.FADE_IN_DELAY0, COLOR_EXITO);

        } catch (XmlRpcException ex) {
            Toast.makeText(App.stage, resource.getString(Constantes.ACTUALIZACION_AUTOR_NO_CORRECTA), Constantes.TOAST_DELAY, Constantes.FADE_IN_DELAY0, Constantes.FADE_IN_DELAY0, COLOR_RED);
        } catch (Exception ex) {
            Toast.makeText(App.stage, resource.getString(Constantes.ACTUALIZACION_AUTOR_NO_CORRECTA), Constantes.TOAST_DELAY, Constantes.FADE_IN_DELAY0, Constantes.FADE_IN_DELAY0, COLOR_RED);
        }

        cambiar_a_cursor_normal();
        closeStage(event);
    }

    protected void cambiar_a_cursor_normal() {
        App.stage.getScene().setCursor(Cursor.DEFAULT);
    }

    public void closeStage(ActionEvent event) {
        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        stage.close();
    }

}
