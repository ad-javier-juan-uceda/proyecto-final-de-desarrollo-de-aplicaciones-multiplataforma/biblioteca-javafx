/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ControladorVista;

import static ControladorVista.ControladorGlobal.COLOR_EXITO;
import DAO.EditorialDAO;
import Modelo.Editorial;
import Thread.ThreadPaginaWebEscrito;
import Utilidades.Constantes;
import Utilidades.Dialog;
import Utilidades.Toast;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.controls.JFXToggleButton;
import com.mycompany.proyectobiblioteca.App;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.Tab;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
import org.apache.xmlrpc.XmlRpcException;

/**
 *
 * @author batoi
 */
public class ControladorEditorial extends ControladorGlobal<Editorial> {

    private EditorialDAO librosDAO;
    private ObservableList<Editorial> lista_libros;

    @FXML
    private Label label_titulo;
    @FXML
    private Label label_isbn;
    @FXML
    private Label elegir_editorial;
    @FXML
    private Label prestado;
    @FXML
    private JFXToggleButton button_prestado;
    @FXML
    private Label elegir_autor;
    @FXML
    private Label elegir_tipo;
    @FXML
    private Label elegir_localizaicion;
    @FXML
    private Label elegir_estado;
    @FXML
    private GridPane gridpane;
    @FXML
    private GridPane opciones_modificar;
    @FXML
    private HBox opciones_buscar;
    @FXML
    private HBox opciones_buscar_2;
    @FXML
    private HBox opciones_buscar_3;
    @FXML
    private VBox fondo;
    @FXML
    private JFXTextField textfield_buscar_2;
    @FXML
    private JFXTextField textfield_cantidad;
    @FXML
    private JFXTextField textfield_paginas;
    @FXML
    private Tab tabinsertar;
    @FXML
    private Tab tabmodificar;

    @FXML
    private Label label_contenido;
    @FXML
    private Label label_autor;
    @FXML
    private Label label_editorial;
    @FXML
    private Label label_tipo;
    @FXML
    private Label label_estado;
    @FXML
    private Label label_localizacion;
    @FXML
    private Label label_cantidad;
    @FXML
    private Label label_paginas;

    @FXML
    private Label label_nombre;
    @FXML
    private Label label_telefono;
    @FXML
    private Label label_fecha;
    @FXML
    private Label label_pagina_web;
    @FXML
    private Label label_facebook;
    @FXML
    private Label label_twitter;
    @FXML
    private Label label_instagram;

    @FXML
    private JFXTextField texto_nombre;
    @FXML
    private JFXTextField texto_telefono;
    @FXML
    private JFXTextField texto_pagina_web;
    @FXML
    private JFXTextField texto_facebook;
    @FXML
    private JFXTextField texto_twitter;
    @FXML
    private JFXTextField texto_instagram;
    @FXML
    private JFXTextField texto_fecha;

    @FXML
    private JFXTextField text_localidad;
    @FXML
    private JFXTextField text_direccion;

    @FXML
    private JFXButton button_limpiar;
    @FXML
    private JFXButton cancelar;
    @FXML
    private JFXButton actualizar;
    @FXML
    private Label cantidad;
    @FXML
    private JFXTextField texto_cantidad;
    @FXML
    private JFXTextField texto_titulo;
    @FXML
    private JFXTextField texto_isbn;
    @FXML
    private Label paginas;
    @FXML
    private JFXTextField texto_paginas;

    @FXML
    private TableColumn<Editorial, String> Titulo;
    @FXML
    private TableColumn<Editorial, String> ISBN;
    @FXML
    private TableColumn<Editorial, String> estado_del_libro;
    @FXML
    private TableColumn<Editorial, String> cantidad_scenebuilder;
    @FXML
    private TableColumn<Editorial, String> paginas_scenebuilder;
    @FXML
    private TableColumn<Editorial, String> editorial;
    @FXML
    private TableColumn<Editorial, String> localizacion;
    @FXML
    private TableColumn<Editorial, String> tipo_del_libro;
    @FXML
    private TableColumn<Editorial, String> autor;

    @FXML
    private TableView<Editorial> tabla_modificar_libros;
    public static Editorial libro;
    private ContextMenu menu;
    private JFXComboBox<String> combobox_tipos_atributo;
    @FXML
    private JFXComboBox<String> lista_opciones;

    @Override
    public void insertar() {
        cambiar_a_cursor_esperando();
        Editorial autor_actualizacion = new Editorial();

        autor_actualizacion.setNombre(texto_nombre.getText());
        autor_actualizacion.setPagina_web(texto_pagina_web.getText());
        autor_actualizacion.setFacebook(texto_facebook.getText());
        autor_actualizacion.setTwitter(texto_twitter.getText());
        autor_actualizacion.setInstagram(texto_instagram.getText());
        autor_actualizacion.setDireccion(text_direccion.getText());
        autor_actualizacion.setLocalidad(text_localidad.getText());
        autor_actualizacion.setCIF(texto_fecha.getText());
        try {
            editorialDAO.insertarRegistro(autor_actualizacion);
            Toast.makeText(App.stage, resource.getString(Constantes.ACTUALIZACION_AUTOR_CORRECTA), Constantes.TOAST_DELAY, Constantes.FADE_IN_DELAY0, Constantes.FADE_IN_DELAY0, COLOR_EXITO);

        } catch (XmlRpcException ex) {
            ex.printStackTrace();
            Toast.makeText(App.stage, resource.getString(Constantes.INSERCION_AUTOR_NO_CORRECTA), Constantes.TOAST_DELAY, Constantes.FADE_IN_DELAY0, Constantes.FADE_IN_DELAY0, COLOR_RED);
        } catch (Exception ex) {
            ex.printStackTrace();
            Toast.makeText(App.stage, resource.getString(Constantes.INSERCION_AUTOR_NO_CORRECTA), Constantes.TOAST_DELAY, Constantes.FADE_IN_DELAY0, Constantes.FADE_IN_DELAY0, COLOR_RED);
        }

        cambiar_a_cursor_normal();
        actualizar_datos_tabla();
        panel.getSelectionModel().select(tabmodificar);
        tabla_modificar_libros.scrollTo(tabla_modificar_libros.getItems().size() - 1);
        tabla_modificar_libros.getSelectionModel().select(tabla_modificar_libros.getItems().size() - 1);
        cambiar_a_cursor_normal();
    }

    @Override
    public void modificar() {
        
    }

    @Override
    public void eliminar() {
        
    }

    @Override
    public void Exportar() {
        File archivo_a_guardar = App.guardar_fileChooser(resource.getString(Constantes.EXPORTAR_FILE_CHOOSER), Constantes.CSV);

        if (archivo_a_guardar != null) {

            try ( BufferedWriter escribir = new BufferedWriter(new FileWriter(archivo_a_guardar))) {
                ObservableList<Editorial> libros_a_eliminar = tabla_modificar_libros.getSelectionModel().getSelectedItems();
                StringBuilder texto = new StringBuilder();
                for (Editorial libro_a_eliminar : libros_a_eliminar) {
                    texto.append(libro_a_eliminar.getNombre()).append(Constantes.SEPARADOR_EXPORTACION);
                    texto.append(libro_a_eliminar.getFacebook()).append(Constantes.SEPARADOR_EXPORTACION);
                    texto.append(libro_a_eliminar.getCIF()).append(Constantes.SEPARADOR_EXPORTACION);
                    texto.append(libro_a_eliminar.getDireccion()).append(Constantes.SEPARADOR_EXPORTACION);
                    texto.append(libro_a_eliminar.getLocalidad()).append(Constantes.SEPARADOR_EXPORTACION);
                    texto.append(libro_a_eliminar.getInstagram()).append(Constantes.SEPARADOR_EXPORTACION);
                    texto.append(libro_a_eliminar.getPagina_web()).append(Constantes.SEPARADOR_EXPORTACION);
                    texto.append(libro_a_eliminar.getTelefono()).append(Constantes.SEPARADOR_EXPORTACION);
                    texto.append(libro_a_eliminar.getTwitter()).append(Constantes.SEPARADOR_EXPORTACION);
                    texto.append(System.lineSeparator());
                }
                escribir.write(texto.toString());

            } catch (IOException ex) {
                Dialog.Mensaje_de_excepcion(ex);
            }
            Dialog.Dialogo_de_confirmacion(resource.getString(Constantes.CONFIRMACION_EXPORTACION), archivo_a_guardar);
        }

    }

    @Override
    public void Cargar_datos() {
        try {
            librosDAO = new EditorialDAO();
            lista_libros = FXCollections.observableArrayList();
            menu = new ContextMenu();
            Anular_exportar();
            actualizar_datos_tabla();
        } catch (XmlRpcException ex) {
            Dialog.Mensaje_de_excepcion(ex);
        } catch (Exception ex) {
            Dialog.Mensaje_de_excepcion(ex);
        }
    }

    public void ir_a_la_pagina(String accion) {

        Editorial autor = tabla_modificar_libros.getSelectionModel().getSelectedItem();
        if ((autor != null) && (tabla_modificar_libros.getSelectionModel().getSelectedItems().size() == 1)) {
            switch (accion) {
                case Constantes.FACEBOOK:
                    new ThreadPaginaWebEscrito(autor.getFacebook()).start();
                    break;
                case Constantes.PAGINA_WEB:
                    new ThreadPaginaWebEscrito(autor.getPagina_web()).start();
                    break;
                case Constantes.TWITTER:
                    new ThreadPaginaWebEscrito(autor.getTwitter()).start();
                    break;
                case Constantes.INSTAGRAM:
                    new ThreadPaginaWebEscrito(autor.getInstagram()).start();
                    break;
            }
        }

    }

    public void actualizar_datos_tabla() {

        try {
            lista_libros.setAll(librosDAO.buscarTodos());
            tabla_modificar_libros.setItems(lista_libros);

            Titulo.setCellValueFactory(o -> new SimpleStringProperty(o.getValue().getNombre()));
            ISBN.setCellValueFactory(o -> new SimpleStringProperty(String.valueOf(o.getValue().getTelefono())));
            estado_del_libro.setCellValueFactory(o -> new SimpleStringProperty(o.getValue().getCIF()));
            editorial.setCellValueFactory(o -> new SimpleStringProperty(o.getValue().getDireccion()));
            localizacion.setCellValueFactory(o -> new SimpleStringProperty(o.getValue().getLocalidad()));

            Titulo.setSortable(true);
            ISBN.setSortable(true);
            estado_del_libro.setSortable(true);

            //editorial.setSortable(true);
            localizacion.setSortable(true);
            //tipo_del_libro.setSortable(true);
            //autor.setSortable(true);
            MenuItem menuItem1 = new MenuItem(resource.getString(Constantes.ELIMINAR));
            MenuItem menuItem2 = new MenuItem(resource.getString(Constantes.ELIMINAR_TODOS_LOS_SELECCIONADOS));

            MenuItem menuItem3 = new MenuItem(resource.getString(Constantes.PAGINA_WEB));
            MenuItem menuItem4 = new MenuItem(resource.getString(Constantes.FACEBOOK));
            MenuItem menuItem5 = new MenuItem(resource.getString(Constantes.TWITTER));
            MenuItem menuItem6 = new MenuItem(resource.getString(Constantes.INSTAGRAM));

            menuItem1.setOnAction((event) -> {
                try {

                    if (Dialog.Dialogo_de_confirmacion(resource.getString(Constantes.PREGUNTA_ELIMINAR))) {
                        librosDAO.eliminarRegistro(tabla_modificar_libros.getSelectionModel().getSelectedItem());
                        actualizar_datos_tabla();
                    }

                } catch (XmlRpcException ex) {
                    Dialog.Mensaje_de_excepcion(ex);
                } catch (Exception ex) {
                    Dialog.Mensaje_de_excepcion(ex);
                }
            });

            menuItem2.setOnAction((event) -> {
                try {

                    if (Dialog.Dialogo_de_confirmacion(resource.getString(Constantes.PREGUNTA_ELIMINAR))) {
                        ObservableList<Editorial> libros_a_eliminar = tabla_modificar_libros.getSelectionModel().getSelectedItems();
                        for (Editorial libro_a_eliminar : libros_a_eliminar) {
                            librosDAO.eliminarRegistro(libro_a_eliminar);
                        }
                        actualizar_datos_tabla();
                    }

                } catch (XmlRpcException ex) {
                    Dialog.Mensaje_de_excepcion(ex);
                } catch (Exception ex) {
                    Dialog.Mensaje_de_excepcion(ex);
                }
            });

            menuItem3.setOnAction((event) -> {
                ir_a_la_pagina(Constantes.PAGINA_WEB);
            });
            menuItem4.setOnAction((event) -> {
                ir_a_la_pagina(Constantes.FACEBOOK);
            });
            menuItem5.setOnAction((event) -> {
                ir_a_la_pagina(Constantes.TWITTER);
            });
            menuItem6.setOnAction((event) -> {
                ir_a_la_pagina(Constantes.INSTAGRAM);
            });

            menu.getItems().setAll(menuItem1, menuItem2, menuItem3, menuItem4, menuItem5, menuItem6);
            tabla_modificar_libros.setContextMenu(menu);
            tabla_modificar_libros.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

            Editorial autor_seleccionado = new Editorial();

            texto_nombre.setText(autor_seleccionado.getNombre());
            texto_telefono.setText(String.valueOf(autor_seleccionado.getTelefono()));
            texto_pagina_web.setText(autor_seleccionado.getPagina_web());
            texto_facebook.setText(autor_seleccionado.getFacebook());
            texto_twitter.setText(autor_seleccionado.getTwitter());
            texto_instagram.setText(autor_seleccionado.getInstagram());
            text_direccion.setText(autor_seleccionado.getDireccion());
            text_localidad.setText(autor_seleccionado.getLocalidad());
            texto_fecha.setText(autor_seleccionado.getCIF());

            lista_opciones.getItems().setAll(Constantes.NOMBRE_EDITORIAL, Constantes.TELEFONO_EDITORIAL, Constantes.DIRECCION, Constantes.LOCALIDAD, Constantes.CIF);

        } catch (XmlRpcException ex) {
            Dialog.Mensaje_de_excepcion(ex);
        } catch (Exception ex) {
            Dialog.Mensaje_de_excepcion(ex);
        }
    }

    @Override
    public void actualizar() {
        actualizar_datos_tabla();
    }

    public void pantalla_modificar_libro(MouseEvent mouse) {

        if (Dialog.es_doble_click(mouse) && Dialog.es_boton_izquierdo(mouse)) {
            Parent parent = null;
            try {
                libro = tabla_modificar_libros.getSelectionModel().getSelectedItem();
                if (libro != null) {
                    FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("dialogmodificareditorial.fxml"),resource);
                    parent = fxmlLoader.load();
                    Scene scene = new Scene(parent);
                    Stage stage = new Stage();
                    stage.initModality(Modality.APPLICATION_MODAL);
                    stage.setScene(scene);
                    stage.showAndWait();
                    actualizar_datos_tabla();
                    cambiar_a_cursor_normal();
                }

            } catch (IOException ex) {
                Dialog.Mensaje_de_excepcion(ex);
            } catch (Exception ex) {
                Dialog.Mensaje_de_excepcion(ex);
            }
        }
    }

    public void Limpiar_busqueda() {
        textfield_buscar_2.setText(Constantes.VACIO);

        try {
            tabla_modificar_libros.getItems().setAll(librosDAO.buscarTodos());
        } catch (XmlRpcException ex) {
            Dialog.Mensaje_de_excepcion(ex);
        } catch (Exception ex) {
            Dialog.Mensaje_de_excepcion(ex);
        }

    }

    public void Buscar_contenido() {

        String texto_a_buscar = textfield_buscar_2.getText().toLowerCase();
        ObservableList<Editorial> libros_a_buscar = tabla_modificar_libros.getItems();
        if ((texto_a_buscar.length() > 0)) {
            ObservableList<Editorial> libros_encontrados = FXCollections.observableArrayList();

            for (Editorial autor : libros_a_buscar) {

                switch (lista_opciones.getSelectionModel().getSelectedItem()) {
                    case Constantes.NOMBRE_EDITORIAL:

                        if (autor.getNombre().toLowerCase().contains(texto_a_buscar)) {
                            libros_encontrados.add(autor);
                        }

                        break;
                    case Constantes.TELEFONO_EDITORIAL:

                        if (String.valueOf(autor.getTelefono()).toLowerCase().contains(texto_a_buscar)) {
                            libros_encontrados.add(autor);
                        }

                        break;
                    case Constantes.DIRECCION:

                        if (autor.getDireccion().toLowerCase().contains(texto_a_buscar)) {
                            libros_encontrados.add(autor);
                        }

                        break;
                    case Constantes.LOCALIDAD:

                        if (autor.getLocalidad().toLowerCase().contains(texto_a_buscar)) {
                            libros_encontrados.add(autor);
                        }

                        break;

                    case Constantes.CIF:

                        if (autor.getCIF().toLowerCase().contains(texto_a_buscar)) {
                            libros_encontrados.add(autor);
                        }

                        break;
                }

            }

            tabla_modificar_libros.getItems().setAll(libros_encontrados);
        }

    }

    public void Anular_exportar() {
        try {
            exportar.setDisable(!tabmodificar.isSelected());
        } catch (NullPointerException n) {

        }
    }

}
