/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ControladorVista;

import static ControladorVista.ControladorGlobal.COLOR_EXITO;
import static ControladorVista.ControladorGlobal.COLOR_RED;
import static ControladorVista.ControladorGlobal.autorDAO;
import static ControladorVista.ControladorGlobal.editorialDAO;
import static ControladorVista.ControladorGlobal.*;
import Modelo.Autor;
import Modelo.Editorial;
import Modelo.Libro;
import Modelo.Localizacion;
import Modelo.Tipo;
import Utilidades.Constantes;
import Utilidades.Dialog;
import Utilidades.Toast;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.controls.JFXToggleButton;
import com.mycompany.proyectobiblioteca.App;
import static com.mycompany.proyectobiblioteca.App.crear_threads;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import org.apache.xmlrpc.XmlRpcException;

/**
 *
 * @author batoi
 */
public class DialogModificarLibro implements Initializable {

    private ResourceBundle resource;

    @FXML
    private GridPane gridpane;

    @FXML
    private Label label_contenido;
    @FXML
    private Label label_autor;
    @FXML
    private Label label_editorial;
    @FXML
    private Label label_tipo;
    @FXML
    private Label label_estado;
    @FXML
    private Label label_localizacion;
    @FXML
    private Label label_cantidad;
    @FXML
    private Label label_paginas;

    @FXML
    private JFXTextField texto_paginas;
    @FXML
    private JFXToggleButton button_prestado;
    @FXML
    private JFXComboBox<String> texto_estado;
    @FXML
    private JFXComboBox<Editorial> texto_editorial;
    @FXML
    private JFXComboBox<Localizacion> texto_localizacion;
    @FXML
    private JFXComboBox<Tipo> texto_tipo;
    @FXML
    private JFXComboBox<Autor> texto_autor;

    @FXML
    private JFXTextField texto_cantidad;
    @FXML
    private JFXTextField texto_titulo;
    @FXML
    private JFXTextField texto_isbn;

    @Override
    public void initialize(URL arg0, ResourceBundle pResource) {
        Libro libro_seleccionado = ControladorLibros.libro;
        resource = pResource;
        try {
            texto_editorial.getItems().setAll(editorialDAO.buscarTodos());
            texto_autor.getItems().setAll(autorDAO.buscarTodos());
            texto_tipo.getItems().setAll(tipoDBDAO.buscarTodos());
            texto_localizacion.getItems().setAll(localizacionDAO.buscarTodos());
        } catch (XmlRpcException ex) {
            Dialog.Mensaje_de_excepcion(ex);
        } catch (Exception ex) {
            Dialog.Mensaje_de_excepcion(ex);
        }
        texto_paginas.setText(String.valueOf(libro_seleccionado.getPaginas()));
        texto_cantidad.setText(String.valueOf(libro_seleccionado.getCantidad()));
        texto_editorial.getSelectionModel().select(libro_seleccionado.getEditorial());
        texto_localizacion.getSelectionModel().select(libro_seleccionado.getLocalizacion());
        texto_tipo.getSelectionModel().select(libro_seleccionado.getTipo_del_libro());
        texto_autor.getSelectionModel().select(libro_seleccionado.getAutor());
        texto_titulo.setText(libro_seleccionado.getTitulo());
        texto_isbn.setText(libro_seleccionado.getISBN());
        texto_estado.getItems().clear();
        button_prestado.setSelected(libro_seleccionado.isEsta_prestado());

        String[] estados_posibles = {
            resource.getString(Constantes.BUEN_ESTADO),
            resource.getString(Constantes.ESTROPEADO),
            resource.getString(Constantes.INUTILIZABLE),
            resource.getString(Constantes.PAGINAS_ROTAS)

        };

        for (String estado : estados_posibles) {
            texto_estado.getItems().add(estado);
        }
        texto_estado.getSelectionModel().select(Integer.parseInt(libro_seleccionado.getEstado_del_libro()));

    }

    protected void cambiar_a_cursor_esperando() {
        App.stage.getScene().setCursor(Cursor.WAIT);
    }

    @FXML
    public void actualizar(ActionEvent event) {
        cambiar_a_cursor_esperando();
        Libro libro_actualizacion = new Libro();
        libro_actualizacion.setId(ControladorLibros.libro.getId());
        libro_actualizacion.setTitulo(texto_titulo.getText());
        libro_actualizacion.setISBN(texto_isbn.getText());
        libro_actualizacion.setEditorial(texto_editorial.getSelectionModel().getSelectedItem());
        libro_actualizacion.setEsta_prestado(button_prestado.isSelected());
        libro_actualizacion.setEstado_del_libro(String.valueOf(texto_estado.getSelectionModel().getSelectedIndex()));
        libro_actualizacion.setLocalizacion(texto_localizacion.getSelectionModel().getSelectedItem());
        int paginas = 0;
        try {
            paginas = Integer.parseInt(texto_paginas.getText());
        } catch (Exception e) {
            paginas = 0;
        }
        int cantidad = 0;
        try {
            cantidad = Integer.parseInt(texto_cantidad.getText());
        } catch (Exception e) {
            cantidad = 0;
        }
        libro_actualizacion.setPaginas(paginas);
        libro_actualizacion.setCantidad(cantidad);
        libro_actualizacion.setTipo_del_libro(texto_tipo.getSelectionModel().getSelectedItem());
        libro_actualizacion.setAutor(texto_autor.getSelectionModel().getSelectedItem());

        try {
            libroDAO.actualizarRegistro(libro_actualizacion);
            Toast.makeText(App.stage, resource.getString(Constantes.ACTUALIZACION_AUTOR_CORRECTA), Constantes.TOAST_DELAY, Constantes.FADE_IN_DELAY0, Constantes.FADE_IN_DELAY0, COLOR_EXITO);
            crear_threads();
        } catch (XmlRpcException ex) {
            Toast.makeText(App.stage, resource.getString(Constantes.INSERCION_AUTOR_NO_CORRECTA), Constantes.TOAST_DELAY, Constantes.FADE_IN_DELAY0, Constantes.FADE_IN_DELAY0, COLOR_RED);
        } catch (Exception ex) {
            Toast.makeText(App.stage, resource.getString(Constantes.INSERCION_AUTOR_NO_CORRECTA), Constantes.TOAST_DELAY, Constantes.FADE_IN_DELAY0, Constantes.FADE_IN_DELAY0, COLOR_RED);
        }

        cambiar_a_cursor_normal();
        closeStage(event);
    }

    protected void cambiar_a_cursor_normal() {
        App.stage.getScene().setCursor(Cursor.DEFAULT);
    }

    public void closeStage(ActionEvent event) {
        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        stage.close();
    }

}
