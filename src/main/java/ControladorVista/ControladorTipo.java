/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ControladorVista;

import static ControladorVista.ControladorGlobal.COLOR_EXITO;
import DAO.TipoDAO;
import Modelo.Tipo;
import Utilidades.Constantes;
import Utilidades.Dialog;
import Utilidades.Toast;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.controls.JFXToggleButton;
import com.mycompany.proyectobiblioteca.App;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.apache.xmlrpc.XmlRpcException;

/**
 *
 * @author batoi
 */
public class ControladorTipo extends ControladorGlobal<Tipo> {

    private TipoDAO librosDAO;
    private ObservableList<Tipo> lista_libros;

    @FXML
    private Label label_titulo;
    @FXML
    private Label label_isbn;
    @FXML
    private Label elegir_editorial;
    @FXML
    private Label prestado;
    @FXML
    private JFXToggleButton button_prestado;
    @FXML
    private Label elegir_autor;
    @FXML
    private Label elegir_tipo;
    @FXML
    private Label elegir_localizaicion;
    @FXML
    private Label elegir_estado;
    @FXML
    private GridPane gridpane;
    @FXML
    private GridPane opciones_modificar;
    @FXML
    private HBox opciones_buscar;
    @FXML
    private HBox opciones_buscar_2;
    @FXML
    private HBox opciones_buscar_3;
    @FXML
    private VBox fondo;
    @FXML
    private JFXTextField textfield_buscar_2;
    @FXML
    private JFXTextField textfield_cantidad;
    @FXML
    private JFXTextField textfield_paginas;
    @FXML
    private Tab tabinsertar;
    @FXML
    private Tab tabmodificar;

    @FXML
    private Label label_contenido;
    @FXML
    private Label label_autor;
    @FXML
    private Label label_editorial;
    @FXML
    private Label label_tipo;
    @FXML
    private Label label_estado;
    @FXML
    private Label label_localizacion;
    @FXML
    private Label label_cantidad;
    @FXML
    private Label label_paginas;

    @FXML
    private Label label_nombre;
    @FXML
    private Label label_telefono;
    @FXML
    private Label label_fecha;
    @FXML
    private Label label_pagina_web;
    @FXML
    private Label label_facebook;
    @FXML
    private Label label_twitter;
    @FXML
    private Label label_instagram;

    @FXML
    private JFXTextField texto_nombre;
    @FXML
    private JFXTextArea texto_telefono;
    @FXML
    private JFXTextField texto_pagina_web;
    @FXML
    private JFXTextField texto_facebook;
    @FXML
    private JFXTextField texto_twitter;
    @FXML
    private JFXTextField texto_instagram;
    @FXML
    private JFXTextField texto_fecha;

    @FXML
    private JFXTextField text_localidad;
    @FXML
    private JFXTextField text_direccion;

    @FXML
    private JFXButton button_limpiar;
    @FXML
    private JFXButton cancelar;
    @FXML
    private JFXButton actualizar;
    @FXML
    private Label cantidad;
    @FXML
    private JFXTextField texto_cantidad;
    @FXML
    private JFXTextField texto_titulo;
    @FXML
    private JFXTextField texto_isbn;
    @FXML
    private Label paginas;
    @FXML
    private JFXTextField texto_paginas;

    @FXML
    private TableColumn<Tipo, String> Titulo;
    @FXML
    private TableColumn<Tipo, String> ISBN;
    @FXML
    private TableColumn<Tipo, String> estado_del_libro;
    @FXML
    private TableColumn<Tipo, String> cantidad_scenebuilder;
    @FXML
    private TableColumn<Tipo, String> paginas_scenebuilder;
    @FXML
    private TableColumn<Tipo, String> editorial;
    @FXML
    private TableColumn<Tipo, String> localizacion;
    @FXML
    private TableColumn<Tipo, String> tipo_del_libro;
    @FXML
    private TableColumn<Tipo, String> autor;

    @FXML
    private TableView<Tipo> tabla_modificar_libros;
    public static Tipo libro;
    private ContextMenu menu;
    private JFXComboBox<String> combobox_tipos_atributo;
    @FXML
    private JFXComboBox<String> lista_opciones;

    @Override
    public void insertar() {
        cambiar_a_cursor_esperando();
        Tipo autor_seleccionado = new Tipo();

        autor_seleccionado.setNombre(texto_nombre.getText());
        autor_seleccionado.setDescripcion(texto_telefono.getText());

        try {
            tipoDBDAO.insertarRegistro(autor_seleccionado);
            Toast.makeText(App.stage, resource.getString(Constantes.INSERCION_AUTOR_CORRECTA), Constantes.TOAST_DELAY, Constantes.FADE_IN_DELAY0, Constantes.FADE_IN_DELAY0, COLOR_EXITO);

        } catch (XmlRpcException ex) {
            Toast.makeText(App.stage, resource.getString(Constantes.INSERCION_AUTOR_NO_CORRECTA), Constantes.TOAST_DELAY, Constantes.FADE_IN_DELAY0, Constantes.FADE_IN_DELAY0, COLOR_RED);
        } catch (Exception ex) {
            Toast.makeText(App.stage, resource.getString(Constantes.INSERCION_AUTOR_NO_CORRECTA), Constantes.TOAST_DELAY, Constantes.FADE_IN_DELAY0, Constantes.FADE_IN_DELAY0, COLOR_RED);
        }

        cambiar_a_cursor_normal();
        actualizar_datos_tabla();
        panel.getSelectionModel().select(tabmodificar);
        tabla_modificar_libros.scrollTo(tabla_modificar_libros.getItems().size() - 1);
        tabla_modificar_libros.getSelectionModel().select(tabla_modificar_libros.getItems().size() - 1);
        cambiar_a_cursor_normal();
    }

    @Override
    public void modificar() {
        
    }

    @Override
    public void eliminar() {
        
    }

    @Override
    public void Exportar() {
        File archivo_a_guardar = App.guardar_fileChooser(resource.getString(Constantes.EXPORTAR_FILE_CHOOSER), Constantes.CSV);

        if (archivo_a_guardar != null) {

            try ( BufferedWriter escribir = new BufferedWriter(new FileWriter(archivo_a_guardar))) {
                ObservableList<Tipo> libros_a_eliminar = tabla_modificar_libros.getSelectionModel().getSelectedItems();
                StringBuilder texto = new StringBuilder();
                for (Tipo libro_a_eliminar : libros_a_eliminar) {
                    texto.append(libro_a_eliminar.getNombre()).append(Constantes.SEPARADOR_EXPORTACION);
                    texto.append(libro_a_eliminar.getDescripcion().replace(Constantes.RETORNON, Constantes.ESPACIO).replace(System.lineSeparator(), Constantes.ESPACIO)).append(Constantes.SEPARADOR_EXPORTACION);
                    texto.append(System.lineSeparator());
                }
                escribir.write(texto.toString());

            } catch (IOException ex) {
                Dialog.Mensaje_de_excepcion(ex);
            }
            Dialog.Dialogo_de_confirmacion(resource.getString(Constantes.CONFIRMACION_EXPORTACION), archivo_a_guardar);
        }

    }

    @Override
    public void Cargar_datos() {
        try {
            librosDAO = new TipoDAO();
            lista_libros = FXCollections.observableArrayList();
            menu = new ContextMenu();
            Anular_exportar();
            actualizar_datos_tabla();
        } catch (XmlRpcException ex) {
            Dialog.Mensaje_de_excepcion(ex);
        } catch (Exception ex) {
            Dialog.Mensaje_de_excepcion(ex);
        }
    }

    public void actualizar_datos_tabla() {

        try {
            lista_libros.setAll(librosDAO.buscarTodos());
            tabla_modificar_libros.setItems(lista_libros);

            Titulo.setCellValueFactory(o -> new SimpleStringProperty(o.getValue().getNombre()));
            ISBN.setCellValueFactory(o -> {
                return new SimpleStringProperty(o.getValue().getDescripcion());
            });

            Titulo.setSortable(true);
            ISBN.setSortable(true);

            MenuItem menuItem1 = new MenuItem(resource.getString(Constantes.ELIMINAR));
            MenuItem menuItem2 = new MenuItem(resource.getString(Constantes.ELIMINAR_TODOS_LOS_SELECCIONADOS));

            menuItem1.setOnAction((event) -> {
                try {

                    if (Dialog.Dialogo_de_confirmacion(resource.getString(Constantes.PREGUNTA_ELIMINAR))) {
                        librosDAO.eliminarRegistro(tabla_modificar_libros.getSelectionModel().getSelectedItem());
                        actualizar_datos_tabla();
                    }

                } catch (XmlRpcException ex) {
                    Dialog.Mensaje_de_excepcion(ex);
                } catch (Exception ex) {
                    Dialog.Mensaje_de_excepcion(ex);
                }
            });

            menuItem2.setOnAction((event) -> {
                try {

                    if (Dialog.Dialogo_de_confirmacion(resource.getString(Constantes.PREGUNTA_ELIMINAR))) {
                        ObservableList<Tipo> libros_a_eliminar = tabla_modificar_libros.getSelectionModel().getSelectedItems();
                        for (Tipo libro_a_eliminar : libros_a_eliminar) {
                            librosDAO.eliminarRegistro(libro_a_eliminar);
                        }
                        actualizar_datos_tabla();
                    }

                } catch (XmlRpcException ex) {
                    Dialog.Mensaje_de_excepcion(ex);
                } catch (Exception ex) {
                    Dialog.Mensaje_de_excepcion(ex);
                }
            });

            menu.getItems().setAll(menuItem1, menuItem2);
            tabla_modificar_libros.setContextMenu(menu);
            tabla_modificar_libros.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

            Tipo autor_seleccionado = new Tipo();

            texto_nombre.setText(autor_seleccionado.getNombre());

            texto_telefono.setText(String.valueOf(autor_seleccionado.getDescripcion()));

            lista_opciones.getItems().setAll(Constantes.NOMBRE_TIPO, Constantes.DESCRIPCION_TIPO);

        } catch (XmlRpcException ex) {
            Dialog.Mensaje_de_excepcion(ex);
        } catch (Exception ex) {
            Dialog.Mensaje_de_excepcion(ex);
        }
    }

    @Override
    public void actualizar() {
        actualizar_datos_tabla();
    }

    public void pantalla_modificar_libro(MouseEvent mouse) {

        if (Dialog.es_doble_click(mouse) && Dialog.es_boton_izquierdo(mouse)) {
            Parent parent = null;
            try {
                libro = tabla_modificar_libros.getSelectionModel().getSelectedItem();
                if (libro != null) {
                    FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("dialogmodificartipo.fxml"),resource);
                    parent = fxmlLoader.load();
                    Scene scene = new Scene(parent);
                    Stage stage = new Stage();
                    stage.initModality(Modality.APPLICATION_MODAL);
                    stage.setScene(scene);
                    stage.showAndWait();
                    actualizar_datos_tabla();
                    cambiar_a_cursor_normal();
                }

            } catch (IOException ex) {
                Dialog.Mensaje_de_excepcion(ex);
            } catch (Exception ex) {
                Dialog.Mensaje_de_excepcion(ex);
            }
        }
    }

    public void Limpiar_busqueda() {
        textfield_buscar_2.setText(Constantes.VACIO);

        try {
            tabla_modificar_libros.getItems().setAll(librosDAO.buscarTodos());
        } catch (XmlRpcException ex) {
            Dialog.Mensaje_de_excepcion(ex);
        } catch (Exception ex) {
            Dialog.Mensaje_de_excepcion(ex);
        }

    }

    public void Buscar_contenido() {

        String texto_a_buscar = textfield_buscar_2.getText().toLowerCase();
        ObservableList<Tipo> libros_a_buscar = tabla_modificar_libros.getItems();
        if ((texto_a_buscar.length() > 0)) {
            ObservableList<Tipo> libros_encontrados = FXCollections.observableArrayList();

            for (Tipo autor : libros_a_buscar) {

                switch (lista_opciones.getSelectionModel().getSelectedItem()) {
                    case Constantes.NOMBRE_TIPO:

                        if (autor.getNombre().toLowerCase().contains(texto_a_buscar)) {
                            libros_encontrados.add(autor);
                        }

                        break;
                    case Constantes.DESCRIPCION_TIPO:

                        if (autor.getDescripcion().toLowerCase().contains(texto_a_buscar)) {
                            libros_encontrados.add(autor);
                        }

                        break;
                }

            }

            tabla_modificar_libros.getItems().setAll(libros_encontrados);
        }

    }

    public void Anular_exportar() {
        try {
            exportar.setDisable(!tabmodificar.isSelected());
        } catch (NullPointerException n) {

        }
    }

}
