/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ControladorVista;

import static ControladorVista.ControladorGlobal.COLOR_EXITO;
import static ControladorVista.ControladorGlobal.COLOR_RED;
import static ControladorVista.ControladorPedido.*;
import DAO.PedidoDAO;
import DAO.Linea_pedidoDAO;
import Modelo.Linea_pedido;
import Modelo.Pedido;
import Utilidades.Constantes;
import Utilidades.Dialog;
import Utilidades.Toast;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import com.mycompany.proyectobiblioteca.App;
import java.io.IOException;
import java.net.URL;
import java.time.ZoneId;
import java.sql.Date;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.apache.xmlrpc.XmlRpcException;

/**
 *
 * @author batoi
 */
public class DialogModificarPedido implements Initializable {

    public static Linea_pedido pedido;
    public static Pedido Pedido;
    
    

    @FXML
    private GridPane gridpane;

    @FXML
    private Label label_nombre;
    @FXML
    private Label label_telefono;
    @FXML
    private Label label_fecha;
    @FXML
    private Label label_pagina_web;
    @FXML
    private Label label_facebook;
    @FXML
    private Label label_twitter;
    @FXML
    private Label label_instagram;

    @FXML
    private JFXTextField texto_nombre;
    @FXML
    private JFXTextField texto_telefono;
    @FXML
    private JFXTextField texto_pagina_web;
    @FXML
    private JFXTextField texto_facebook;
    @FXML
    private JFXTextField texto_twitter;
    @FXML
    private JFXTextField texto_instagram;

    @FXML
    private JFXButton actualizar;
    @FXML
    private JFXButton cancelar;
    @FXML
    private DatePicker texto_fecha_pedido;
    @FXML
    private DatePicker texto_entrega;
    @FXML
    private TableColumn<Linea_pedido, String> columnalinro;
    @FXML
    private TableColumn<Linea_pedido, String> columnacantidad;
    @FXML
    private TableColumn<Linea_pedido, String> columnaprecio;

    @FXML
    private TableView<Linea_pedido> tabla;
    private ResourceBundle resource;

    @Override
    public void initialize(URL arg0, ResourceBundle pResource) {
        //Pedido libro_seleccionado = ControladorPedido.autor;
        resource = pResource;
        Pedido = ControladorPedido.libro;

        try {
            Instant instant = Instant.ofEpochMilli(Pedido.getFecha_de_entrega().getTime());
            LocalDateTime localDateTime = LocalDateTime.ofInstant(instant, ZoneId.systemDefault());
            LocalDate localDate = localDateTime.toLocalDate();

            texto_fecha_pedido.setValue(localDate);
        } catch (Exception e) {
            texto_fecha_pedido.setValue((new java.util.Date()).toInstant().atZone(ZoneId.systemDefault()).toLocalDate());

        }

        try {
            Instant instant = Instant.ofEpochMilli(Pedido.getFecha_de_realizacion().getTime());
            LocalDateTime localDateTime = LocalDateTime.ofInstant(instant, ZoneId.systemDefault());
            LocalDate localDate = localDateTime.toLocalDate();

            texto_entrega.setValue(localDate);
        } catch (Exception e) {
            texto_entrega.setValue((new java.util.Date()).toInstant().atZone(ZoneId.systemDefault()).toLocalDate());

        }

        actualizar_datos_tabla();

    }

    protected void cambiar_a_cursor_esperando() {
        App.stage.getScene().setCursor(Cursor.WAIT);
    }

    @FXML
    public void actualizar(ActionEvent event) {

        Pedido autor_seleccionado = ControladorPedido.libro;
        System.out.println(autor_seleccionado.getId());
        cambiar_a_cursor_esperando();

        autor_seleccionado.setFecha_de_entrega(Date.valueOf(texto_fecha_pedido.getValue()));
        autor_seleccionado.setFecha_de_realizacion(Date.valueOf(texto_entrega.getValue()));
//        ArrayList<Linea_pedido> lineas = new ArrayList();

        for (Linea_pedido linea : Pedido.getLineas_pedido()) {
            linea.setPedido(autor_seleccionado);
        }

        autor_seleccionado.setLineas_pedido(Pedido.getLineas_pedido());
        try {
            pedidoDBDAO.actualizarRegistro(autor_seleccionado);
            Toast.makeText(App.stage, resource.getString(Constantes.INSERCION_AUTOR_CORRECTA), Constantes.TOAST_DELAY, Constantes.FADE_IN_DELAY0, Constantes.FADE_IN_DELAY0, COLOR_EXITO);

        } catch (XmlRpcException ex) {
            ex.printStackTrace();
            Toast.makeText(App.stage, resource.getString(Constantes.INSERCION_AUTOR_NO_CORRECTA), Constantes.TOAST_DELAY, Constantes.FADE_IN_DELAY0, Constantes.FADE_IN_DELAY0, COLOR_RED);
        } catch (Exception ex) {
            ex.printStackTrace();
            Toast.makeText(App.stage, resource.getString(Constantes.INSERCION_AUTOR_NO_CORRECTA), Constantes.TOAST_DELAY, Constantes.FADE_IN_DELAY0, Constantes.FADE_IN_DELAY0, COLOR_RED);
        }

        cambiar_a_cursor_normal();
        closeStage(event);

    }

    @FXML
    public void anyadirlinea(ActionEvent event) {
        try {
            pedido = null;
            Parent parent = null;
            FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("dialogmodificarlineapedido.fxml"),resource);
            parent = fxmlLoader.load();
            Scene scene = new Scene(parent);
            Stage stage = new Stage();
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setScene(scene);
            stage.showAndWait();
            Pedido.getLineas_pedido().add(DialogModificarLineaPedido.lineapedido);
            actualizar_datos_tabla();
        } catch (IOException ex) {
            Dialog.Mensaje_de_excepcion(ex);
        }
    }

    protected void cambiar_a_cursor_normal() {
        App.stage.getScene().setCursor(Cursor.DEFAULT);
    }

    public void closeStage(ActionEvent event) {
        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        stage.close();
    }

    private void actualizar_datos_tabla() {
        Pedido autor_seleccionado;
        autor_seleccionado = ControladorPedido.libro;

        ObservableList<Linea_pedido> lineas = FXCollections.observableArrayList();
        lineas.setAll(Pedido.getLineas_pedido());
        tabla.setItems(lineas);

        columnalinro.setCellValueFactory(o -> new SimpleStringProperty(o.getValue().getLibro().toString()));
        columnacantidad.setCellValueFactory(o -> new SimpleStringProperty(String.valueOf(o.getValue().getCantidad())));
        columnaprecio.setCellValueFactory(o -> new SimpleStringProperty(String.valueOf(o.getValue().getPrecio())));

        columnalinro.setSortable(true);

        MenuItem menuItem1 = new MenuItem(resource.getString(Constantes.ELIMINAR));
        MenuItem menuItem2 = new MenuItem(resource.getString(Constantes.ELIMINAR_TODOS_LOS_SELECCIONADOS));

        menuItem1.setOnAction((event) -> {
            try {

                if (Dialog.Dialogo_de_confirmacion(resource.getString(Constantes.PREGUNTA_ELIMINAR))) {
                    Linea_pedidoDBDAO.eliminarRegistro(tabla.getSelectionModel().getSelectedItem());
                    Pedido = pedidoDBDAO.buscarPorClavePrimaria(Pedido.getId());
                    actualizar_datos_tabla();

                }

            } catch (XmlRpcException ex) {
                Dialog.Mensaje_de_excepcion(ex);
            } catch (Exception ex) {
                Dialog.Mensaje_de_excepcion(ex);
            }
        });

        menuItem2.setOnAction((event) -> {
            try {

                if (Dialog.Dialogo_de_confirmacion(resource.getString(Constantes.PREGUNTA_ELIMINAR))) {
                    ObservableList<Linea_pedido> libros_a_eliminar = tabla.getSelectionModel().getSelectedItems();
                    for (Linea_pedido libro_a_eliminar : libros_a_eliminar) {
                        Linea_pedidoDBDAO.eliminarRegistro(libro_a_eliminar);
                        Pedido = pedidoDBDAO.buscarPorClavePrimaria(Pedido.getId());
                        actualizar_datos_tabla();
                    }
                }

            } catch (XmlRpcException ex) {
                Dialog.Mensaje_de_excepcion(ex);
            } catch (Exception ex) {
                Dialog.Mensaje_de_excepcion(ex);
            }
        });
        ContextMenu menu = new ContextMenu();
        menu.getItems().setAll(menuItem1, menuItem2);
        tabla.setContextMenu(menu);
    }

    public void pantalla_modificar_libro(MouseEvent mouse) {

        if (Dialog.es_doble_click(mouse) && Dialog.es_boton_izquierdo(mouse)) {
            Parent parent = null;
            try {
                pedido = tabla.getSelectionModel().getSelectedItem();
                if (libro != null) {
                    FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("dialogmodificarlineapedido.fxml"),resource);
                    parent = fxmlLoader.load();
                    Scene scene = new Scene(parent);
                    Stage stage = new Stage();
                    stage.initModality(Modality.APPLICATION_MODAL);
                    stage.setScene(scene);
                    stage.showAndWait();
                    Pedido = pedidoDBDAO.buscarPorClavePrimaria(Pedido.getId());
                    actualizar_datos_tabla();
                    cambiar_a_cursor_normal();
                }

            } catch (IOException ex) {
                Dialog.Mensaje_de_excepcion(ex);
            } catch (Exception ex) {
                Dialog.Mensaje_de_excepcion(ex);
            }
        }
    }

}
