package DAO;

import ConexionOdoo.ConexionOdoo;
import static ControladorVista.ControladorGlobal.libroDAO;
import Modelo.Socio;
import Utilidades.Constantes;
import Utilidades.DAOMetodos;
import org.GenericoDAO.GenericoDAO;
import org.apache.xmlrpc.client.XmlRpcClient;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

import java.util.*;
import static java.util.Arrays.asList;
import org.apache.xmlrpc.XmlRpcException;

public class SocioDAO implements GenericoDAO<Socio> {

    private final XmlRpcClient Connection;
    
    private static final String ID = ConexionOdoo.ID;
    private static final String FIELDS = ConexionOdoo.FIELDS;
    private static final String BUSCAR_EN_LA_TABLA = ConexionOdoo.BUSCAR_EN_LA_TABLA;
    private static final String EJECUTAR = ConexionOdoo.EJECUTAR;
    private static final String INSERTAR = ConexionOdoo.INSERTAR;
    private static final String ACTUALIZAR = ConexionOdoo.ACTUALIZAR;
    private static final String ELIMINAR = ConexionOdoo.ELIMINAR;
    private static final String NOMBRE_TABLA = "biblioteca.socio";
    
    private static final String NOMBRE = Constantes.NOMBRE_TIPO;
    private static final String DNI = "dni";
    private static final String FECHA_DE_NACIMIENTO = "fecha_de_nacimiento";
    private static final String DIRECCION = "direccion";
    private static final String LOCALIDAD = "localidad";
    private static final String CONTRASENYA = "contrasenya";
    

    public SocioDAO() throws MalformedURLException, XmlRpcException, Exception {
        Connection = ConexionOdoo.getConnectionOdoo();
    }

    @Override
    public ArrayList<Socio> buscarTodos() throws MalformedURLException, XmlRpcException, Exception {
        List<Object> list;
        list = asList((Object[]) Connection.execute(EJECUTAR, asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, BUSCAR_EN_LA_TABLA,
                asList(Collections.emptyList()),
                new HashMap() {
            {
                put(FIELDS, asList(ID,  NOMBRE, DNI, FECHA_DE_NACIMIENTO, DIRECCION, LOCALIDAD, CONTRASENYA));
            }
        }
        )));

        return obtenerLista(list);
    }

    @Override
    public Socio buscarPorClavePrimaria(int id) throws MalformedURLException, XmlRpcException, Exception {
        List<Object> list;
        list = asList((Object[]) Connection.execute(EJECUTAR, asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, BUSCAR_EN_LA_TABLA,
                asList(asList(
                        asList(ID, "=", id)
                )),
                new HashMap() {
            {
                put(FIELDS, asList(ID,  NOMBRE, DNI, FECHA_DE_NACIMIENTO, DIRECCION, LOCALIDAD, CONTRASENYA));
            }
        }
        )));
        ArrayList<Socio> lista = obtenerLista(list);
        return lista.isEmpty() ? null : lista.get(0);
    }

    @Override
    public ArrayList<Socio> buscarPorTodasLasClavePrimaria(ArrayList<Integer> ids) throws MalformedURLException, XmlRpcException, Exception {
         List<Object> list;
        list = asList((Object[]) Connection.execute(EJECUTAR, asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, BUSCAR_EN_LA_TABLA,
                asList(asList(
                        asList(ID, "=", ids)
                )),
                new HashMap() {
            {
                put(FIELDS, asList(ID,  NOMBRE, DNI, FECHA_DE_NACIMIENTO, DIRECCION, LOCALIDAD, CONTRASENYA));
            }
        }
        )));
        ArrayList<Socio> lista = obtenerLista(list);
        return lista;
    }

    @Override
    public Integer insertarRegistro(Socio objeto) throws MalformedURLException, XmlRpcException, Exception {
        final Integer id = (Integer) Connection.execute(EJECUTAR, asList(ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, INSERTAR,
                asList(new HashMap() {
                    {
                        put(NOMBRE, objeto.getNombre());
                        put(DNI, objeto.getDNI());
                        put(FECHA_DE_NACIMIENTO, DAOMetodos.fecha_correcta(objeto.getFecha_de_nacimiento()));
                        put(DIRECCION, objeto.getDireccion());
                        put(LOCALIDAD, objeto.getLocalidad());
                        put(CONTRASENYA, objeto.getContrasenya());
                        
                    }
                })
        ));

        return id;
    }
    
    public Integer insertarRegistroRestaurar(Socio objeto) throws MalformedURLException, XmlRpcException, Exception {
        final Integer id = (Integer) Connection.execute(EJECUTAR, asList(ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, INSERTAR,
                asList(new HashMap() {
                    {
                        put(NOMBRE, objeto.getNombre());
                        put(DNI, objeto.getDNI());
                        put(FECHA_DE_NACIMIENTO, DAOMetodos.fecha_correcta(objeto.getFecha_de_nacimiento()));
                        put(DIRECCION, objeto.getDireccion());
                        put(LOCALIDAD, objeto.getLocalidad());
                        put(CONTRASENYA, objeto.getContrasenya());
                    }
                })
        ));

        return id;
    }

    public Integer[] insertarMasDeUnRegistroRestaurar(ArrayList<Socio> listaObjetos) throws MalformedURLException, XmlRpcException, Exception {
        int tamanyo = listaObjetos.size();
        Integer[] ids_creadas = new Integer[tamanyo];
        for (int contador = 0; contador < tamanyo; contador++) {
            ids_creadas[contador] = insertarRegistroRestaurar(listaObjetos.get(contador));
        }
        return ids_creadas;
    }

    @Override
    public boolean actualizarRegistro(Socio objeto) throws MalformedURLException, XmlRpcException, Exception {
        Connection.execute(EJECUTAR, asList(ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, ACTUALIZAR,
                asList(asList(objeto.getId()),
                        new HashMap() {
                    {
                        put(NOMBRE, objeto.getNombre());
                        put(DNI, objeto.getDNI());
                        put(FECHA_DE_NACIMIENTO, DAOMetodos.fecha_correcta(objeto.getFecha_de_nacimiento()));
                        put(DIRECCION, objeto.getDireccion());
                        put(LOCALIDAD, objeto.getLocalidad());
                        put(CONTRASENYA, objeto.getContrasenya());
                    }
                }
                )
        ));
        return true;
    }

    @Override
    public boolean eliminarRegistro(Socio objeto) throws MalformedURLException, XmlRpcException, Exception {
        int id = objeto.getId();
        Connection.execute(EJECUTAR, asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, ELIMINAR,
                asList(asList(id))));

        return buscarPorClavePrimaria(id) == null;
    }

    @Override
    public boolean eliminarRegistro(int idObjeto) throws MalformedURLException, XmlRpcException, Exception {
        Connection.execute(EJECUTAR, asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, ELIMINAR,
                asList(asList(idObjeto))));

        return buscarPorClavePrimaria(idObjeto) == null;
    }

    @Override
    public ArrayList<Socio> obtenerLista(List<Object> listaObjetos) throws MalformedURLException, XmlRpcException, Exception {
        ArrayList<Socio> lista = new ArrayList();
        Socio socio;
        HashMap elemento_actual;
        for (Object elemento : listaObjetos) {
            socio = new Socio();
            elemento_actual = (HashMap) elemento;
            
            socio.setId((int) elemento_actual.get(ID));
            socio.setNombre(DAOMetodos.transformar_texto(elemento_actual.get(NOMBRE)));
            socio.setFecha_de_nacimiento(DAOMetodos.obtener_fecha(elemento_actual.get(FECHA_DE_NACIMIENTO)));
            socio.setDNI(DAOMetodos.transformar_texto(elemento_actual.get(DNI)));
            socio.setDireccion(DAOMetodos.transformar_texto(elemento_actual.get(DIRECCION)));
            socio.setLocalidad(DAOMetodos.transformar_texto(elemento_actual.get(LOCALIDAD)));
            socio.setContrasenya(DAOMetodos.transformar_texto(elemento_actual.get(CONTRASENYA)));
            lista.add(socio);
        }

        return lista;
    }

    @Override
    public Integer[] insertarMasDeUnRegistro(ArrayList<Socio> listaObjetos) throws MalformedURLException, XmlRpcException, Exception {
        int tamanyo = listaObjetos.size();
        Integer[] ids_creadas = new Integer[tamanyo];
        for (int contador = 0; contador < tamanyo; contador++) {
            ids_creadas[contador] = insertarRegistro(listaObjetos.get(contador));
        }
        return ids_creadas;
    }
    
    public Socio buscar(Socio objeto) throws MalformedURLException, XmlRpcException, Exception {
        Socio encontrado = null;
        ArrayList<Socio> lista = buscarTodos();
        for (int i = 0; i < lista.size(); i++) {
            if (lista.get(i).equals(objeto)){
                i = lista.size();
            }
        }
        return encontrado;
    }

   
}
