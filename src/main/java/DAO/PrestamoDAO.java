package DAO;

import ConexionOdoo.ConexionOdoo;
import static ControladorVista.ControladorGlobal.libroDAO;
import static ControladorVista.ControladorGlobal.prestamoDBDAO;
import static ControladorVista.ControladorGlobal.socioDBDAO;
import Modelo.Libro;
import Modelo.Prestamo;
import Modelo.Socio;
import Utilidades.DAOMetodos;
import java.net.MalformedURLException;
import org.GenericoDAO.GenericoDAO;
import org.apache.xmlrpc.client.XmlRpcClient;

import java.util.ArrayList;
import java.util.List;
import org.apache.xmlrpc.XmlRpcException;

import java.util.*;
import static java.util.Arrays.asList;
import org.apache.xmlrpc.XmlRpcException;

public class PrestamoDAO implements GenericoDAO<Prestamo> {

    private XmlRpcClient Connection;
    private LibroDAO libroDAO;
    private SocioDAO socioDAO;

    private static final String ESTADO_DEL_LIBRO = "estado";
    private static final String FECHA_DE_DEVOLUCION = "fecha_de_devolucion";
    private static final String FECHA_DE_DEVUELTO = "fecha_de_devuelto";
    private static final String FECHA_DE_PRESTAMO = "fecha_de_prestamo";
    private static final String SOCIO = "socio_id";
    private static final String LIBRO = "libro_id";

    private static final String NOMBRE_TABLA = "biblioteca.prestamo";
    private static final String ID = ConexionOdoo.ID;
    private static final String FIELDS = ConexionOdoo.FIELDS;
    private static final String BUSCAR_EN_LA_TABLA = ConexionOdoo.BUSCAR_EN_LA_TABLA;
    private static final String EJECUTAR = ConexionOdoo.EJECUTAR;
    private static final String INSERTAR = ConexionOdoo.INSERTAR;
    private static final String ACTUALIZAR = ConexionOdoo.ACTUALIZAR;
    private static final String ELIMINAR = ConexionOdoo.ELIMINAR;

    public PrestamoDAO() throws Exception {
        Connection = ConexionOdoo.getConnectionOdoo();
        libroDAO = new LibroDAO();
        socioDAO = new SocioDAO();
    }

    @Override
    public ArrayList<Prestamo> buscarTodos() throws MalformedURLException, XmlRpcException, Exception {
        List<Object> list;

        list = asList((Object[]) Connection.execute(EJECUTAR, asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, BUSCAR_EN_LA_TABLA,
                asList(Collections.emptyList()),
                new HashMap() {
            {
                put(FIELDS, asList(ID, FECHA_DE_DEVOLUCION, FECHA_DE_DEVUELTO, FECHA_DE_PRESTAMO, SOCIO, LIBRO));
            }
        }
        )));

        return obtenerLista(list);
    }

    @Override
    public Prestamo buscarPorClavePrimaria(int id) throws MalformedURLException, XmlRpcException, Exception {
        List<Object> list;

        list = asList((Object[]) Connection.execute(EJECUTAR, asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, BUSCAR_EN_LA_TABLA,
                asList(asList(
                        asList(ID, "=", id)
                )),
                new HashMap() {
            {
                put(FIELDS, asList(ID, FECHA_DE_DEVOLUCION, FECHA_DE_DEVUELTO, FECHA_DE_PRESTAMO, SOCIO, LIBRO));
            }
        }
        )));
        ArrayList<Prestamo> lista = obtenerLista(list);

        return lista.isEmpty() ? null : lista.get(0);
    }

    @Override
    public ArrayList<Prestamo> buscarPorTodasLasClavePrimaria(ArrayList<Integer> ids) throws MalformedURLException, XmlRpcException, Exception {
        List<Object> list;

        list = asList((Object[]) Connection.execute(EJECUTAR, asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, BUSCAR_EN_LA_TABLA,
                asList(asList(
                        asList(ID, "=", ids)
                )),
                new HashMap() {
            {
                put(FIELDS, asList(ID, FECHA_DE_DEVOLUCION, FECHA_DE_DEVUELTO, FECHA_DE_PRESTAMO, SOCIO, LIBRO));
            }
        }
        )));
        ArrayList<Prestamo> lista = obtenerLista(list);

        return lista;
    }

    public Integer insertarRegistroRestaurar(Prestamo objeto) throws MalformedURLException, XmlRpcException, Exception {
        final Integer id = (Integer) Connection.execute(EJECUTAR, asList(ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, INSERTAR,
                asList(new HashMap() {
                    {
                        put(FECHA_DE_DEVOLUCION, DAOMetodos.fecha_correcta(objeto.getFecha_que_se_tiene_que_devolver()));
                        put(FECHA_DE_DEVUELTO, DAOMetodos.fecha_correcta(objeto.getFecha_que_se_ha_devuelto()));
                        put(FECHA_DE_PRESTAMO, DAOMetodos.fecha_correcta(objeto.getFecha_de_prestamo()));
                        put(SOCIO, socioDBDAO.buscar(objeto.getSocio()).getId());
                        put(LIBRO, libroDAO.buscar(objeto.getLibro_prestado()).getId());

                    }
                })
        ));

        return id;
    }

    public Integer[] insertarMasDeUnRegistroRestaurar(ArrayList<Prestamo> listaObjetos) throws MalformedURLException, XmlRpcException, Exception {
        int tamanyo = listaObjetos.size();
        Integer[] ids_creadas = new Integer[tamanyo];
        for (int contador = 0; contador < tamanyo; contador++) {
            ids_creadas[contador] = insertarRegistroRestaurar(listaObjetos.get(contador));
        }
        return ids_creadas;
    }

    @Override
    public Integer insertarRegistro(Prestamo objeto) throws MalformedURLException, XmlRpcException, Exception {
        final Integer id = (Integer) Connection.execute(EJECUTAR, asList(ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, INSERTAR,
                asList(new HashMap() {
                    {
                        put(FECHA_DE_DEVOLUCION, DAOMetodos.fecha_correcta(objeto.getFecha_que_se_tiene_que_devolver()));
                        put(FECHA_DE_DEVUELTO, DAOMetodos.fecha_correcta(objeto.getFecha_que_se_ha_devuelto()));
                        put(FECHA_DE_PRESTAMO, DAOMetodos.fecha_correcta(objeto.getFecha_de_prestamo()));
                        put(SOCIO, objeto.getSocio().getId());
                        put(LIBRO, objeto.getLibro_prestado().getId());

                    }
                })
        ));

        return id;
    }

    @Override
    public boolean actualizarRegistro(Prestamo objeto) throws MalformedURLException, XmlRpcException, Exception {
        Connection.execute(EJECUTAR, asList(ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, ACTUALIZAR,
                asList(asList(objeto.getId()),
                        new HashMap() {
                    {
                        put(FECHA_DE_DEVOLUCION, DAOMetodos.fecha_correcta(objeto.getFecha_que_se_tiene_que_devolver()));
                        put(FECHA_DE_DEVUELTO, DAOMetodos.fecha_correcta(objeto.getFecha_que_se_ha_devuelto()));
                        put(FECHA_DE_PRESTAMO, DAOMetodos.fecha_correcta(objeto.getFecha_de_prestamo()));
                        put(SOCIO, objeto.getSocio().getId());
                        put(LIBRO, objeto.getLibro_prestado().getId());
                    }
                }
                )
        ));
        return true;
    }

    @Override
    public boolean eliminarRegistro(Prestamo objeto) throws MalformedURLException, XmlRpcException, Exception {
        int id = objeto.getId();
        Connection.execute(EJECUTAR, asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, ELIMINAR,
                asList(asList(id))));

        return buscarPorClavePrimaria(id) == null;
    }

    @Override
    public boolean eliminarRegistro(int idObjeto) throws MalformedURLException, XmlRpcException, Exception {
        Connection.execute(EJECUTAR, asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, ELIMINAR,
                asList(asList(idObjeto))));

        return buscarPorClavePrimaria(idObjeto) == null;
    }

    @Override
    public ArrayList<Prestamo> obtenerLista(List<Object> listaObjetos) throws MalformedURLException, XmlRpcException, Exception {
        ArrayList<Prestamo> lista = new ArrayList();
        Prestamo autor;
        HashMap elemento_actual;
        for (Object elemento : listaObjetos) {
            autor = new Prestamo();
            elemento_actual = (HashMap) elemento;

            autor.setId((int) elemento_actual.get(ID));
            autor.setSocio((Socio) DAOMetodos.Relacion_uno_a_muchos(elemento_actual, SOCIO, SocioDAO.class.getName()));
            autor.setLibro_prestado((Libro) DAOMetodos.Relacion_uno_a_muchos(elemento_actual, LIBRO, LibroDAO.class.getName()));
            autor.setFecha_de_prestamo(DAOMetodos.obtener_fecha(elemento_actual.get(FECHA_DE_PRESTAMO)));
            autor.setFecha_que_se_ha_devuelto(DAOMetodos.obtener_fecha(elemento_actual.get(FECHA_DE_DEVUELTO)));
            autor.setFecha_que_se_tiene_que_devolver(DAOMetodos.obtener_fecha(elemento_actual.get(FECHA_DE_DEVOLUCION)));

            lista.add(autor);
        }

        return lista;
    }

    @Override
    public Integer[] insertarMasDeUnRegistro(ArrayList<Prestamo> listaObjetos) throws MalformedURLException, XmlRpcException, Exception {
        int tamanyo = listaObjetos.size();
        Integer[] ids_creadas = new Integer[tamanyo];
        for (int contador = 0; contador < tamanyo; contador++) {
            ids_creadas[contador] = insertarRegistro(listaObjetos.get(contador));
        }
        return ids_creadas;
    }

    public ArrayList<Socio> obtenerLista(Socio socio) throws MalformedURLException, XmlRpcException, Exception {
        ArrayList<Socio> lista = new ArrayList();

        for (Socio socio_a_comparar : socioDBDAO.buscarTodos()) {
            if (socio_a_comparar.equals(socio)) {
                lista.add(socio_a_comparar);
            }
        }

        return lista;
    }

    public ArrayList<Libro> obtenerLista(Libro libro) throws MalformedURLException, XmlRpcException, Exception {
        ArrayList<Libro> lista = new ArrayList();

        for (Libro libro_a_comparar : libroDAO.buscarTodos()) {
            if (libro_a_comparar.equals(libro)) {
                lista.add(libro_a_comparar);
            }
        }
        return lista;
    }

    public Prestamo buscar(Prestamo objeto) throws MalformedURLException, XmlRpcException, Exception {
        Prestamo encontrado = null;
        ArrayList<Prestamo> lista = buscarTodos();
        for (int i = 0; i < lista.size(); i++) {
            if (lista.get(i).equals(objeto)) {
                i = lista.size();
            }
        }
        return encontrado;
    }

    public ArrayList<Prestamo> buscarPorLibro(final Libro libro) throws Exception {
        ArrayList<Prestamo> listaPrestamos = new ArrayList<>();

        ArrayList<Prestamo> listaPrestamos2 = buscarTodos();
        for (int contador = 0; contador < listaPrestamos2.size(); contador++) {
            if (listaPrestamos2.get(contador).getLibro_prestado().getId() == libro.getId()) {
                listaPrestamos.add(listaPrestamos2.get(contador));
            }
        }

        return listaPrestamos;
    }

    public synchronized boolean noHayPrestamo(final Libro libroABuscar) {
        final boolean[] noExistePrestamo = {true};
        try {
            ArrayList<Prestamo> listaPrestamos = buscarPorLibro(libroABuscar);

            Date fechaActual = new Date();

            for (int contador = 0; contador < listaPrestamos.size(); contador++) {
                Date fechaDevolucion = listaPrestamos.get(contador).getFecha_que_se_ha_devuelto();
                Date fechaDevolucionPactada = listaPrestamos.get(contador).getFecha_que_se_tiene_que_devolver();
                if (despuesDeDia(fechaDevolucion, fechaActual) || despuesDeDia(fechaDevolucionPactada, fechaActual)) {
                    noExistePrestamo[0] = false;
                    contador = listaPrestamos.size();
                }
            }
        } catch (Exception e) {
            noExistePrestamo[0] = false;
        }
        return noExistePrestamo[0];
    }

    public ArrayList<Libro> librosNoHayPrestamo() throws Exception {
        ArrayList<Libro> listaLibros = libroDAO.buscarTodos();
        ArrayList<Libro> listaLibros2 = new ArrayList<>();
        for (Libro libro : listaLibros) {
            if (!prestamoDBDAO.noHayPrestamo(libro)) {
                listaLibros2.add(libro);
            }
        }
        return listaLibros2;
    }

    public static Date variarDiasAFecha(Date fecha, int dias) {
        if (dias == 0) {
            return fecha;
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(fecha);
        calendar.add(Calendar.DAY_OF_YEAR, dias);
        return calendar.getTime();
    }

    public ArrayList<Prestamo> buscarPorLibro(final int libro) throws Exception {
        ArrayList<Prestamo> listaPrestamos = new ArrayList<>();

        ArrayList<Prestamo> listaPrestamos2 = buscarTodos();
        for (int contador = 0; contador < listaPrestamos2.size(); contador++) {
            if (listaPrestamos2.get(contador).getLibro_prestado().getId() == libro) {
                listaPrestamos.add(listaPrestamos2.get(contador));
            }
        }

        return listaPrestamos;
    }

    public boolean despuesDeDia(Date fechaAComparar, Date fechaActual) {
        boolean entreDosFechas;
        boolean dia = fechaAComparar.getDate() >= fechaActual.getDate();
        boolean mes = fechaAComparar.getMonth() >= fechaActual.getMonth();
        boolean anyo = fechaAComparar.getYear() >= fechaActual.getYear();
        entreDosFechas = dia && mes && anyo;

        return entreDosFechas;
    }

}
