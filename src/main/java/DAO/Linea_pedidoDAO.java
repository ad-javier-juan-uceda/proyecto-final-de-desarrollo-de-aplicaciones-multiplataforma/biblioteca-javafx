package DAO;

import ConexionOdoo.ConexionOdoo;
import static ControladorVista.ControladorGlobal.pedidoDBDAO;
import Modelo.Libro;
import Modelo.Linea_pedido;
import Modelo.Pedido;
import Utilidades.DAOMetodos;
import java.net.MalformedURLException;
import org.GenericoDAO.GenericoDAO;
import org.apache.xmlrpc.client.XmlRpcClient;

import java.util.ArrayList;
import java.util.List;
import org.apache.xmlrpc.XmlRpcException;

import java.util.*;
import static java.util.Arrays.asList;
import org.apache.xmlrpc.XmlRpcException;

public class Linea_pedidoDAO implements GenericoDAO<Linea_pedido> {

    private static XmlRpcClient Connection;
    private static PedidoDAO pedidoDAO;
    private static LibroDAO libroDAO;

    private static final String PRECIO = "precio";
    private static final String CANTIDAD = "cantidad";
    private static final String PEDIDO = "pedido_id";
    private static final String LIBRO = "libro_id";

    private static final String NOMBRE_TABLA = "biblioteca.lineapedido";
    private static final String ID = ConexionOdoo.ID;
    private static final String FIELDS = ConexionOdoo.FIELDS;
    private static final String BUSCAR_EN_LA_TABLA = ConexionOdoo.BUSCAR_EN_LA_TABLA;
    private static final String EJECUTAR = ConexionOdoo.EJECUTAR;
    private static final String INSERTAR = ConexionOdoo.INSERTAR;
    private static final String ACTUALIZAR = ConexionOdoo.ACTUALIZAR;
    private static final String ELIMINAR = ConexionOdoo.ELIMINAR;

    public Linea_pedidoDAO() throws Exception {
        Connection = ConexionOdoo.getConnectionOdoo();

        //libroDAO = new LibroDAO();
        if (libroDAO == null) {
            libroDAO = new LibroDAO();
        }

    }

    @Override
    public ArrayList<Linea_pedido> buscarTodos() throws MalformedURLException, XmlRpcException, Exception {
        List<Object> list;

        list = asList((Object[]) Connection.execute(EJECUTAR, asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, BUSCAR_EN_LA_TABLA,
                asList(Collections.emptyList()),
                new HashMap() {
            {
                put(FIELDS, asList(ID, PRECIO, CANTIDAD, PEDIDO, LIBRO));
            }
        }
        )));

        return obtenerLista(list);
    }

    @Override
    public Linea_pedido buscarPorClavePrimaria(int id) throws MalformedURLException, XmlRpcException, Exception {
        List<Object> list;

        list = asList((Object[]) Connection.execute(EJECUTAR, asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, BUSCAR_EN_LA_TABLA,
                asList(asList(
                        asList(ID, "=", id)
                )),
                new HashMap() {
            {
                put(FIELDS, asList(ID, PRECIO, CANTIDAD, PEDIDO, LIBRO));
            }
        }
        )));
        ArrayList<Linea_pedido> lista = obtenerLista(list);

        return lista.isEmpty() ? null : lista.get(0);
    }

    @Override
    public ArrayList<Linea_pedido> buscarPorTodasLasClavePrimaria(ArrayList<Integer> ids) throws MalformedURLException, XmlRpcException, Exception {
        List<Object> list;

        list = asList((Object[]) Connection.execute(EJECUTAR, asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, BUSCAR_EN_LA_TABLA,
                asList(asList(
                        asList(ID, "=", ids)
                )),
                new HashMap() {
            {
                put(FIELDS, asList(ID, PRECIO, CANTIDAD, PEDIDO, LIBRO));
            }
        }
        )));
        ArrayList<Linea_pedido> lista = obtenerLista(list);

        return lista;
    }

    @Override
    public Integer insertarRegistro(Linea_pedido objeto) throws MalformedURLException, XmlRpcException, Exception {
        final Integer id = (Integer) Connection.execute(EJECUTAR, asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, INSERTAR,
                asList(new HashMap() {
                    {
                        put(PRECIO, objeto.getPrecio());
                        put(CANTIDAD, objeto.getCantidad());
                        put(PEDIDO, objeto.getPedido().getId());
                        put(LIBRO, objeto.getLibro().getId());

                    }
                })
        ));

        return id;
    }

    public Integer insertarRegistroRestaurar(Linea_pedido objeto) throws MalformedURLException, XmlRpcException, Exception {
        final Integer id = (Integer) Connection.execute(EJECUTAR, asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, INSERTAR,
                asList(new HashMap() {
                    {
                        put(PRECIO, objeto.getPrecio());
                        put(CANTIDAD, objeto.getCantidad());
                        put(PEDIDO, pedidoDBDAO.buscar(objeto.getPedido()).getId());
                        put(LIBRO, libroDAO.buscar(objeto.getLibro()).getId());

                    }
                })
        ));

        return id;
    }

    public Integer[] insertarMasDeUnRegistroRestaurar(ArrayList<Linea_pedido> listaObjetos) throws MalformedURLException, XmlRpcException, Exception {
        int tamanyo = listaObjetos.size();
        Integer[] ids_creadas = new Integer[tamanyo];
        for (int contador = 0; contador < tamanyo; contador++) {
            ids_creadas[contador] = insertarRegistroRestaurar(listaObjetos.get(contador));
        }
        return ids_creadas;
    }

    @Override
    public boolean actualizarRegistro(Linea_pedido objeto) throws MalformedURLException, XmlRpcException, Exception {
        System.out.println("Linea " + objeto.getId());
        Connection.execute(EJECUTAR, asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, ACTUALIZAR,
                asList(
                        asList(objeto.getId()),
                        new HashMap() {
                    {
                        put(PRECIO, objeto.getPrecio());
                        put(CANTIDAD, objeto.getCantidad());
                        put(PEDIDO, objeto.getPedido().getId());
                        put(LIBRO, objeto.getLibro().getId());
                    }
                }
                )
        ));
        return true;
    }

    @Override
    public boolean eliminarRegistro(Linea_pedido objeto) throws MalformedURLException, XmlRpcException, Exception {
        int id = objeto.getId();
        Connection.execute(EJECUTAR, asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, ELIMINAR,
                asList(asList(id))));

        return buscarPorClavePrimaria(id) == null;
    }

    @Override
    public boolean eliminarRegistro(int idObjeto) throws MalformedURLException, XmlRpcException, Exception {
        Connection.execute(EJECUTAR, asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, ELIMINAR,
                asList(asList(idObjeto))));

        return buscarPorClavePrimaria(idObjeto) == null;
    }

    public void eliminarRegistros(ArrayList<Linea_pedido> lista_Lineas) throws MalformedURLException, XmlRpcException, Exception {

        for (Linea_pedido linea_pedido : lista_Lineas) {
            eliminarRegistro(linea_pedido);
        }
    }

    @Override
    public ArrayList<Linea_pedido> obtenerLista(List<Object> listaObjetos) throws MalformedURLException, XmlRpcException, Exception {

        ArrayList<Linea_pedido> lista = new ArrayList();
        Linea_pedido lineaPedido;
        HashMap elemento_actual;
        for (Object elemento : listaObjetos) {
            lineaPedido = new Linea_pedido();
            elemento_actual = (HashMap) elemento;

            lineaPedido.setId((int) elemento_actual.get(ID));
            lineaPedido.setPrecio((double) elemento_actual.get(PRECIO));
            lineaPedido.setCantidad((int) elemento_actual.get(CANTIDAD));
            //lineaPedido.setIdpedido((Pedido) DAOMetodos.Relacion_uno_a_muchos(elemento_actual, PEDIDO, pedidoDAO));
            lineaPedido.setIdpedido(DAOMetodos.Relacion_uno_a_muchos2(elemento_actual, PEDIDO, pedidoDAO));
            lineaPedido.setLibro((Libro) DAOMetodos.Relacion_uno_a_muchos(elemento_actual, LIBRO, libroDAO));
            lineaPedido.calcular_total();

            lista.add(lineaPedido);
        }

        return lista;
    }

    @Override
    public Integer[] insertarMasDeUnRegistro(ArrayList<Linea_pedido> listaObjetos) throws MalformedURLException, XmlRpcException, Exception {
        int tamanyo = listaObjetos.size();
        Integer[] ids_creadas = new Integer[tamanyo];
        for (int contador = 0; contador < tamanyo; contador++) {
            ids_creadas[contador] = insertarRegistro(listaObjetos.get(contador));
        }
        return ids_creadas;
    }

    public Linea_pedido buscar(Linea_pedido objeto) throws MalformedURLException, XmlRpcException, Exception {
        Linea_pedido encontrado = null;
        ArrayList<Linea_pedido> lista = buscarTodos();
        for (int i = 0; i < lista.size(); i++) {
            if (lista.get(i).equals(objeto)) {
                i = lista.size();
            }
        }
        return encontrado;
    }

    public ArrayList<Linea_pedido> buscarMasDeUno(ArrayList<Linea_pedido> objeto) throws MalformedURLException, XmlRpcException, Exception {
        ArrayList<Linea_pedido> encontrado = new ArrayList<>();
        ArrayList<Linea_pedido> lista = buscarTodos();

        for (int i = 0; i < lista.size(); i++) {
            encontrado.add(buscar(lista.get(i)));
        }

        return objeto;
    }

    public ArrayList<Linea_pedido> buscarLineasPedido(int id) throws XmlRpcException,Exception {
        ArrayList<Linea_pedido> lineasFactura = new ArrayList<>();
        ArrayList<Linea_pedido> todasLasLineasFactura = buscarTodos();
        for (Linea_pedido lineaFactura : todasLasLineasFactura) {
            if (lineaFactura.getIdpedido() == id) {
                lineasFactura.add(lineaFactura);
            }
            
        }
        return lineasFactura;
    }

    public void actualizarMasDeUnRegistro(ArrayList<Linea_pedido> lineas_pedido) throws XmlRpcException, Exception {
        for (Linea_pedido lineaFactura : lineas_pedido) {
            System.out.println("Linea " + lineaFactura.getPedido().getId());
            actualizarRegistro(lineaFactura);
            
        }
    }

}
