/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import ConexionOdoo.ConexionOdoo;
import Modelo.Usuario;
import Utilidades.Constantes;
import Utilidades.DAOMetodos;
import org.GenericoDAO.GenericoDAO;
import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

import java.util.*;
import static java.util.Arrays.asList;
import org.apache.xmlrpc.XmlRpcException;

/**
 *
 * @author batoi
 */
public class UsuarioDAO implements GenericoDAO<Usuario> {
    
    private XmlRpcClient Connection;
    
    private static final String ID = ConexionOdoo.ID;
    private static final String FIELDS = ConexionOdoo.FIELDS;
    private static final String BUSCAR_EN_LA_TABLA = ConexionOdoo.BUSCAR_EN_LA_TABLA;
    private static final String EJECUTAR = ConexionOdoo.EJECUTAR;
    private static final String INSERTAR = ConexionOdoo.INSERTAR;
    private static final String ACTUALIZAR = ConexionOdoo.ACTUALIZAR;
    private static final String ELIMINAR = ConexionOdoo.ELIMINAR;
    private static final String NOMBRE_TABLA = "biblioteca.usuario";
    
    private static final String NOMBRE = Constantes.NOMBRE_TIPO;
    private static final String DNI = "dni";
    private static final String FECHA_DE_NACIMIENTO = "fecha_de_nacimiento";
    private static final String DIRECCION = "direccion";
    private static final String LOCALIDAD = "localidad";
    private static final String CONTRASENYA = "contrasenya";
    

    public UsuarioDAO() throws MalformedURLException, XmlRpcException, Exception {
        Connection = ConexionOdoo.getConnectionOdoo();
    }

    @Override
    public ArrayList<Usuario> buscarTodos() throws MalformedURLException, XmlRpcException, Exception {
        List<Object> list;
        list = asList((Object[]) Connection.execute(EJECUTAR, asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, BUSCAR_EN_LA_TABLA,
                asList(Collections.emptyList()),
                new HashMap() {
            {
                put(FIELDS, asList(ID, NOMBRE, DNI, FECHA_DE_NACIMIENTO, DIRECCION, LOCALIDAD, CONTRASENYA));
            }
        }
        )));

        return obtenerLista(list);
    }

    @Override
    public Usuario buscarPorClavePrimaria(int id) throws MalformedURLException, XmlRpcException, Exception {
        List<Object> list;
        list = asList((Object[]) Connection.execute(EJECUTAR, asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, BUSCAR_EN_LA_TABLA,
                asList(asList(
                        asList(ID, "=", id)
                )),
                new HashMap() {
            {
                put(FIELDS, asList(ID, NOMBRE, DNI, FECHA_DE_NACIMIENTO, DIRECCION, LOCALIDAD, CONTRASENYA));
            }
        }
        )));
        ArrayList<Usuario> lista = obtenerLista(list);
        return lista.isEmpty() ? null : lista.get(0);
    }

    @Override
    public ArrayList<Usuario> buscarPorTodasLasClavePrimaria(ArrayList<Integer> ids) throws MalformedURLException, XmlRpcException, Exception {
         List<Object> list;
        list = asList((Object[]) Connection.execute(EJECUTAR, asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, BUSCAR_EN_LA_TABLA,
                asList(asList(
                        asList(ID, "=", ids)
                )),
                new HashMap() {
            {
                put(FIELDS, asList(ID, NOMBRE, DNI, FECHA_DE_NACIMIENTO, DIRECCION, LOCALIDAD, CONTRASENYA));
            }
        }
        )));
        ArrayList<Usuario> lista = obtenerLista(list);
        return lista;
    }

    @Override
    public Integer insertarRegistro(Usuario objeto) throws MalformedURLException, XmlRpcException, Exception {
        final Integer id = (Integer) Connection.execute(EJECUTAR, asList(ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, INSERTAR,
                asList(new HashMap() {
                    {
                        put(NOMBRE, objeto.getNombre());
                        put(DNI, objeto.getDNI());
                        put(FECHA_DE_NACIMIENTO, DAOMetodos.fecha_correcta(objeto.getFecha_de_nacimiento()));
                        put(DIRECCION, objeto.getDireccion());
                        put(LOCALIDAD, objeto.getLocalidad());
                        put(CONTRASENYA, objeto.getContrasenya());
                    }
                })
        ));

        return id;
    }
    
    public Integer insertarRegistroRestaurar(Usuario objeto) throws MalformedURLException, XmlRpcException, Exception {
        final Integer id = (Integer) Connection.execute(EJECUTAR, asList(ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, INSERTAR,
                asList(new HashMap() {
                    {
                        put(NOMBRE, objeto.getNombre());
                        put(DNI, objeto.getDNI());
                        put(FECHA_DE_NACIMIENTO, DAOMetodos.fecha_correcta(objeto.getFecha_de_nacimiento()));
                        put(DIRECCION, objeto.getDireccion());
                        put(LOCALIDAD, objeto.getLocalidad());
                    }
                })
        ));

        return id;
    }

    public Integer[] insertarMasDeUnRegistroRestaurar(ArrayList<Usuario> listaObjetos) throws MalformedURLException, XmlRpcException, Exception {
        int tamanyo = listaObjetos.size();
        Integer[] ids_creadas = new Integer[tamanyo];
        for (int contador = 0; contador < tamanyo; contador++) {
            ids_creadas[contador] = insertarRegistroRestaurar(listaObjetos.get(contador));
        }
        return ids_creadas;
    }

    @Override
    public boolean actualizarRegistro(Usuario objeto) throws MalformedURLException, XmlRpcException, Exception {
        Connection.execute(EJECUTAR, asList(ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, ACTUALIZAR,
                asList(asList(objeto.getId()),
                        new HashMap() {
                    {
                        put(NOMBRE, objeto.getNombre());
                        put(DNI, objeto.getDNI());
                        put(FECHA_DE_NACIMIENTO, DAOMetodos.fecha_correcta(objeto.getFecha_de_nacimiento()));
                        put(DIRECCION, objeto.getDireccion());
                        put(LOCALIDAD, objeto.getLocalidad());
                        put(CONTRASENYA, objeto.getContrasenya());
                    }
                }
                )
        ));
        return true;
    }

    @Override
    public boolean eliminarRegistro(Usuario objeto) throws MalformedURLException, XmlRpcException, Exception {
        int id = objeto.getId();
        Connection.execute(EJECUTAR, asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, ELIMINAR,
                asList(asList(id))));

        return buscarPorClavePrimaria(id) == null;
    }

    @Override
    public boolean eliminarRegistro(int idObjeto) throws MalformedURLException, XmlRpcException, Exception {
        Connection.execute(EJECUTAR, asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, ELIMINAR,
                asList(asList(idObjeto))));

        return buscarPorClavePrimaria(idObjeto) == null;
    }

    @Override
    public ArrayList<Usuario> obtenerLista(List<Object> listaObjetos) throws MalformedURLException, XmlRpcException, Exception {
        ArrayList<Usuario> lista = new ArrayList();
        Usuario socio;
        HashMap elemento_actual;
        for (Object elemento : listaObjetos) {
            socio = new Usuario();
            elemento_actual = (HashMap) elemento;
            
            socio.setId((int) elemento_actual.get(ID));
            socio.setNombre(DAOMetodos.transformar_texto(elemento_actual.get(NOMBRE)));
            socio.setFecha_de_nacimiento(DAOMetodos.obtener_fecha(elemento_actual.get(FECHA_DE_NACIMIENTO)));
            socio.setDNI(DAOMetodos.transformar_texto(elemento_actual.get(DNI)));
            socio.setDireccion(DAOMetodos.transformar_texto(elemento_actual.get(DIRECCION)));
            socio.setLocalidad(DAOMetodos.transformar_texto(elemento_actual.get(LOCALIDAD)));
            socio.setContrasenya(DAOMetodos.transformar_texto(elemento_actual.get(CONTRASENYA)));
            lista.add(socio);
        }

        return lista;
    }

    @Override
    public Integer[] insertarMasDeUnRegistro(ArrayList<Usuario> listaObjetos) throws MalformedURLException, XmlRpcException, Exception {
        int tamanyo = listaObjetos.size();
        Integer[] ids_creadas = new Integer[tamanyo];
        for (int contador = 0; contador < tamanyo; contador++) {
            ids_creadas[contador] = insertarRegistro(listaObjetos.get(contador));
        }
        return ids_creadas;
    }
    
    public Usuario Iniciar_sesion(String nombre_usuario, String dni) throws MalformedURLException, XmlRpcException, Exception {
        ArrayList<Usuario> lista_usuarios = buscarTodos();
        boolean encontrado = Constantes.BOOLEAN_POR_DEFECTO;
        int contador;
        int posicion = 0;
        String contrasenya = DAOMetodos.desencriptar_contrasenya(dni);
        for(contador=0;contador<lista_usuarios.size(); contador++){
            if (lista_usuarios.get(contador).getNombre().equalsIgnoreCase(nombre_usuario) && lista_usuarios.get(contador).getContrasenya().equalsIgnoreCase(contrasenya)) {
                posicion = contador;
                contador = lista_usuarios.size();
                
                encontrado = true;
            }
        }
        
        
        return encontrado ? lista_usuarios.get(posicion) : null;
    }
    
    public Usuario buscar(Usuario objeto) throws MalformedURLException, XmlRpcException, Exception {
        Usuario encontrado = null;
        ArrayList<Usuario> lista = buscarTodos();
        for (int i = 0; i < lista.size(); i++) {
            if (lista.get(i).equals(objeto)){
                i = lista.size();
            }
        }
        return encontrado;
    }
    
}
